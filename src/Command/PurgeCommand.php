<?php

namespace App\Command;


use App\Mailer\ImportMailer;

use App\Repository\FacturenokRepository;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @see https://flysystem.thephpleague.com/v2/docs/usage/filesystem-api/
 */
class PurgeCommand extends Command
{
    /*
    private FilesystemReader $sourcesStorage;
    private FilesystemReader $sourceftpStorage;
    private FilesystemOperator $importsStorage;
    private NamingStrategyInterface $namingStrategy;
    private LoggerInterface $importLogger;
    private LoggerInterface $securityLogger;
    private ImportMailer $importMailer;
*/


    private  $importLogger;
    private  $importMailer;
    private $facturenokRepository;

    public function __construct(
        ImportMailer $importMailer,
        ?LoggerInterface $importLogger,
        FacturenokRepository $facturenokRepository

    )
    {
        parent::__construct();

        $this->importMailer = $importMailer;
        $this->importLogger = $importLogger ?? new NullLogger();
        $this->facturenokRepository =$facturenokRepository;

    }

    protected function configure(): void
    {
        $this
            ->setName('app:purge:files')
            ->setDescription('Purge factures fournisseurs en rejet.')
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Dry run mode.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {

            // Style
            $io = new SymfonyStyle($input, $output);
            $summary = [];
            # Répertoire correspondant à l'exercice en cours
            # récupération de la date du dernier import
            $recupmindate = $this->facturenokRepository->recupMaxdate();
            $mindate=$recupmindate[0]['datemin'];
            $nbrec =  $this->facturenokRepository->nbFactFrnRejetByDatemax($mindate);

            $nbrecords = $nbrec[0]['nbrecords'];
            $nbrecordsdeleted =  $this->facturenokRepository->delFactFrnRejetByDatemax($mindate);

            $summary[] = [$nbrecords, $nbrecordsdeleted];

            } catch ( \Exception $e) {

                $this->importLogger->error("Erreur : ".$e);

            }

        $this->importLogger->info('Purge terminée !');

        $this->importMailer->sendReportPurge($summary);


        return self::SUCCESS;
    }
}
