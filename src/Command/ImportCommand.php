<?php

namespace App\Command;

use App\Import\NamingStrategyInterface;
use App\Mailer\ImportMailer;
use League\Flysystem\FilesystemOperator;
use League\Flysystem\FilesystemReader;
use League\Flysystem\StorageAttributes;
use League\Flysystem\UnableToWriteFile;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @see https://flysystem.thephpleague.com/v2/docs/usage/filesystem-api/
 */
class ImportCommand extends Command
{
    /*
    private FilesystemReader $sourcesStorage;
    private FilesystemReader $sourceftpStorage;
    private FilesystemOperator $importsStorage;
    private NamingStrategyInterface $namingStrategy;
    private LoggerInterface $importLogger;
    private LoggerInterface $securityLogger;
    private ImportMailer $importMailer;
*/
    private  $sourcesStorage;
    private  $sourceftpStorage;
    private  $importsStorage;
    private  $namingStrategy;
    private  $importLogger;
   // private  $tytyLogger;
    private  $securityLogger;
    private  $importMailer;

    public function __construct(
       // FilesystemReader $sourcesStorage,
        FilesystemReader $sourceftpStorage,
        FilesystemOperator $importsStorage,
        NamingStrategyInterface $namingStrategy,
        ImportMailer $importMailer,
        ?LoggerInterface $importLogger,
        ?LoggerInterface $securityLogger

    )
    {
        parent::__construct();
        $this->sourceftpStorage = $sourceftpStorage;
        //$this->sourcesStorage = $sourcesStorage;
        $this->importsStorage = $importsStorage;

        $this->namingStrategy = $namingStrategy;
        $this->importMailer = $importMailer;

        $this->importLogger = $importLogger ?? new NullLogger();
        $this->securityLogger = $securityLogger ?? new NullLogger();
    }

    protected function configure(): void
    {
        $this
            ->setName('app:import:files')
            ->setDescription('Import des factures.')
            ->addArgument('folder', InputArgument::OPTIONAL, 'Folder to scan.')
            ->addOption('sleep', 's', InputOption::VALUE_REQUIRED, 'Sleep in seconds.', 0)
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Dry run mode.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        # Mois en cours
        $mois =  date("m");
        # Répertoire correspondant à l'exercice en cours
        $folder = '/'.date("Y");
        # Répertoire correspondant à l'exercice précédent
        $folder1 = '/'.(date("Y")-1);

        // Style
        $io = new SymfonyStyle($input, $output);

       // if ($io->confirm('Nettoyage du répertoire avant Import ?', true)) {
           # Nettoyage des répertoire(s) avant Import
            $this->importsStorage->deleteDirectory($folder);
            #re-création en donnant les droits en lecture
            $this->importsStorage->createDirectory($folder,['visibility' => 'public']);
            # Durant les deux premiers mois de l'année on travaille aussi sur l'exercice N-1
        if (($mois =='01') ||($mois =='02')) {

            $this->importsStorage->deleteDirectory($folder1);
            $this->importsStorage->createDirectory($folder1,['visibility' => 'public']);

        }


        if ($input->hasOption('sleep')) {
            $sleep = (float) $input->getOption('sleep');
        } else {
            $sleep = 0;
        }

        $dryRun = $input->getOption('dry-run');
        # Récupèrer le nombre de fichiers , pour optimiser l'avancement de la progress bar
        // Get contents (should be in a dedicated service)

        /** @var array<StorageAttributes> $contents */
        //$contents = $this->sourcesStorage->listContents($folder, FilesystemReader::LIST_DEEP)->toArray();

        $contents = $this->sourceftpStorage->listContents($folder, FilesystemReader::LIST_DEEP)->toArray();

        # Trois premiers mois de l'année
        if (($mois =='01') ||($mois =='02')) {
            /** @var array<StorageAttributes> $contents1 */
            //$contents1 = $this->sourcesStorage->listContents($folder1, FilesystemReader::LIST_DEEP)->toArray();
            $contents1 = $this->sourceftpStorage->listContents($folder1, FilesystemReader::LIST_DEEP)->toArray();
            # Fusion des des deux tableaux (récup des données pour les deux exercices)
            $contents = array_merge((array)$contents, (array)$contents1);
        }

        if ($dryRun) {
            $io->warning('Dry run mode !');
        }
        $progressBar = $io->createProgressBar(count($contents));
        $progressBar->start();

        $summary = [];

        foreach ($contents as $content) {


            $from = $content->path();
            $to = ($this->namingStrategy)($content->path());
            $summary[] = [$from, $to];

            if ($this->importsStorage->fileExists($to)) {
                $this->importLogger->warning("$to existe déjà.");
            }

            try {
                # a revoir dans le cadre de la cpam
                // Should be in a dedicated service
                if (preg_match('#\.php$#', $from)) {
                    $this->securityLogger->error("Hack detecté \"$from\" !");
                }

                    #-------------------------------------------------------------
                    # $this->importsStorage
                    # !!!! et voir aussi importsFtpStorage pour des imports provenant de deux source différentes
                    if (!preg_match("#^(?P<prefixe>__error)(?P<file>.+)$#", $to)) {

                       // $this->importsStorage->writeStream($to, $this->sourceStorage->readStream($from));
                        $this->importsStorage->writeStream($to, $this->sourceftpStorage->readStream($from), ['visibility' => 'public']);
                   }



            } catch (UnableToWriteFile $e) {

                $this->importLogger->error("Unable to write $to !");
            }
        }

        $progressBar->finish();
        //$message = utf8_encode('Import termine !');
        $io->newLine();

        $io->success('Import termine !');

        # récapitulatif de tout ce qui a été fait
        $io->table(['from', 'to'], $summary);

        $this->importLogger->info('Import termine !');

        $this->importMailer->sendReport($summary);


        return self::SUCCESS;
    }
}
