<?php


namespace App\Security;
use Cnam\AuthSrvSecBundle\Authenticator\SrvSecAuthenticator;
use Cnam\AuthSrvSecBundle\DependencyInjection\Security\AuthenticatorResponseService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;




class AuthenticatorRedirect extends AuthenticatorResponseService

{
    /**
     * Response retournée en cas d'échec.
     *
     * @return Response|null
     */
    public function getFailureResponse(Request $request, AuthenticationException $exception)
    {
        $message="Test </br>";
        $data = [
            'message' => $message.strtr($exception->getMessageKey(), $exception->getMessageData()),
        ];

        return new JsonResponse($data, 403);
    }
}