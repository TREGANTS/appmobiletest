<?php

namespace App\Import;

class DateNamingStrategy implements NamingStrategyInterface
{
    public function __invoke(string $source): string
    {

        # traitement du fichier : repertoire (année , '/', quantieme[facultatif:cas possible !!],'/',prefixe chaine 'ODP' - numéro ODP - nom du fichier - extension pdf

        if (preg_match("#^(?P<repannee>\d{4})(?P<sep>\/)(?P<repquantieme>.+\/)?(?P<prefixe>ODP)(?P<blanc>'^\s+$')? (?P<numodp>\d+)(?P<file>.+)(?P<ext>\.(pdf))$#", $source, $matches)) {
           return $matches['repannee'].'/'.$matches['numodp'].$matches['ext'];

        }

        return '__error/'.$source;

    }
}
