<?php

namespace App\Import;

class DummyNamingStrategy implements NamingStrategyInterface
{
    public function __invoke(string $source): string
    {
        return $source;
    }
}
