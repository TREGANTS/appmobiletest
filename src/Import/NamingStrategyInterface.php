<?php

namespace App\Import;

interface NamingStrategyInterface
{
    public function __invoke(string $source): string;
}
