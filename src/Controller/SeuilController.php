<?php

namespace App\Controller;

use App\Entity\Seuil;
use App\Form\SeuilType;
use App\Form\SeuilUpdateType;
use App\Repository\SeuilRepository;
use App\Dataclass\Seuil\UpdateSeuilRequest;
use App\Tools\outils;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * @Route("/seuil")
 */
class SeuilController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct( EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/", name="seuil_index", methods={"GET"})
     */
    public function index(SeuilRepository $seuilRepository){

       $seuil=$seuilRepository->findByAnnee(date("Y"));

        return $this->render('seuil/index.html.twig',[
            'seuils'=>$seuil
        ]);

    }

    /**
     * @Route("/new", name="seuil_new", methods={"GET","POST"})
     */
    public function new(Request $request,ValidatorInterface $validator): Response
    {
        $form = $this->createForm(SeuilType::class);

        $form->handleRequest($request);
        $seuil = new Seuil();


        if ($form->isSubmitted() && $form->isValid()) {

            $data= $form->getData();

            $seuil->setNom($data->nom);
            $seuil->setSeuil($data->seuil);
            $seuil->setSeuilAlerte($data->seuilAlerte);
            # on ne gère que l'année en cours, que l'on intégre systèmatiquement
            $seuil->setAnnee(date("Y"));
            # vérification contrainte d'unicité
            $errors = $validator->validate($seuil,null, ['uniq']);

            if (count($errors) > 0) {

                $erreurs = outils::violations_to_array($errors);
                $erreur = $erreurs['nom'][0];
                $this->addFlash('warning' ,$erreur);

            } else {
                # Maj entité
                $this->em->persist($seuil);
                $this->em->flush();

                return $this->redirectToRoute('seuil_index');

            }

        }

        return $this->render('seuil/new.html.twig', [
            'example' => $seuil,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="seuil_edit", methods={"GET","POST"})
     */
    public function edit(Seuil $seuil, Request $request,ValidatorInterface $validator)
    {
        # $seuil : argument  converti a partir de  {id} par le ParamConverter

        # on alimente l'instance  the $updateSeuilRequest avec les données du seuil
        $updateSeuilsRequest = UpdateSeuilRequest::fromSeuil($seuil);
        $form = $this->createForm(SeuilUpdateType::class, $updateSeuilsRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            # récup des données
            $data= $form->getData();

            $seuil->setNom($data->nom);
            $seuil->setSeuil($data->seuil);
            $seuil->setSeuilAlerte($data->seuilAlerte);

            # vérification contrainte d'unicité
            $errors = $validator->validate($seuil, null, ['uniq']);
            if (count($errors) >0) {
                $erreurs = outils::violations_to_array($errors);
                $erreur = $erreurs['nom'][0];
                $this->addFlash('warning' ,$erreur);

             } else{

                # Maj entité
                $this->em->flush();

                $this->addFlash('success' ,'La mise à jour est effectuée !');

                return $this->redirectToRoute('seuil_index');
             }
        }

        return $this->render('seuil/edit.html.twig', [
            'seuil' => $seuil,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="seuil_show", methods={"GET"})
     */
    public function show(Seuil $seuil): Response
    {
        return $this->render('seuil/show.html.twig', [
            'seuil' => $seuil,
        ]);
    }
    /**
     * @Route("/{id}", name="seuil_delete", methods={"POST"})
     */
    public function delete(Request $request, Seuil $seuil,ValidatorInterface $validator): Response
    {
        if ($this->isCsrfTokenValid('delete'.$seuil->getId(), $request->request->get('_token'))) {

            # vérification contrainte de validation / nomenclatures
            $errors = $validator->validate($seuil, null, ['nomencl']);

            if (count($errors) > 0) {

                $erreurs = outils::violations_to_array($errors);
                $erreur = $erreurs['nomenclatures'][0];

                $this->addFlash('warning' ,$erreur);
                # recréation du  formulaire de detail pour retourner dessus avec le message d'erreur
                # pour éviter  sur la liste des seuils'

                return $this->render('seuil/show.html.twig', [
                    'seuil' => $seuil,
                ]);

            } else {
                # Maj entité
                $this->em->remove($seuil);
                $this->em->flush();
                $this->addFlash('success' ,'La suppression est  effectuée !');

            }
        }

        return $this->redirectToRoute('seuil_index');
    }
    /**
     * @Route("/delete/{id}", name="seuil_delete2", methods={"GET"})
     */
    public function delete2(Request $request, Seuil $seuil,ValidatorInterface $validator): Response
    {

            # vérification contrainte de validation / nomenclatures
            $errors = $validator->validate($seuil, null, ['nomencl']);

            if (count($errors) > 0) {

                $erreurs = outils::violations_to_array($errors);
                $erreur = $erreurs['nomenclatures'][0];

                $this->addFlash('warning' ,$erreur);

                /*
                return $this->render('seuil/show.html.twig', [
                    'seuil' => $seuil,
                ]);
                */
            } else {
                # Maj entité
                $this->em->remove($seuil);
                $this->em->flush();
                $this->addFlash('success' ,'La suppression est  effectuée !');

            }


        return $this->redirectToRoute('seuil_index');
    }
}
