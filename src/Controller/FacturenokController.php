<?php

namespace App\Controller;

use App\Entity\Facture;
use App\Entity\Facturenok;
use App\Form\FacturenokType;
use App\Form\FournisseurFacturenok2Type;
use App\Repository\FacturenokRepository;
use App\Repository\FournisseurRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/facturenok")
 */
class FacturenokController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct( EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/", name="facturenok_index", methods={"GET"})
     */
    public function index(FacturenokRepository $facturenokRepository, Request $request): Response
    {
            return $this->render('facturenok/index.html.twig', [
                'facturenoks' => $facturenokRepository->findFacturesFrnOk(),
            ]);
    }

    /**
     * @Route("/new", name="facturenok_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $pp=2;
        $facturenok = new Facturenok();
        $form = $this->createForm(FacturenokType::class, $facturenok);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($facturenok);
            $this->em->flush();

            return $this->redirectToRoute('facturenok_index');
        }

        return $this->render('facturenok/new.html.twig', [
            'facturenok' => $facturenok,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="facturenok_show", methods={"GET"})
     */
    public function show(Facturenok $facturenok): Response
    {
        return $this->render('facturenok/show.html.twig', [
            'facturenok' => $facturenok,
        ]);
    }


    /**
     * @Route("/{idfacts}/{idfr}/{anfact}/edit", name="facturenok_edit", methods={"GET","POST"})
     */
    public function edit(FacturenokRepository $facturenokRepository,FournisseurRepository $fournisseurRepository,Request $request): Response
    {

       //$fournisseur = new Fournisseur();
        # tableau de récupération des options: id fournisseur, année
        $opts = [];
        # id factures
        $idfactures = json_decode($request->attributes->get('idfacts'));
        # id fournisseur
        $opts['idfournisseur'] = json_decode($request->attributes->get('idfr'));
        # annee facture
        $opts['annee'] = json_decode($request->attributes->get('anfact'));

        $form = $this->createForm(FournisseurFacturenok2Type::class,$opts);

        # recupération du fournisseur
        $fournisseur = $fournisseurRepository->find( $opts['idfournisseur']);

        # recupération des factures
        $facturenoks = $facturenokRepository->findlistFacturesnok($idfactures);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            # Récup des données
            $data= $form->getData();

            # Transaction : insertion dans table facture et suppression de la table facturenok
            $this->em->getConnection()->beginTransaction(); // suspend auto-commit

             try {
                # boucle sur les factures
                foreach ($facturenoks as $facturenok) {

                    $facture= new Facture();
                    # Alimentation entité
                    $facture->setFournisseur($data['fournisseur']);
                    $facture->setAnnee($facturenok->getAnnee());
                    $facture->setMontant($facturenok->getMontant());
                    $facture->setNomenclatureFournisseur($data['nomenclatureFournisseur']);
                    $facture->setChemindoc($facturenok->getChemindoc());
                    $facture->setDateimport($facturenok->getDateimport());
                    $facture->setOrganisme($facturenok->getOrganisme());
                    $facture->setCompte($facturenok->getCompte());
                    $facture->setDescription($facturenok->getDescription());
                    $facture->setQuantieme($facturenok->getQuantieme());
                    $facture->setDatefacture($facturenok->getDatefacture());

                        # Intégration de la facture complétée =>entité facture.
                        $this->em->persist($facture);
                        $this->em->flush();

                        # Suppression  de la facture a compléter => entité facturenok
                        $this->em->remove($facturenok);
                        $this->em->flush();

                }

            # Traitement réussi : on procède au  "commit"
             $this->em->getConnection()->commit();
             $this->addFlash('success' ,'Les factures sont intégrées !');


           } catch ( \Exception $e) {
               $this->em->getConnection()->rollback();
               $this->em->close();
               throw $e;
           }

            return $this->redirectToRoute('facturenok_index');

        }

        return $this->render('facturenok/edit.html.twig', [
            'fournisseur' => $fournisseur,
            'facturenoks' => $facturenoks,
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/delete/{idfacts}", name="facturenok_delete2", methods={"GET"})
     */
    public function delete2(FacturenokRepository $facturenokRepository,Request $request): Response
    {

        # id factures
        $idfactures = json_decode($request->attributes->get('idfacts'));

        # recupération des factures
        $facturenoks = $facturenokRepository->findlistFacturesnok($idfactures);

            # Transaction :  suppressions de la table facturenok
            $this->em->getConnection()->beginTransaction(); // suspend auto-commit

            try {
                # boucle sur les factures
                foreach ($facturenoks as $facturenok) {
                    $this->em->remove($facturenok);
                    $this->em->flush();
                }

                # Traitement réussi : on "commit"
                $this->em->getConnection()->commit();
                $this->addFlash('success' ,'Les factures sont supprimées !');

            } catch ( \Exception $e) {
                $this->em->getConnection()->rollback();
                $this->em->close();
                throw $e;
            }



        return $this->redirectToRoute('facturenok_index');
    }

    /**
     * @Route("/fournisseurs/rejet", name="factures_frn", methods={"GET"})
     */
    public function FournisseursRejet(FacturenokRepository $facturenokRepository): Response
    {

        # récupération de la date du dernier import
        $recupmindate = $facturenokRepository->recupMaxdate();
        //$mindate= $recupmaxdate[0][1];
        $mindate=$recupmindate[0]['datemin'];

        return $this->render('facturenok/frnrejet.html.twig', [
            'factures' => $facturenokRepository-> findFrnRejetByDatemax($mindate),
        ]);
    }

}
