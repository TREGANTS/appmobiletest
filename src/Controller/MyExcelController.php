<?php

namespace App\Controller;

use App\Repository\NomenclatureRepository;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
class MyExcelController extends AbstractController
{
    /**
     * @Route("/myexcel/{an<\d+>}", name="myexcel",requirements={"an"="\d{4}"})
     */
    public function index(NomenclatureRepository $nomenclatureRepository,$an): Response
    {

        $tab=$nomenclatureRepository->findForExcel($an);
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical'   => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'bottom' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color' => array('rgb' => '333333'),
                ),
            ),
            'fill' => array(
                'type'       => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation'   => 90,
                'startcolor' => array('rgb' => '0d0d0d'),
                'endColor'   => array('rgb' => 'f2f2f2'),
            ),
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleArray);
        // auto fit column to content
        foreach(range('A', 'G') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }

        $sheet->setTitle("Résultats année ".$an);
        // Entetes du tableau

        $sheet -> setCellValue('A1', 'Famille');
        $sheet -> setCellValue('B1', 'Numéro');
        $sheet -> setCellValue('C1', 'Intitulé');
        $sheet -> setCellValue('D1', 'Numéro tiers');
        $sheet -> setCellValue('E1', 'Fournisseur');
        $sheet -> setCellValue('F1', 'seuil');
        $sheet -> setCellValue('G1', 'Montant fournisseur');
        $sheet -> setCellValue('H1', 'Total nomenclature');

        $x = 2;
        foreach($tab as $get) {
            $sheet->setCellValue('A' . $x, $get['famille']);
            $sheet->setCellValue('B' . $x, $get['numnomenclature']);
            $sheet->setCellValue('C' . $x, $get['intitule']);
            $sheet->setCellValue('D' . $x, $get['numtiers']);
            $sheet->setCellValue('E' . $x, $get['fournisseur']);
            $sheet->setCellValue('F' . $x, $get['seuil']);
            $sheet->setCellValue('G' . $x, $get['totalfournisseur']);
            $sheet->setCellValue('H' . $x, $get['totalnomenclature']);
            $x++;
        }
       // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);
        $path=$this->getParameter('kernel.project_dir').'\var\exportExcel_'.$an.'xlsx';


        $writer->save($path);

     //  return new response("ok");
    /*    $disposition = HeaderUtils::makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'exportExcel.xlsx'*/
       // );
      //  return new response("ok");
        return $this->file($path, 'exportExcel.xlsx', ResponseHeaderBag::DISPOSITION_ATTACHMENT);

     //   return $this->file($path, $path, ResponseHeaderBag::DISPOSITION_INLINE);*/


    }
}
