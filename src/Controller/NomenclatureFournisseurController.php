<?php

namespace App\Controller;


use App\Entity\NomenclatureFournisseur;
use App\Form\NomenclatureFournisseurUpdateType;
use App\Repository\FactureRepository;
use App\Repository\NomenclatureFournisseurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/nomenclature/fournisseur")
 */
class NomenclatureFournisseurController extends AbstractController
{
    /**
     * @Route("/", name="nomenclature_fournisseur")
     */
   /* public function index(): Response
    {
        return $this->render('nomenclature_fournisseur/index.html.twig', [
            'controller_name' => 'NomenclatureFournisseurController',
        ]);
    }*/
    /**
     * @Route("/{id}/edit", name="nomenclature_fournisseur_edit", methods={"GET","POST"})
     */
    //edition des nomenclaturefournisseurs dans le tableua des resultats
    public function edit(Request $request,$id,EntityManagerInterface $em,NomenclatureFournisseurRepository $nomenclatureFournisseurRepository): Response{

        $nomacF=$nomenclatureFournisseurRepository->find($id);
        $user=$this->getUser();

        if ($user->getRoles()[0]==="ROLE_ADMIN"){
            $form = $this->createForm(NomenclatureFournisseurUpdateType::class, $nomacF);
            $form->handleRequest($request);

            if ($form->isSubmitted() ) {

                # Maj entité
                $em->persist($nomacF);
                $em->flush();

                return $this->redirectToRoute('resultat_index', ['an' => $nomacF->getNomenclature()->getAnnee()]);
            }

            return $this->render('nomenclature_fournisseur/edit.html.twig', [
                'nomacf'=>$nomacF,
                'formView' => $form->createView(),
            ]);
        }
        else{

            return $this->render('nomenclature_fournisseur/show.html.twig', [
                'nomacf' => $nomacF,
            ]);
        }
    }

    /**
     * @Route("/{id}/factures",name="nomenclature_fournisseur_factures",methods={"GET"})
     */
    public function factures(FactureRepository $factureRepository,NomenclatureFournisseur $nomenclatureFournisseur){

        $factures=$factureRepository->findByNF($nomenclatureFournisseur);

        return $this->render('facture/showbynf.html.twig',[
            'factures'=>$factures,
            'NF'=>$nomenclatureFournisseur
        ]);
    }
}
