<?php

namespace App\Controller;

use App\Entity\CategorieMarche;
use App\Form\CategorieMarcheType;
use App\Repository\CategorieMarcheRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Tools\outils;
/**
 * @Route("/categorie/marche")
 */
class CategorieMarcheController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct( EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/", name="categorie_marche_index", methods={"GET"})
     */
    public function index(CategorieMarcheRepository $categorieMarcheRepository): Response
    {
        return $this->render('categorie_marche/index.html.twig', [
            'categorie_marches' => $categorieMarcheRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="categorie_marche_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $categorieMarche = new CategorieMarche();
        $form = $this->createForm(CategorieMarcheType::class, $categorieMarche);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($categorieMarche);
            $entityManager->flush();

            return $this->redirectToRoute('categorie_marche_index');
        }

        return $this->render('categorie_marche/new.html.twig', [
            'categorie_marche' => $categorieMarche,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="categorie_marche_show", methods={"GET"})
     */
    public function show(CategorieMarche $categorieMarche): Response
    {
        return $this->render('categorie_marche/show.html.twig', [
            'categorie_marche' => $categorieMarche,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="categorie_marche_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CategorieMarche $categorieMarche): Response
    {
        $form = $this->createForm(CategorieMarcheType::class, $categorieMarche);
        $form->handleRequest($request);
        //dd($form);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('categorie_marche_index');
        }

        return $this->render('categorie_marche/edit.html.twig', [
            'categorie_marche' => $categorieMarche,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="categorie_marche_delete", methods={"POST"})
     */
    public function delete(Request $request, CategorieMarche $categorieMarche,ValidatorInterface $validator ): Response
    {
        if ($this->isCsrfTokenValid('delete'.$categorieMarche->getId(), $request->request->get('_token'))) {

            # vérification contrainte de validation / nomenclatures
            $errors = $validator->validate($categorieMarche, null, ['nomacfrn']);

            if (count($errors) > 0) {

                $erreurs = outils::violations_to_array($errors);

                $erreur = $erreurs['nomenclatureFournisseurs'][0];

                $this->addFlash('warning' ,$erreur);
                // A VOIR  !!!!! A VAlIDER
                // recréation du  formulaire de detail pour retourner dessus avec le message d'erreur
                // pour eviter  sur la liste des seuils'

                return $this->render('/categorie_marche/show.html.twig', [
                    'categorieMarche' => $categorieMarche,
                ]);

            } else {
                # Maj entité
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($categorieMarche);
                $entityManager->flush();
                }

        return $this->redirectToRoute('categorie_marche_index');
        }
    }
    /**
     * @Route("/delete/{id}", name="categorie_marche_delete2", methods={"GET"})
     */
    public function delete2(Request $request, CategorieMarche $categorieMarche,ValidatorInterface $validator): Response
    {

        # vérification contrainte de validation / nomenclatures

        $errors = $validator->validate($categorieMarche, null, ['nomacfrn']);

        if (count($errors) > 0) {

            $erreurs = outils::violations_to_array($errors);

            $erreur = $erreurs['nomenclatureFournisseurs'][0];

            $this->addFlash('warning' ,$erreur);

        } else {

            # Maj entité
            $this->em->remove($categorieMarche);

            $this->em->flush();
            $this->addFlash('success' ,'La suppression est  effectuée !');

        }


        return $this->redirectToRoute('categorie_marche_index');
    }













}
