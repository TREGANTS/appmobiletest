<?php

namespace App\Controller;

use App\Entity\Facture;
use App\Form\FactureType;
use App\Repository\FactureRepository;
use App\Repository\FournisseurRepository;
use Cnam\TwigBootstrapBundle\Table\DataTableQuery;
use Cnam\TwigBootstrapBundle\Table\DataTableResponse;
use League\Flysystem\FilesystemOperator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


/**
 * @Route("/facture")
 */
class FactureController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/", name="facture_index", methods={"GET"})
     */
    public function index(FactureRepository $factureRepository): Response
    {
        return $this->render('facture/index.html.twig', [
            'factures' => $factureRepository->findByAnnee(date("Y")),
        ]);

    }

    /**
     * @Route("/recup", name="facture_recup", methods={"GET"})
     */
    public function recupfactures(FactureRepository $factureRepository): Response
    {
        return ($factureRepository->findAllValid());

    }


    /**
     * @Route("/liste", name="facture_liste", methods={"GET"})
     */
    public function lstf(FactureRepository $factureRepository): Response
    {
        return $this->render('facture/list.html.twig', [
            'factures' => $factureRepository->findAllValid(),
        ]);
    }


    /**
     * @Route("/new", name="facture_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $facture = new Facture();
        $form = $this->createForm(FactureType::class, $facture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($facture);
            $this->em->flush();
            return $this->redirectToRoute('facture_index');
        }

        return $this->render('facture/new.html.twig', [
            'facture' => $facture,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="facture_show", methods={"GET"})
     */
    public function show(Facture $facture): Response
    {
        return $this->render('facture/show.html.twig', [
            'facture' => $facture,
        ]);
    }


    /**
     * @Route("/{id}/edit", name="facture_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Facture $facture): Response
    {
        $traitement = "";

        $form = $this->createForm(FactureType::class, $facture);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            # Insertion des informations dans la table facture

            # Récup des données
            $data = $form->getData();

            # Alimentation entité

            $facture->setFournisseur($data->getFournisseur());
            $facture->setAnnee($data->getAnnee());
            $facture->setMontant($data->getMontant());
            $facture->setNomenclatureFournisseur($data->getNomenclatureFournisseur());
            $facture->setChemindoc($data->getChemindoc());
            $facture->setDateimport($data->getDateimport());
            $facture->setOrganisme($data->getOrganisme());
            $facture->setCompte($data->getCompte());
            $facture->setDescription($data->getDescription());
            $facture->setQuantieme($data->getQuantieme());
            $facture->setDateimport($data->getDatefacture());

            # Maj entité
            $this->em->persist($facture);
            $this->em->flush();

            $this->addFlash('success', 'La mise à jour est effectuée !');
            return $this->redirectToRoute('facture_index');
        }

        return $this->render('facture/edit.html.twig', [
            'facture' => $facture,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="facture_delete", methods={"POST"})
     */
    public function delete(Request $request, Facture $facture): Response
    {
        if ($this->isCsrfTokenValid('delete' . $facture->getId(), $request->request->get('_token'))) {
            $this->em->remove($facture);
            $this->em->flush();
        }

        return $this->redirectToRoute('facture_index');
    }


/**
 * @Route("/lst/recupfr2/{id<\d+>}/{an<\d+>}",name="recupfournisseur2",requirements={"an"="\d{4}"})
 */

public function data($id,$an,DataTableQuery $tableQuery, FactureRepository $factureRepository, SerializerInterface $serializer, Request $request)
{
    if (!(is_null($id)) and !(is_null($an))){

    //data est un tableau de factures
        $data = $factureRepository->findtest($id, $an);
        $dataTableResponse = new DataTableResponse($tableQuery, $data, count($data));
        # Ignorer les attributs qui générent l'erreur "circular reference"
        $jsonObject = $serializer->serialize($dataTableResponse, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [
                'nomenclatureFournisseur',
                'fournisseur'
            ]
        ]);

        $response = JsonResponse::fromJsonString($jsonObject); //un objet json
        return $response;
    }

    return $this->redirectToRoute('factures_detail'); //rajoute par ST
}

    /**
     * @Route("/fournisseur/detail", name="factures_detail",methods={"GET","POST"})
     */
    public function detail(Request $request, FactureRepository $factureRepository, FournisseurRepository $fournisseurRepository): Response
    {
        $fournisseurs = $fournisseurRepository->findFournisseurValid();

        return $this->render('facture/listfr.html.twig', [
            'fournisseurs' => $fournisseurs

        ]);
    }
    /**
     * @Route("/listeparfournisseur/{id<\d+>}/{an<\d+>}",name="factures_par_fournisseur",requirements={"an"="\d{4}"})
     */
    public function facturesparfournisseur($id,$an,DataTableQuery $tableQuery, FactureRepository $factureRepository, SerializerInterface $serializer, Request $request)
    {
        if (!(is_null($id)) and !(is_null($an))){
            $factures=$factureRepository->findtest($id, $an);
            return $this->render('facture/facturesbyfournisseur.html.twig',[
                'factures'=>$factures
            ]);
        }
        return $this->redirectToRoute('factures_detail'); //rajoute par ST
    }


    /**
     * @route("/display-pdf/{id}/{annee}", name="display-pdf")
     */
    public function displayPDF($id, $annee, FilesystemOperator $importsStorage, Request $request): Response
    {
        //Affichage des factures numérisées
        // load the file from the filesystem
        $location = '/' . $annee . '/' . $id . '.pdf';
        if ($importsStorage->fileExists($location)) {
            $file = new File('../var/factures/' . $location);
            return $this->file($file, '', ResponseHeaderBag::DISPOSITION_INLINE);
        } else {
            return $this->render('bundles/TwigBundle/Exception/errorFactureAbsente.twig', [
                'annee' => $annee,
                'odp' => $id
            ]);


        }
    }

    /**
     * @Route("/fournisseur/montant", name="factures_fournisseurs_montant",methods={"GET","POST"})
     */
    public function montantfournisseurs(Request $request, FactureRepository $factureRepository): Response
    {
        $montantsfournisseurs = $factureRepository->montantFacturesFournisseurs();
        //dd($montantsfournisseurs);
        return $this->render('facture/montantbyfournisseur.html.twig', [
            'fournisseurs' => $montantsfournisseurs

        ]);
    }


}
