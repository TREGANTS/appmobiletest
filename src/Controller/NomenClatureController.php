<?php

namespace App\Controller;

use App\Dataclass\AnneeData;
use App\Entity\Nomenclature;
use App\Entity\Seuil;
use App\Form\NomenclatureType;
use App\Repository\NomenclatureRepository;
use App\Repository\SeuilRepository;
use App\Tools\outils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/nomenclature")
 */
class NomenClatureController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct( EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * @Route("/{exercice}", name="nomenclature_index", methods={"GET"},requirements={"exercice"="\d{4}"})
     */
    public function index($exercice,NomenclatureRepository $nomenclatureRepository): Response
    {
        $ancourant=(Int) date("Y");
        $anneeExercice=array($ancourant,$ancourant-1);

        if (!in_array($exercice,$anneeExercice)){
            $msg= "Les nomenclatures de l'année d'exercice ".$exercice." ne peuvent pas être éditées";
            $this->addFlash('warning' ,$msg);
            return $this->render('home.html.twig');
        }
    $nomenclatures=$nomenclatureRepository->findByAnnee( (Int) $exercice);
        return $this->render('nomenclature/index.html.twig', [
            'nomenclatures' => $nomenclatures,
            'anneesexercice'=>$anneeExercice
        ]);
    }

    /**
     * @Route("/new/{exercice}", name="nomenclature_new", methods={"GET","POST"},requirements={"exercice"="\d{4}"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request,$exercice):Response{
    //on ne paut ajouter une nomenclature que sur l'annee en cours
        $nomenclature=new Nomenclature();
//        $annee=(Int) date("Y");

        $form = $this->createForm(NomenclatureType::class,$nomenclature,array(
            'annee' => $exercice));
        //ajout de l'anneee sur nomenclature et active=true pour nomenclatureFournisseur

//        $nomenclature ->setAnnee(date("Y"));
        $nomenclature ->setAnnee($exercice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() ) {

            $this->em->persist($nomenclature);
            $this->em->flush();
            $this->addFlash('success', 'Enregistrement effectué !');
            return $this->redirectToRoute('nomenclature_index',array('exercice'=> $exercice));
        }
        return $this->render('nomenclature/new.html.twig', [
            'form' => $form->createView(),
            'exercice'=>$exercice
        ]);
    }

    /**
     * @Route("/{id}/edit", name="nomenclature_edit", methods={"GET","POST"})
     */
    public function edit(NomenclatureRepository $nomenclatureRepository,$id,Request $request,ValidatorInterface $validator):Response
    {

        $nomenclature=$nomenclatureRepository->find($id);
        $exercice=$nomenclature->getAnnee();
        $originalNomenclatureFournisseur=new ArrayCollection();

        foreach ($nomenclature->getNomenclatureFournisseurs() as $nomenclatureFournisseur){
            $originalNomenclatureFournisseur->add($nomenclatureFournisseur);
        }

        $form = $this->createForm(NomenclatureType::class,$nomenclature, array(
            'annee' => $exercice));


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() ) {

            foreach ($originalNomenclatureFournisseur as $orgF){
                if ($nomenclature->getNomenclatureFournisseurs()->contains($orgF)===false){
                    $errors = $validator->validate($orgF, null, ['fac']);
                    if (count($errors) > 0) {
                        $erreurs = outils::violations_to_array($errors);
                        $erreur = $erreurs['factures'][0];
                        $this->addFlash('warning' ,$erreur);
                        return $this->redirectToRoute('nomenclature_edit', [
                            'id' => $nomenclature->getId()
                        ]);

                    }
                    $this->em->remove($orgF);
                }
            }

            # Maj entité
            $this->em->flush();
            $this->addFlash('success', 'La mise à jour est effectuée !');
            return $this->redirectToRoute('nomenclature_index', array('exercice' => $exercice));
        }

        return $this->render('nomenclature/new.html.twig',[
            'form'=>$form->createView(),
            'exercice'=>$exercice
        ]);

    }

    /**
     * @Route("/{id}/editseuil/{seuilid}", name="nomenclature_editSeuil", methods={"GET"})
     * @param Nomenclature $nomenclature
     * @param SeuilRepository $seuilRepository
     * @param $seuilid
     * @return Response
     */
    public function editSeuil(Nomenclature $nomenclature, SeuilRepository $seuilRepository, $seuilid):Response
    {
    //Edition du seuil dans le tableau de resultat
        /** @var $seuil Seuil */
        $seuil=$seuilRepository->find($seuilid);
        $nomenclature->setSeuil($seuil);
        $this->em->flush();
        $this->addFlash('success', 'La mise à jour est effectuée !');

        return $this->redirectToRoute('resultat_index', [
            'an' => $nomenclature->getAnnee()
        ]);
    }


    /**
     * @Route("/delete/{id}", name="nomenclature_delete", methods={"GET"})
     * @param Nomenclature $nomenclature
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function delete(Nomenclature $nomenclature, ValidatorInterface $validator): Response
    {
        $exercice=$nomenclature->getAnnee();
        # vérification contrainte de validation
        $errors = $validator->validate($nomenclature, null,'frn');

        if (count($errors) > 0) {

            $erreurs = outils::violations_to_array($errors);
            $erreur = $erreurs['nomenclatureFournisseurs'][0];

            $this->addFlash('warning' ,$erreur);
            return $this->redirectToRoute('nomenclature_edit',[
                'id'=>$nomenclature->getId()
            ]);

        } else {

            # Maj entité
            $this->em->remove($nomenclature);
            $this->em->flush();
            $this->addFlash('success' ,'La suppression est  effectuée !');
        }

        return $this->redirectToRoute('nomenclature_index', array('exercice' => $exercice));
    }

   // /**
    // * @Route("/test", name="nomenclature_test", methods={"GET"})
     //*/
 /*   public function test(){
        return $this->render('nomenclature/test.html.twig');
    }*/
}
