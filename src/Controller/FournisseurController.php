<?php

namespace App\Controller;
use App\Entity\Fournisseur;
use App\Form\FournisseurType;
use App\Form\FournisseurUpdateType;
use App\Dataclass\Fournisseur\UpdateFournisseurRequest;
use App\Repository\FournisseurRepository;
use App\Tools\outils;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Form\ContactType;



/**
 * @Route("/fournisseur")
 */
class FournisseurController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct( EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/", name="fournisseur_index", methods={"GET"})
     */
    public function index(FournisseurRepository $fournisseurRepository): Response
    {
        return $this->render('fournisseur/index.html.twig', [
            'fournisseurs' => $fournisseurRepository->findAllAsc(),
        ]);
    }

    /**
     * @Route("/new", name="fournisseur_new", methods={"GET","POST"})
     */
    public function new (Request $request,ValidatorInterface $validator): Response
    {
        $fournisseur= new Fournisseur();

        $form = $this->createForm(FournisseurType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            # Récup des données
            $data= $form->getData();
            # Alimentation entité
            $fournisseur->setNumTiers($data->numTiers);
            $fournisseur->setNom($data->nom);
            $fournisseur->setRejet($data->rejet);

            # vérification contrainte d'unicité
            $errors = $validator->validate($fournisseur,null, ['uniq']);

            if (count($errors) >0) {

                $erreurs = outils::violations_to_array($errors);
                $erreur = $erreurs['numTiers'][0];
                $this->addFlash('warning' ,$erreur);

            } else{

                $this->em->persist($fournisseur);
                $this->em->flush();

                $this->addFlash('success' ,'Une nouveau fournisseur est créé !');
                return $this->redirectToRoute('fournisseur_index');
            }

        }

        return $this->render('fournisseur/new.html.twig', [
            'fournisseur' => $fournisseur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="fournisseur_show", methods={"GET"})
     */
    public function show(Fournisseur $fournisseur): Response
    {
        return $this->render('fournisseur/show.html.twig', [
            'fournisseur' => $fournisseur,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="fournisseur_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Fournisseur $fournisseur,ValidatorInterface $validator): Response
    {
        $updateFournisseurRequest = UpdateFournisseurRequest::fromFournisseur($fournisseur);
        $form = $this->createForm(FournisseurUpdateType::class, $updateFournisseurRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            # Récup des données
            $data= $form->getData();
            # Alimentation entité
            $fournisseur->setNom($data->nom);
            $fournisseur->setNumTiers($data->numTiers);
            $fournisseur->setRejet($data->rejet);
            # vérification contrainte d'unicité /numéro
            $errors = $validator->validate($fournisseur, null, ['uniq']);

            # vérification contrainte de validation /rejet
            $error2s = $validator->validate($fournisseur, null, ['rejetfournisseur']);

            if (count($errors) >0) {
                $erreurs = outils::violations_to_array($errors);
                $erreur = $erreurs['numTiers'][0];
                $this->addFlash('warning' ,$erreur);

            }  elseif  (count($error2s) >0) {
                $erreur2s = outils::violations_to_array($error2s);

                $erreur2 = $erreur2s['nomenclatureFournisseurs'][0];

                $this->addFlash('warning' ,$erreur2);


            }  else{

                # Maj entité
                $this->em->flush();

                $this->addFlash('success' ,'La mise à jour est effectuée !');
                return $this->redirectToRoute('fournisseur_index');
            }

        }

        return $this->render('fournisseur/edit.html.twig', [
            'fournisseur' => $fournisseur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="fournisseur_delete", methods={"POST"})
     */
    public function delete(Request $request, Fournisseur $fournisseur,ValidatorInterface $validator): Response
    {
        if ($this->isCsrfTokenValid('delete'.$fournisseur->getId(), $request->request->get('_token'))) {

            # vérification contrainte de validation / nomenclatures
            $errors = $validator->validate($fournisseur, null, ['nomenclaturefournisseur']);

            if (count($errors) > 0) {

                $erreurs = outils::violations_to_array($errors);
                $erreur = $erreurs['nomenclatures'][0];

                $this->addFlash('warning' ,$erreur);
                // A VOIR  !!!!! A VAlIDER
                // recréation du  formulaire de detail pour retourner dessus avec le message d'erreur
                // pour eviter  sur la liste des seuils'

                return $this->render('fournisseur/show.html.twig', [
                    'fournisseur' => $fournisseur,
                ]);

            } else {
                # Maj entité
                $this->em->remove($fournisseur);
                $this->em->flush();
                $this->addFlash('success' ,'La suppression est effectuée !');
            }

        }
        return $this->redirectToRoute('fournisseur_index');
    }

    /**
     * @Route("/delete/{id}", name="fournisseur_delete2", methods={"GET"})
     */
    public function delete2(Request $request, Fournisseur $fournisseur,ValidatorInterface $validator): Response
    {

            # vérification contrainte de validation / factures
            $errors = $validator->validate($fournisseur, null, ['factures']);
            # vérification contrainte de validation / nomenclatures
            $errorsnomac = $validator->validate($fournisseur, null, ['nomenclaturefournisseur']);
            if (count($errors) > 0) {

                $erreurs = outils::violations_to_array($errors);
                $erreur = $erreurs['factures'][0];

                $this->addFlash('warning', $erreur);
            } elseif (count($errorsnomac) > 0) {

                $erreursnomac = outils::violations_to_array($errorsnomac);
                $erreurnomac = $erreursnomac['nomenclatureFournisseurs'][0];

                $this->addFlash('warning', $erreurnomac);
            }

            else {
                # Maj entité
                $this->em->remove($fournisseur);
                $this->em->flush();
                $this->addFlash('success' ,'La suppression est effectuée !');
            }

        return $this->redirectToRoute('fournisseur_index');
    }




}

