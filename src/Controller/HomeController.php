<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home", methods={"GET","POST"})
     */
    public function index(): Response
    {
        return $this->render('home.html.twig');
    }

    /**
     * @Route("/home", name="homepage", methods={"GET"})
     */
    public function indexbouchon(): Response
    {
        return $this->render('home.html.twig');
    }


    /**
     * @Route("/test",name="test")

    public function test(){
        return $this->render('test.html.twig');
    }
     */
}