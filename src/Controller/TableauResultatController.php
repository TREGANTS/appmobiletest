<?php

namespace App\Controller;

use App\Dataclass\AnneeData;
use App\Repository\NomenclatureFournisseurRepository;
use App\Repository\NomenclatureRepository;
use App\Repository\SeuilRepository;
use App\Repository\ViewTotalNomacRepository;
use Cnam\TwigBootstrapBundle\Table\DataTableQuery;
use Cnam\TwigBootstrapBundle\Table\DataTableResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * @Route("/resultat")
 */
class TableauResultatController extends AbstractController
{
    /**
     * @Route("/{an<\d+>}", name="resultat_index",requirements={"an"="\d{4}"})
     * @ParamConverter("tableQuery", class="Cnam\TwigBootstrapBundle\Table\DataTableQuery")
     * @param NomenclatureRepository $nomenclatureRepository
     * @param $an
     * @param ViewTotalNomacRepository $viewTotalNomacRepository
     * @param SeuilRepository $seuilRepository
     * @return Response
     */
    public function index(NomenclatureRepository $nomenclatureRepository, $an, ViewTotalNomacRepository $viewTotalNomacRepository, SeuilRepository $seuilRepository): Response
    {
        $seuils=$seuilRepository->findByAnnee($an);
        //array d'objets anneedata
        /** @var AnneeData[] $annees */
        $annees= $viewTotalNomacRepository->findAnneesExercice();


        /** @var AnneeData $anneedata */
        $bool=false;
        foreach($annees as $anneedata){
           if ($anneedata->annee==(Int) date("Y")){
               $bool=true;
              break;
            }
        }

        if (!$bool){
            $curAn= new AnneeData();
            $curAn->annee=(Int) date("Y");
            array_unshift($annees,$curAn);
        }

        //attention dans nomenclatures c'est un tableau avec des informations sur la nomenclature enrichies (total, seuil alerte)
        //ce n'est pas un tableau d'objets
        return $this->render('nomenclature/indextabresultat.html.twig', [
            'nomenclatures' => $nomenclatureRepository->findByAnWithTotaux($an),
            'annees'=>$annees,
            'seuils'=>$seuils
         ]);
    }


    /**
     * @Route("/fournisseurs/{id<\d+>}/{an<\d+>}",name="fournisseur_by_id",requirements={"an"="\d{4}"})
     * @param $id
     * @param Request $request
     * @param DataTableQuery $tableQuery
     * @param NomenclatureFournisseurRepository $NomenclatureFournisseurRepository
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function fournisseurbyid($id,$an, Request $request, DataTableQuery $tableQuery, NomenclatureFournisseurRepository $NomenclatureFournisseurRepository, SerializerInterface $serializer){
    //affichage des lignes nomenclature/fournisseur dans le tableau des resultats
        //retourne du json

      /*  if ($request->get("id") and $request->get("an")){
            $tableQuery->hasAdditionalParameters('annee');
            $tableQuery->setAdditionalParameters('annee',$request->get("an"));
            $tableQuery->setSearch(($request->get("id")));
        }*/

        if (!(is_null($id)) and !(is_null($an))){
            $tableQuery->hasAdditionalParameters('annee');
            $tableQuery->setAdditionalParameters('annee',$request->get("an"));
            $tableQuery->setSearch(($request->get("id")));
        }
      //  dd($tableQuery);
        $data = $NomenclatureFournisseurRepository->findByAnnee($tableQuery);
        //data est un tableau de tableau
        $user=$this->getUser();
        $user->getRoles()[0];

        //ajout pour chaque enregistrment du role
        foreach( $data as $key => $value ) {
            $data[$key]['auth'] = $user->getRoles()[0] ;
        }

        $dataTableResponse = new DataTableResponse($tableQuery, $data ,count($data));

        $jsonObject = $serializer->serialize($dataTableResponse,'json',[
            objectNormalizer::GROUPS=>['jsonf']
        ]);

        return JsonResponse::fromJsonString($jsonObject); //un objet json

    }
}