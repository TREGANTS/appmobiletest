<?php

namespace App\Controller;


use App\Entity\FamilleNomenclature;
use App\Form\FamilleNomenclatureType;
use App\Form\FamilleNomenclatureUpdateType;
use App\Repository\FamilleNomenclatureRepository;
use App\Tools\outils;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Dataclass\FamilleNomenclature\UpdateFamilleNomenclatureRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/famille/nomenclature")
 */
class FamilleNomenclatureController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct( EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    /**
     * @Route("/", name="famille_nomenclature_index", methods={"GET"})
     */
    public function index(FamilleNomenclatureRepository $familleNomenclatureRepository): Response
    {

      // dd(date("Y"));

       $famillenomenclature=$familleNomenclatureRepository->findByAnnee(date("Y"));

        return $this->render('famille_nomenclature/index.html.twig',[
            'famille_nomenclatures'=>$famillenomenclature
        ]);

    }
    /**
     * @Route("/new", name="famille_nomenclature_new", methods={"GET","POST"})
     */
    public function new(Request $request,ValidatorInterface $validator): Response
    {

        $familleNomenclature = new FamilleNomenclature();

        $form = $this->createForm(FamilleNomenclatureType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            # Récup des données
            $data= $form->getData();
            # Alimentation entité
            $familleNomenclature->setIntitule($data->intitule);
            # on ne gère que l'année en cours, que l'on intégre systèmatiquement
            $familleNomenclature->setAnnee(date("Y"));
            # vérification contrainte d'unicité
            $errors = $validator->validate($familleNomenclature,null, ['uniq']);

            if (count($errors) >0) {
                $erreurs = outils::violations_to_array($errors);
                $erreur = $erreurs['intitule'][0];
                $this->addFlash('warning' ,$erreur);
             //   return

            } else{
                # Maj entité
                $this->em->persist($familleNomenclature);
                $this->em->flush();

                $this->addFlash('success' ,'Une nouvelle famille est créée !');
                return $this->redirectToRoute('famille_nomenclature_index');
            }
        }

        return $this->render('famille_nomenclature/new.html.twig', [
            'famille_nomenclature' => $familleNomenclature,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="famille_nomenclature_show", methods={"GET"})
     */
    public function show(FamilleNomenclature $familleNomenclature): Response
    {
        return $this->render('famille_nomenclature/show.html.twig', [
            'famille_nomenclature' => $familleNomenclature,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="famille_nomenclature_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, FamilleNomenclature $familleNomenclature,ValidatorInterface $validator): Response
    {
        # $famillenomenclature : argument converti a partir de {id} par le ParamConverter

        # on alimente l'instance   $updateNomenclatureRequest  avec les données nomenclature
        $updateFamilleNomenclatureRequest = UpdateFamilleNomenclatureRequest::fromFamilleNomenclature($familleNomenclature);

        $form = $this->createForm(FamilleNomenclatureUpdateType::class,$updateFamilleNomenclatureRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            # Récup des données
            $data= $form->getData();
            # Alimentation entité
            $familleNomenclature->setIntitule($data->intitule);
            # vérification contrainte d'unicité

            $errors = $validator->validate($familleNomenclature, null, ['uniq']);
            if (count($errors) >0) {
                $erreurs = outils::violations_to_array($errors);
                $erreur = $erreurs['intitule'][0];
                $this->addFlash('warning' ,$erreur);

            } else{
                # Maj entité
                $this->em->flush();

                $this->addFlash('success' ,'La mise à jour est effectuée !');

                return $this->redirectToRoute('famille_nomenclature_index');
            }
        }

        return $this->render('famille_nomenclature/edit.html.twig', [
            'famille_nomenclature' => $familleNomenclature,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="famille_nomenclature_delete", methods={"POST"})
     */
    public function delete(Request $request, FamilleNomenclature $familleNomenclature,ValidatorInterface $validator): Response
    {

        if ($this->isCsrfTokenValid('delete'.$familleNomenclature->getId(), $request->request->get('_token'))) {
            # vérification  contrainte de validation / nomenclatures
            $errors = $validator->validate($familleNomenclature, null, ['nomencl']);

            if (count($errors) > 0) {

                $erreurs = outils::violations_to_array($errors);
                $erreur = $erreurs['nomenclatures'][0];

                $this->addFlash('warning' ,$erreur);

                return $this->render('famille_nomenclature/show.html.twig', [
                    'famille_nomenclature' => $familleNomenclature,
                ]);

            } else {

                # Maj entité
                $this->em->remove($familleNomenclature);
                $this->em->flush();
                $this->addFlash('success' ,'La suppression est  effectuée !');
            }
        }

        return $this->redirectToRoute('famille_nomenclature_index');
    }


    /**
     * @Route("/delete/{id}", name="famille_nomenclature_delete2", methods={"GET"})
     */
    public function delete2(Request $request, FamilleNomenclature $familleNomenclature,ValidatorInterface $validator): Response
    {

            # vérification  contrainte de validation / nomenclatures
            $errors = $validator->validate($familleNomenclature, null, ['nomencl']);

            if (count($errors) > 0) {

                $erreurs = outils::violations_to_array($errors);
                $erreur = $erreurs['nomenclatures'][0];
                $this->addFlash('warning' ,$erreur);

            } else {
                # Maj entité
                $this->em->remove($familleNomenclature);
                $this->em->flush();
                $this->addFlash('success' ,'La suppression est  effectuée !');
            }


        return $this->redirectToRoute('famille_nomenclature_index');

    }


}
