<?php

namespace App\Entity;

use App\Repository\CategorieMarcheRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * @ORM\Entity(repositoryClass=CategorieMarcheRepository::class)
 * @UniqueEntity(fields={"nom"},message = "Ce nom de catégorie est déjà utilisé, veuillez rectifier !")
 */
class CategorieMarche
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity=NomenclatureFournisseur::class, mappedBy="categorieMarche")
     */
    private $nomenclatureFournisseurs;

    public function __construct()
    {
        $this->nomenclatureFournisseurs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|NomenclatureFournisseur[]
     */
    public function getNomenclatureFournisseurs(): Collection
    {
        return $this->nomenclatureFournisseurs;
    }

    public function addNomenclatureFournisseur(NomenclatureFournisseur $nomenclatureFournisseur): self
    {
        if (!$this->nomenclatureFournisseurs->contains($nomenclatureFournisseur)) {
            $this->nomenclatureFournisseurs[] = $nomenclatureFournisseur;
            $nomenclatureFournisseur->setCategorieMarche($this);
        }

        return $this;
    }

    public function removeNomenclatureFournisseur(NomenclatureFournisseur $nomenclatureFournisseur): self
    {
        if ($this->nomenclatureFournisseurs->removeElement($nomenclatureFournisseur)) {
            // set the owning side to null (unless already changed)
            if ($nomenclatureFournisseur->getCategorieMarche() === $this) {
                $nomenclatureFournisseur->setCategorieMarche(null);
            }
        }

        return $this;
    }
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new UniqueEntity([
            'fields' => ['nom'],
            'errorPath' => 'nom',
            'message' => 'Ce nom de catégorie est déjà utilisé, veuillez rectifier !',
            'groups' => ['uniq'],
        ]));

        $metadata->addPropertyConstraint('nomenclatureFournisseurs', new Assert\Count([
            //'min' => 0,
            'max' => 0,
            'groups' => ['nomacfrn'],
            //'minMessage' => '',
            //'maxMessage' => 'Une ou plusieurs nomenclatures sont liées à ce fournisseur, vous ne pouvez donc le supprimer !',
            'exactMessage'=> 'Cette catégorie est liée a une ou plusieurs association nomenclature-fournisseur, vous ne pouvez donc pas la supprimer !',

        ]));

    }


}
