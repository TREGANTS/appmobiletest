<?php

namespace App\Entity;

use App\Repository\FamilleNomenclatureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=FamilleNomenclatureRepository::class)
 * @UniqueEntity(fields={"intitule","annee"},message = "Cet intitulé de famille de nomenclature est déjà utilisé pour cette année, veuillez rectifier !")
 */
class FamilleNomenclature
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"jsonadd"})
     */
    private $intitule;

    /**
     * @ORM\OneToMany(targetEntity=Nomenclature::class, mappedBy="famille")
     */
    private $nomenclatures;

    /**
     * @ORM\Column(type="integer")
     */
    private $annee;

    public function __construct()
    {
        $this->nomenclatures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * @return Collection|Nomenclature[]
     */
    public function getNomenclatures(): Collection
    {
        return $this->nomenclatures;
    }

    public function addNomenclature(Nomenclature $nomenclature): self
    {
        if (!$this->nomenclatures->contains($nomenclature)) {
            $this->nomenclatures[] = $nomenclature;
            $nomenclature->setFamille($this);
        }

        return $this;
    }

    public function removeNomenclature(Nomenclature $nomenclature): self
    {
        if ($this->nomenclatures->removeElement($nomenclature)) {
            // set the owning side to null (unless already changed)
            if ($nomenclature->getFamille() === $this) {
                $nomenclature->setFamille(null);
            }
        }

        return $this;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {

        $metadata->addConstraint(new UniqueEntity([
            'fields' => ['intitule','annee'],
            'errorPath' => 'intitule',
            'message' => 'Cet intitulé est déjà utilisé, veuillez rectifier !',
            'groups' => ['uniq'],
        ]));

        $metadata->addPropertyConstraint('nomenclatures', new Assert\Count([
            //'min' => 0,
            'max' => 0,
            'groups' => ['nomencl'],
            //'minMessage' => 'You must specify at least one email',
            //'maxMessage' => 'Une ou plusieurs nomenclatures sont liées à cette famille, vous ne pouvez donc le supprimer !',
            'exactMessage'=> 'Une ou plusieurs nomenclatures sont liées à cette famille, vous ne pouvez donc pas la supprimer !',
        ]));
    }

    public function getAnnee(): ?int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): self
    {
        $this->annee = $annee;

        return $this;
    }



}
