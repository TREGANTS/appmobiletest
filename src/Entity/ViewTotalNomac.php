<?php

namespace App\Entity;

use App\Repository\ViewTotalNomacRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ViewTotalNomacRepository::class,readOnly=true)
 */
class ViewTotalNomac
{
    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups({"jsonadd"})
     */
    private $total;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Groups({"jsonadd"})
     */
    private $annee;

    /**
     * @ORM\OneToOne(targetEntity=Nomenclature::class, inversedBy="viewTotalNomac", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $nomenclature;

    public function getId(): ?string
    {
        return $this->nomenclature->getId();
    }


    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getAnnee(): ?int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    public function getNomenclature(): ?Nomenclature
    {
        return $this->nomenclature;
    }

    public function setNomenclature(Nomenclature $nomenclature): self
    {
        $this->nomenclature = $nomenclature;

        return $this;
    }
}
