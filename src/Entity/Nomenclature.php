<?php /** @noinspection PhpUnusedPrivateFieldInspection */

namespace App\Entity;

use App\Repository\NomenclatureRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;



/**
 * @ORM\Entity(repositoryClass=NomenclatureRepository::class)
 * @UniqueEntity(fields={"intitule","annee"},message = "Cet intitulé de nomenclature est déjà utilisé pour cette année, veuillez rectifier !")
 * @UniqueEntity(fields={"numero","annee"},message = "Ce Numéro de nomenclature  est déjà utilisé, veuillez rectifier !")
 */

class Nomenclature
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"jsonadd"})
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\NotBlank(message="L'indication du numéro de nomenclature est obligatoire")
     * @Groups({"jsonadd"})
     * @ORM\OrderBy({"numero" = "ASC"})
     */
    private $numero;

    /**
     * @ORM\Column(type="string", length=350)
     * @Assert\NotBlank(message="L'indication de l'intitulé obligatoire")
     * @Groups({"jsonadd"})
     */
    private $intitule;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"jsonadd"})
     */
    private $detailIntitule;

    /**
     * @ORM\ManyToOne(targetEntity=FamilleNomenclature::class, inversedBy="nomenclatures")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="L'indication de la famille est obligatoire")
     * @Groups({"jsonadd"})
     */
    private $famille;

    /**
     * @ORM\ManyToOne(targetEntity=Seuil::class, inversedBy="nomenclatures")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="L'indication du seuil est obligatoire")
     * @Groups({"jsonadd"})
     */
    private $seuil;

    /**
     * @ORM\OneToMany(targetEntity=NomenclatureFournisseur::class, mappedBy="nomenclature",cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * @Assert\Valid()
     * @Groups({"jsonadd"})
     */

    private $nomenclatureFournisseurs;


    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="L'indication du l'année est obligatoire")
     */
    private $annee;



    /**
     * @ORM\OneToOne(targetEntity=ViewTotalNomac::class, mappedBy="nomenclature", cascade={"persist", "remove"})
     */
    private $viewTotalNomac;


    public function __construct()
    {

        $this->nomenclatureFournisseurs = new ArrayCollection();
        $this->factures = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(?string $numero): self
    {
        $this->numero = $numero;
        return $this;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(?string $intitule): self
    {
        $this->intitule = $intitule;
        return $this;
    }

    public function getDetailIntitule(): ?string
    {
        return $this->detailIntitule;
    }

    public function setDetailIntitule(?string $detailIntitule): self
    {
        $this->detailIntitule = $detailIntitule;
        return $this;
    }

    public function getFamille(): ?FamilleNomenclature
    {
        return $this->famille;
    }

    public function setFamille(?FamilleNomenclature $famille): self
    {
        $this->famille = $famille;

        return $this;
    }

    public function getSeuil(): ?Seuil
    {
        return $this->seuil;
    }

    public function setSeuil(?Seuil $seuil): self
    {
        $this->seuil = $seuil;

        return $this;
    }

    public function getAnnee(): ?int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): self
    {
        $this->annee = $annee;
        foreach ($this->getNomenclatureFournisseurs() as $nomenclatureFournisseur){
            $nomenclatureFournisseur->setAnnee($annee);
        }
        return $this;
    }

    /**
     * @return Collection|NomenclatureFournisseur[]
     */
    public function getNomenclatureFournisseurs(): Collection
    {
        return $this->nomenclatureFournisseurs;
    }

    public function addNomenclatureFournisseur(NomenclatureFournisseur $nomenclatureFournisseur): self
    {

        if (!$this->nomenclatureFournisseurs->contains($nomenclatureFournisseur)) {
            $this->nomenclatureFournisseurs[] = $nomenclatureFournisseur;
            $nomenclatureFournisseur->setNomenclature($this);
        }

        return $this;
    }

    public function removeNomenclatureFournisseur(NomenclatureFournisseur $nomenclatureFournisseur): self
    {
        if ($this->nomenclatureFournisseurs->removeElement($nomenclatureFournisseur)) {
            // set the owning side to null (unless already changed)

            if ($nomenclatureFournisseur->getNomenclature() === $this) {
                $nomenclatureFournisseur->setNomenclature(null);
            }
        }
        return $this;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context){

        $tabf= new ArrayCollection();
        foreach ( $this->getNomenclatureFournisseurs() as $nomenclatureFournisseur) {
            if ($tabf->contains($nomenclatureFournisseur->getFournisseur())) {

                $context->buildViolation("Pour une nomenclature un fournisseur ne peut être enregistré qu'une fois")
                    ->addViolation();
                break;
            }
            $tabf->add($nomenclatureFournisseur->getFournisseur());
        }}


    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
      $metadata->addPropertyConstraint('nomenclatureFournisseurs', new Assert\Count([
            //'min' => 0,
            'max' => 0,
            'groups' => ['frn'],
            //'minMessage' => 'You must specify at least one email',
            //'maxMessage' => 'Une ou plusieurs nomenclatures sont liées à cette famille, vous ne pouvez donc le supprimer !',
            'exactMessage'=> 'Un ou plusieurs fournisseurs sont liés à cette nomenclature, vous ne pouvez donc pas la supprimer !',
        ]));
     }

public function getViewTotalNomac(): ?ViewTotalNomac
{
    return $this->viewTotalNomac;
}

public function setViewTotalNomac(ViewTotalNomac $viewTotalNomac): self
{
    // set the owning side of the relation if necessary
    if ($viewTotalNomac->getNomenclature() !== $this) {
        $viewTotalNomac->setNomenclature($this);
    }

    $this->viewTotalNomac = $viewTotalNomac;

    return $this;
}
}