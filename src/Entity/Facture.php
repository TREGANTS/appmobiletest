<?php

namespace App\Entity;

use App\Repository\FactureRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FactureRepository::class)
 */
class Facture
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $annee;

    /**
     * @ORM\ManyToOne(targetEntity=Fournisseur::class, inversedBy="factures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fournisseur;

    /**
     * @ORM\ManyToOne(targetEntity=NomenclatureFournisseur::class, inversedBy="factures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $nomenclatureFournisseur;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $montant;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cheminDoc;

    /**
     * @ORM\Column(type="date")
     */
    private $dateimport;

    /**
     * @ORM\Column(type="integer")
     */
    private $organisme;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $compte;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="smallint")
     */
    private $quantieme;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $datefacture;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnnee(): ?int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    public function getFournisseur(): ?fournisseur
    {
        return $this->fournisseur;
    }

    public function setFournisseur(?fournisseur $fournisseur): self
    {
        $this->fournisseur = $fournisseur;

        return $this;
    }

    public function getMontant(): ?string
    {
        return $this->montant;
    }

    public function setMontant(?string $montant): self
    {
        $this->montant = $montant;

        return $this;
    }


    public function getCheminDoc(): ?string
    {
        return $this->cheminDoc;
    }

    public function setCheminDoc(?string $cheminDoc): self
    {
        $this->cheminDoc = $cheminDoc;

        return $this;
    }



    public function getDateimport(): ?\DateTimeInterface
    {
        return $this->dateimport;
    }

    public function setDateimport(?\DateTimeInterface $dateimport): self
    {
        $this->dateimport = $dateimport;

        return $this;
    }

    public function getOrganisme(): ?int
    {
        return $this->organisme;
    }

    public function setOrganisme(int $organisme): self
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getCompte(): ?int
    {
        return $this->compte;
    }

    public function setCompte(?int $compte): self
    {
        $this->compte = $compte;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getQuantieme(): ?int
    {
        return $this->quantieme;
    }

    public function setQuantieme(int $quantieme): self
    {
        $this->quantieme = $quantieme;

        return $this;
    }

    public function getNomenclatureFournisseur(): ?NomenclatureFournisseur
    {
        return $this->nomenclatureFournisseur;
    }

    public function setNomenclatureFournisseur(?NomenclatureFournisseur $nomenclatureFournisseur): self
    {
        $this->nomenclatureFournisseur = $nomenclatureFournisseur;

        return $this;
    }

    public function getDatefacture(): ?\DateTimeInterface
    {
        return $this->datefacture;
    }

    public function setDatefacture(?\DateTimeInterface $datefacture): self
    {
        $this->datefacture = $datefacture;

        return $this;
    }
}
