<?php

namespace App\Entity;

use App\Repository\ViewTotalFournisseurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ViewTotalFournisseurRepository::class,readOnly=true)
 */

class ViewTotalFournisseur
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
    private $annee;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups({"jsonf"})
     */
    private $total;

    /**
     * @ORM\OneToOne(targetEntity=NomenclatureFournisseur::class, inversedBy="viewTotalFournisseur", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $nomenclatureFournisseur;


    public function __construct()
    {
        $this->nomenclatureFournisseur = new NomenclatureFournisseur();
    }


    public function getAnnee(): ?int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;
        return $this;
    }

    public function getNomenclatureFournisseur(): ?NomenclatureFournisseur
    {
        return $this->nomenclatureFournisseur;
    }

    public function setNomenclatureFournisseur(NomenclatureFournisseur $nomenclatureFournisseur): self
    {
        $this->nomenclatureFournisseur = $nomenclatureFournisseur;

        return $this;
    }

}