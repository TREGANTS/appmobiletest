<?php

namespace App\Entity;

use App\Repository\NomenclatureFournisseurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;
/**
 * @ORM\Entity(repositoryClass=NomenclatureFournisseurRepository::class)
 */
class NomenclatureFournisseur

{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Fournisseur::class, inversedBy="nomenclatureFournisseurs")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(allowNull=false,message="L'indication d'un fournisseur est obligatoire")
     * @Groups({"jsonadd","jsonf"})
     */
    private $fournisseur;

    /**
     * @ORM\ManyToOne(targetEntity=Nomenclature::class, inversedBy="nomenclatureFournisseurs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $nomenclature;


    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Groups({"jsonadd","jsonf"})
     */
    private $typeMarche;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     * @Groups({"jsonadd","jsonf"})
     */
    private $numMarche;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"jsonadd","jsonf"})
     */
    private $obs1;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"jsonadd","jsonf"})
     */
    private $obs2;

    /**
     * @ORM\Column(type="boolean",options={"default":"1"})
     * @Groups({"jsonadd"})
     */
    private $active;

    /**
     * @ORM\OneToMany(targetEntity=Facture::class, mappedBy="nomenclatureFournisseur")
     */
    private $factures;

    /**
     * @ORM\OneToOne(targetEntity=ViewTotalFournisseur::class, mappedBy="nomenclatureFournisseur", cascade={"persist", "remove"})
     */
    private $viewTotalFournisseur;

    /**
     * @ORM\ManyToOne(targetEntity=CategorieMarche::class, inversedBy="nomenclatureFournisseurs")
     * @ORM\JoinColumn(nullable=true)
     */
    private $categorieMarche;

    /**
     * @ORM\Column(type="integer",nullable=false)
     * @Assert\NotBlank(allowNull=false,message="L'indication de l'année est obligatoire")
     */
    private $annee;


    public function __construct()
    {
        $this->factures = new ArrayCollection();
        $this->viewTotalFournisseurs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFournisseur(): ?Fournisseur
    {
        return $this->fournisseur;
    }

    public function setFournisseur(?Fournisseur $fournisseur): self
    {
        $this->fournisseur = $fournisseur;

        return $this;
    }

    public function getNomenclature(): ?Nomenclature
    {
        return $this->nomenclature;
    }

    public function setNomenclature(?Nomenclature $nomenclature): self
    {

        $this->nomenclature = $nomenclature;
        if (is_null($nomenclature)==false){
            $this->annee=$nomenclature->getAnnee();
        }
        return $this;
    }



    public function getTypeMarche(): ?string
    {
        return $this->typeMarche;
    }

    public function setTypeMarche(?string $typeMarche): self
    {
        $this->typeMarche = $typeMarche;

        return $this;
    }

    public function getNumMarche(): ?string
    {
        return $this->numMarche;
    }

    public function setNumMarche(?string $numMarche): self
    {
        $this->numMarche = $numMarche;

        return $this;
    }

    public function getObs1(): ?string
    {
        return $this->obs1;
    }

    public function setObs1(?string $obs1): self
    {
        $this->obs1 = $obs1;

        return $this;
    }

    public function getObs2(): ?string
    {
        return $this->obs2;
    }

    public function setObs2(?string $obs2): self
    {
        $this->obs2 = $obs2;

        return $this;
    }


    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Collection|Facture[]
     */
    public function getFactures(): Collection
    {
        return $this->factures;
    }

    public function addFacture(Facture $facture): self
    {
        if (!$this->factures->contains($facture)) {
            $this->factures[] = $facture;
            $facture->setNomenclatureFournisseur($this);
        }

        return $this;
    }

    public function removeFacture(Facture $facture): self
    {
        if ($this->factures->removeElement($facture)) {
            // set the owning side to null (unless already changed)
            if ($facture->getNomenclatureFournisseur() === $this) {
                $facture->setNomenclatureFournisseur(null);
            }
        }

        return $this;
    }

    public function getViewTotalFournisseur(): ?ViewTotalFournisseur
    {
        return $this->viewTotalFournisseur;
    }

    public function setViewTotalFournisseur(ViewTotalFournisseur $viewTotalFournisseur): self
    {
        // set the owning side of the relation if necessary
        if ($viewTotalFournisseur->getNomenclatureFournisseur() !== $this) {
            $viewTotalFournisseur->setNomenclatureFournisseur($this);
        }

        $this->viewTotalFournisseur = $viewTotalFournisseur;

        return $this;
    }

    public function getCategorieMarche(): ?CategorieMarche
    {
        return $this->categorieMarche;
    }

    public function setCategorieMarche(?CategorieMarche $categorieMarche): self
    {
        $this->categorieMarche = $categorieMarche;

        return $this;
    }
    public function getAnnee(): ?int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): self
    {
        $this->annee = $annee;

        return $this;
    }
    public static function loadValidatorMetadata(ClassMetadata $metadata){
         $metadata->addPropertyConstraint('factures', new Assert\Count([
             //'min' => 0,
             'max' => 0,
             'groups' => ['fac'],
             'exactMessage'=> 'Un ou plusieurs factures sont liées au fournisseur que vous avez tenté de supprimer. Opération interdite !',
         ]));
    }
}