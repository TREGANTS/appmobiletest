<?php

namespace App\Entity;

use App\Repository\FournisseurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * @ORM\Entity(repositoryClass=FournisseurRepository::class)
 * @UniqueEntity(fields={"numTiers"},message = "Ce Numéro de tiers est déjà utilisé, veuillez rectifier !")
 */
class Fournisseur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"jsonadd","jsonf"})
     */
    private $id;

    /**
     * @ORM\Column(type="bigint", unique=true)
     * @Groups({"jsonadd","jsonf"})
     */
    private $numTiers;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"jsonadd","jsonf"})
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity=NomenclatureFournisseur::class, mappedBy="fournisseur")
     */
    private $nomenclatureFournisseurs;

    /**
     * @ORM\OneToMany(targetEntity=Facture::class, mappedBy="fournisseur")
     */
    private $factures;

    /**
     * @ORM\Column(type="boolean",options={"default":"0"})
     * @Groups({"jsonadd"})
     */
    private $rejet;

    public function __construct()
    {
        $this->nomenclatureFournisseurs = new ArrayCollection();
        $this->factures = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumTiers(): ?int
    {
        return $this->numTiers;
    }

    public function setNumTiers(int $numTiers): self
    {
        $this->numTiers = $numTiers;

        return $this;
    }


    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }


    /**
     * @return Collection|NomenclatureFournisseur[]
     */
    public function getNomenclatureFournisseurs(): Collection
    {
        return $this->nomenclatureFournisseurs;
    }

    public function addNomenclatureFournisseur(NomenclatureFournisseur $nomenclatureFournisseur): self
    {
        if (!$this->nomenclatureFournisseurs->contains($nomenclatureFournisseur)) {
            $this->nomenclatureFournisseurs[] = $nomenclatureFournisseur;
            $nomenclatureFournisseur->setFournisseur($this);
        }

        return $this;
    }

    public function removeNomenclatureFournisseur(NomenclatureFournisseur $nomenclatureFournisseur): self
    {
        if ($this->nomenclatureFournisseurs->removeElement($nomenclatureFournisseur)) {
            // set the owning side to null (unless already changed)
            if ($nomenclatureFournisseur->getFournisseur() === $this) {
                $nomenclatureFournisseur->setFournisseur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Facture[]
     */
    public function getFactures(): Collection
    {
        return $this->factures;
    }

    public function addFacture(Facture $facture): self
    {
        if (!$this->factures->contains($facture)) {
            $this->factures[] = $facture;
            $facture->setFournisseur($this);
        }

        return $this;
    }

    public function removeFacture(Facture $facture): self
    {
        if ($this->factures->removeElement($facture)) {
            // set the owning side to null (unless already changed)
            if ($facture->getFournisseur() === $this) {
                $facture->setFournisseur(null);
            }
        }

        return $this;
    }

    /**
     * nomcomplet
     *
     * @return string
     */


    public function getNomcomplet(): ?string
    {
        return $this->getNom().' / '.' Numéro Tiers : '.$this->getNumTiers();
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new UniqueEntity([
            'fields' => ['numTiers'],
            'errorPath' => 'numTiers',
            'message' => 'Ce Numéro de tiers est déjà utilisé, veuillez rectifier !',
            'groups' => ['uniq'],
        ]));
        $metadata->addPropertyConstraint('factures', new Assert\Count([
            //'min' => 0,
            'max' => 0,
            'groups' => ['factures'],
            //'minMessage' => '',
            //'maxMessage' => 'Une ou plusieurs factures sont liées à ce fournisseur, vous ne pouvez donc pas le supprimer !',
            'exactMessage'=> 'Une ou plusieurs factures sont liées à ce fournisseur, vous ne pouvez donc pas le supprimer !',
        ]));

        $metadata->addPropertyConstraint('nomenclatureFournisseurs', new Assert\Count([
            //'min' => 0,
            'max' => 0,
            'groups' => ['nomenclaturefournisseur'],
            //'minMessage' => '',
            //'maxMessage' => 'Une ou plusieurs nomenclatures sont liées à ce fournisseur, vous ne pouvez donc le supprimer !',
            'exactMessage'=> 'Une ou plusieurs nomenclatures sont liées à ce fournisseur, vous ne pouvez donc pas le supprimer !',

        ]));
        $metadata->addPropertyConstraint('nomenclatureFournisseurs', new Assert\Count([
            //'min' => 0,
            'max' => 0,
            'groups' => ['rejetfournisseur'],
            //'minMessage' => '',
            //'maxMessage' => 'Une ou plusieurs nomenclatures sont liées à ce fournisseur, vous ne pouvez donc le supprimer !',
            'exactMessage'=> 'Une ou plusieurs nomenclatures sont liées à ce fournisseur, vous ne pouvez donc pas le rejeter !',

        ]));

        /*    $metadata->addPropertyConstraint('nomenclatures', new Assert\Count([
               //'min' => 0,
                'max' => 0,
                'groups' => ['nomencl'],
               //'minMessage' => '',
               //'maxMessage' => 'Une ou plusieurs nomenclatures sont liées à cette famille, vous ne pouvez donc le supprimer !',
                'exactMessage'=> 'Une ou plusieurs nomenclatures sont liées à ce fournisseur, vous ne pouvez donc pas le supprimer !',

      ]));*/
    }

    public function getRejet(): ?bool
    {
        return $this->rejet;
    }

    public function setRejet(bool $rejet): self
    {
        $this->rejet = $rejet;

        return $this;
    }


}