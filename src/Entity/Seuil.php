<?php

namespace App\Entity;

use App\Repository\SeuilRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Mapping\ClassMetadata;
/**
 * @ORM\Entity(repositoryClass=SeuilRepository::class)
 * @UniqueEntity(fields={"nom"},message = "Cette valeur est déjà utilisée, veuillez rectifier !")
 */
class Seuil
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups({"jsonadd"})
     */
    private $seuil;
    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups({"jsonadd"})
     */
    private $seuilAlerte;

    /**
     * @ORM\OneToMany(targetEntity=Nomenclature::class, mappedBy="seuil")
     */
    private $nomenclatures;

    /**
     * @ORM\Column(type="integer")
     */
    private $annee;

    public function __construct()
    {
        $this->nomenclatures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getSeuil(): ?int
    {
        return $this->seuil;
    }

    public function setSeuil(int $seuil): self
    {
        $this->seuil = $seuil;

        return $this;
    }

    public function getSeuilAlerte(): ?int
    {
        return $this->seuilAlerte;
    }

    public function setSeuilAlerte(int $seuilAlerte): self
    {
        $this->seuilAlerte = $seuilAlerte;

        return $this;
    }

    /**
     * @return Collection|Nomenclature[]
     */
    public function getNomenclatures(): Collection
    {
        return $this->nomenclatures;
    }

    public function addNomenclature(Nomenclature $nomenclature): self
    {
        if (!$this->nomenclatures->contains($nomenclature)) {
            $this->nomenclatures[] = $nomenclature;
            $nomenclature->setSeuil($this);
        }

        return $this;
    }

    public function removeNomenclature(Nomenclature $nomenclature): self
    {
        if ($this->nomenclatures->removeElement($nomenclature)) {
            // set the owning side to null (unless already changed)
            if ($nomenclature->getSeuil() === $this) {
                $nomenclature->setSeuil(null);
            }
        }

        return $this;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new UniqueEntity([
            'fields' => ['nom','annee'],
            'errorPath' => 'nom',
            'message' => 'Ce nom est déjà utilisé, veuillez rectifier !',
            'groups' => ['uniq'],
        ]));

        $metadata->addPropertyConstraint('nomenclatures', new Assert\Count([
            //'min' => 0,
            'max' => 0,
            'groups' => ['nomencl'],
            //'minMessage' => 'You must specify at least one email',
            //'maxMessage' => 'Une ou plusieurs nomenclatures sont liées à ce seuil, vous ne pouvez donc le supprimer !',
            'exactMessage'=> 'Une ou plusieurs nomenclatures sont liées à ce seuil, vous ne pouvez donc pas le supprimer !',
        ]));
    }

    public function getAnnee(): ?int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

}
