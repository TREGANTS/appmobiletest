<?php

namespace App\Repository;

use App\Entity\ViewFacturesMontant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ViewFacturesMontant|null find($id, $lockMode = null, $lockVersion = null)
 * @method ViewFacturesMontant|null findOneBy(array $criteria, array $orderBy = null)
 * @method ViewFacturesMontant[]    findAll()
 * @method ViewFacturesMontant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ViewFacturesMontantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ViewFacturesMontant::class);
    }

    // /**
    //  * @return ViewFacturesMontant[] Returns an array of ViewFacturesMontant objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ViewFacturesMontant
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
