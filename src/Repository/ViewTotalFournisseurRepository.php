<?php

namespace App\Repository;

use App\Entity\ViewTotalFournisseur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ViewTotalFournisseur|null find($id, $lockMode = null, $lockVersion = null)
 * @method ViewTotalFournisseur|null findOneBy(array $criteria, array $orderBy = null)
 * @method ViewTotalFournisseur[]    findAll()
 * @method ViewTotalFournisseur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ViewTotalFournisseurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ViewTotalFournisseur::class);
    }

    // /**
    //  * @return ViewTotalFournisseur[] Returns an array of ViewTotalFournisseur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ViewTotalFournisseur
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
