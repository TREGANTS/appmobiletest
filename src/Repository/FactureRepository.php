<?php

namespace App\Repository;

use App\Entity\Facture;
use App\Entity\NomenclatureFournisseur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\ParameterType;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Facture|null find($id, $lockMode = null, $lockVersion = null)
 * @method Facture|null findOneBy(array $criteria, array $orderBy = null)
 * @method Facture[]    findAll()
 * @method Facture[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FactureRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry,NomenclatureFournisseurRepository $nomenclatureFournisseurRepository)
    {
        parent::__construct($registry, Facture::class);


    }

    public function findAllValid()
    {

        return $this->createQueryBuilder('f')
            ->innerJoin('App\Entity\Fournisseur', 'fr', 'WITH', 'f.fournisseur = fr.id')
            ->andWhere('fr.rejet = :val')
            ->setParameter('val', 'false')
            ->groupBy('f.id')
            ->orderBy('f.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findFacturesAnFr($fournisseur,$annee)
    {

        return $this->createQueryBuilder('f')
            ->innerJoin('App\Entity\Fournisseur', 'fr', 'WITH', 'f.fournisseur = fr.id')
            ->andWhere('fr.id = :val')
            ->andWhere('fr.rejet = :val1')
            ->andWhere('f.annee = :val2')
            ->setParameter('val', $fournisseur)
            ->setParameter('val1', 'false')
            ->setParameter('val2', $annee)
            ->groupBy('f.id')
            ->orderBy('f.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByAnnee($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.annee = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }
    public function findtest($fournisseur,$annee)
    {

        return $this->createQueryBuilder('f')
            ->innerJoin('App\Entity\Fournisseur', 'fr', 'WITH', 'f.fournisseur = fr.id')
            ->andWhere('fr.id = :val')
            ->andWhere('fr.rejet = :val1')
            ->andWhere('f.annee = :val2')
            ->setParameter('val', $fournisseur)
            ->setParameter('val1', 'false')
            ->setParameter('val2', $annee)
            ->groupBy('f.id')
            ->orderBy('f.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findByNF(NomenclatureFournisseur $nomenclatureFournisseur){
        //recherche de toutes les factures liées à un couple nomenclaturefournisseur
        //recherce sur l'id nomenclature fournisseur

        $qb=$this->createQueryBuilder('f')
            ->andWhere('f.nomenclatureFournisseur = :val')
            ->setParameter('val',$nomenclatureFournisseur)
            ->orderBy('f.datefacture', 'DESC')
            ->getQuery()
            ->getResult();
     return $qb;
    }
    public function montantFacturesFournisseurs()
    {

        $connexion = $this->getEntityManager()->getConnection();

        $sql = " select fournisseur_id as id, num_tiers as numero,nom,sum(total3ans) as montantcumul ,sum(total) as montantannuel    ";
        $sql=$sql ."  from ";
        $sql=$sql ."   ( ";
        $sql=$sql ."     select  f.fournisseur_id,fr.num_tiers,fr.nom, sum(montant) as total3ans, 0 as total from facture f ,fournisseur fr where f.fournisseur_id = fr.id and fr.rejet = false  and date_part('year',datefacture) > (date_part('year', CURRENT_DATE) -3) group by f.fournisseur_id,fr.num_tiers,fr.nom ";
        $sql=$sql ."      union ";
        $sql=$sql ."     select  f.fournisseur_id,fr.num_tiers,fr.nom, 0 as total3ans,sum(montant) as  total from facture f ,fournisseur fr  where f.fournisseur_id = fr.id and fr.rejet = false and date_part('year',datefacture) = (date_part('year', CURRENT_DATE) ) group by f.fournisseur_id,fr.num_tiers,fr.nom ";
        $sql=$sql ."   ) ";
        $sql=$sql ." as tmp ";
        $sql=$sql ." group by fournisseur_id ,num_tiers,nom ";
        $sql=$sql ." order by montantcumul desc ";

        $stat= $connexion->prepare($sql);

        return($stat->executeQuery()->fetchAllAssociative());


    }

}
