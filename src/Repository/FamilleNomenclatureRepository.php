<?php

namespace App\Repository;

use App\Entity\FamilleNomenclature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FamilleNomenclature|null find($id, $lockMode = null, $lockVersion = null)
 * @method FamilleNomenclature|null findOneBy(array $criteria, array $orderBy = null)
 * @method FamilleNomenclature[]    findAll()
 * @method FamilleNomenclature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FamilleNomenclatureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FamilleNomenclature::class);
    }

    public function findByAnnee($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.annee = :val')
            ->setParameter('val', $value)
            ->orderBy('s.intitule', 'ASC')
            ->getQuery()
            ->getResult()
            ;

    }

}
