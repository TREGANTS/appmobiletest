<?php

namespace App\Repository;

use App\Entity\CategorieMarche;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CategorieMarche|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategorieMarche|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategorieMarche[]    findAll()
 * @method CategorieMarche[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategorieMarcheRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategorieMarche::class);
    }

    public function findAll1(){
        //attention même requête que findAll mais renvoie le querybuilder
        //pour nomenclatureFournisseurType
        $qb = $this->createQueryBuilder('cm')
            ->select('cm')
            ->addOrderBy('cm.nom', 'ASC');

        return ($qb);
    }

}
