<?php

namespace App\Repository;

use App\Entity\Nomenclature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\ParameterType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Nomenclature|null find($id, $lockMode = null, $lockVersion = null)
 * @method Nomenclature|null findOneBy(array $criteria, array $orderBy = null)
 * @method Nomenclature[]    findAll()
 * @method Nomenclature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NomenclatureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Nomenclature::class);
    }

//permet de ne recuperer que les nomenclatures pour lesquelles il y a une facture
//sur l'annee spécifiée
    /**
     * @param Int $annee
     * @return [] Returns an array
     */
     public function findByAnWithTotaux(Int $annee){
//attention cette requete renvoie un tableau simple et pas un tableau d'objets
    $qb= $this->createQueryBuilder('n')
           ->select('n.numero','n.id','n.annee','n.detailIntitule','n.intitule')
          ->andWhere('n.annee= :annee')
            ->setParameter('annee', $annee)
           ->join('n.viewTotalNomac', 'vw')
           ->join ('n.seuil','s')
           ->join('n.famille','f')
           ->addSelect('f.intitule as famille')
           ->addSelect('s.id as seuilid')
           ->addSelect('s.seuilAlerte')
           ->addSelect('s.seuil')
           ->addSelect('s.nom as nomseuil')
           ->addSelect('vw.total')

           ->getQuery()
           ->getResult();
//dd($qb);
return $qb;
/*version en requête raw
         $conn = $this->getEntityManager()
             ->getConnection();
         $sql = 'SELECT n0_.id AS id,
	n0_.numero AS numero,
	n0_.intitule AS intitule,
	n0_.detail_intitule AS detailintitule,
	n0_.annee AS annee,

	f0.intitule AS famille,
    v1_.total AS total,
    s0.seuil as seuil

	        FROM nomenclature n0_ INNER JOIN view_total_nomac v1_ ON n0_.id = v1_.nomenclature_id
            INNER JOIN seuil s0 on s0.id=n0_.seuil_id
            INNER JOIN famille_nomenclature f0 on f0.id=n0_.famille_id
	    WHERE n0_.annee = :annee order by famille,numero';



         $stmt = $conn->prepare($sql);
         $stmt->execute(array('annee' => $annee));
return $stmt->fetchAll();*/
     }

    /**
     * @param Int $annee
     * @return Nomenclature[] Returns an array of Nomenclature objects
     */
    public function findByAnnee(Int $annee){
  /*    $qb=$this->createQueryBuilder('nomac')
//          ->addSelect('nomac.numero as HIDDEN mysort')
          ->andWhere('nomac.annee= :annee')
            ->setParameter('annee', $annee)
          ->orderBy('nomac.numero','ASC')
//            ->orderBy('mysort','ASC')
     //     ->orderBy('nomac.intitule','ASC')
            ->getQuery()
           ->getResult();
            return $qb;*/


        return $this->getEntityManager()->createQuery('
      SELECT n
      FROM App\Entity\Nomenclature n
      WHERE n.annee=:annee
      ORDER BY n.intitule ASC')->setParameter('annee',$annee)->getResult();
    }

    public function findForExcel(Int $annee){
        //impression à partir du tableau des resultats
    //requete raw
        //renvoie un array d'array
        $conn = $this->getEntityManager()->getConnection();
        $sql='SELECT
                    f0.intitule AS famille,
                    n0_.numero AS numnomenclature,
                    n0_.intitule AS intitule,
                    s0.seuil as seuil,
                    v1_.total AS totalnomenclature,
                   fo0.num_tiers as numtiers,
                    fo0.nom as fournisseur,
                    vf.total as totalfournisseur

                FROM nomenclature n0_ INNER JOIN view_total_nomac v1_ ON n0_.id = v1_.nomenclature_id
                INNER JOIN seuil s0 on s0.id=n0_.seuil_id
                INNER JOIN famille_nomenclature f0 on f0.id=n0_.famille_id
               INNER JOIN nomenclature_fournisseur nf0 on nf0.nomenclature_id=n0_.id and nf0.annee= :annee
                INNER JOIN fournisseur fo0 on fo0.id=nf0.fournisseur_id
                INNER JOIN view_total_fournisseur vf on vf.annee= :annee and vf.nomenclature_fournisseur_id=nf0.id
                where  n0_.annee = :annee  order by famille,numNomenclature';



       $stat= $conn->prepare($sql);
       $stat->bindValue('annee',$annee,ParameterType::INTEGER);
       return($stat->executeQuery()->fetchAllAssociative());

    }
}
