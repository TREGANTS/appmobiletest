<?php

namespace App\Repository;

use App\Dataclass\AnneeData;
use App\Entity\ViewTotalNomac;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ViewTotalNomac|null find($id, $lockMode = null, $lockVersion = null)
 * @method ViewTotalNomac|null findOneBy(array $criteria, array $orderBy = null)
 * @method ViewTotalNomac[]    findAll()
 * @method ViewTotalNomac[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ViewTotalNomacRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ViewTotalNomac::class);
    }

    /**
     * @return AnneeData[]
     */
    public function findAnneesExercice()
    {
        $qb=$this->createQueryBuilder('vw')
            ->select('DISTINCT vw.annee')
            ->getQuery()
            ->getResult();
        //$qb tableau de tableau
        $result=[];
        $i=0;
        foreach($qb as $elt){
            $anneedata=new AnneeData();
            $anneedata->annee=$elt['annee'];
            $result[$i]=$anneedata;
            $i+=1;
        }
       // var_dump($result);
        return ($result);
    }

    // /**
    //  * @return ViewTotalNomac[] Returns an array of ViewTotalNomac objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ViewTotalNomac
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
