<?php

namespace App\Repository;


use App\Entity\NomenclatureFournisseur;
use Cnam\TwigBootstrapBundle\Table\QueryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NomenclatureFournisseur|null find($id, $lockMode = null, $lockVersion = null)
 * @method NomenclatureFournisseur|null findOneBy(array $criteria, array $orderBy = null)
 * @method NomenclatureFournisseur[]    findAll()
 * @method NomenclatureFournisseur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NomenclatureFournisseurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NomenclatureFournisseur::class);
    }

    public function findByAnnee(QueryInterface $tableQuery){

        $queryBuilder = $this->createQueryBuilder('nf')
            ->select('nf.id','nf.numMarche','nf.typeMarche','nf.obs1')
            ->innerjoin('nf.fournisseur', 'ff')
            ->innerJoin('nf.viewTotalFournisseur','vf')
            ->leftJoin('nf.categorieMarche','cm')
            ->addSelect('vf.total')
            ->addSelect('ff.nom','ff.numTiers')
            ->addSelect('cm.nom as categorie')
        ;
    /*    $queryBuilder->andWhere('nf.active= :active')
                    ->setParameter('active','true');*/
        if ($tableQuery->getSearch()) {
            $queryBuilder->andWhere('nf.nomenclature = :search')
                ->setParameter('search', $tableQuery->getSearch())
            ;
        }

        if ($tableQuery->hasAdditionalParameters('annee')){
            $queryBuilder->andWhere('vf.annee = :annee')
                -> setParameter('annee',$tableQuery->getAdditionalParameters("annee"));
        }

        return $queryBuilder->getQuery()->getResult();

    }


  /*  public function findByAnnee2(){
        $qb = $this->createQueryBuilder('n')
            ->select('n')
            ->innerJoin('n.viewTotalFournisseur','vw')
            ->addSelect('vw')
//           ->select('n.numero','n.id','n.annee','n.detailIntitule','n.intitule')
            ->getQuery()
            ->getResult();
        dd($qb);

    }*/

}
