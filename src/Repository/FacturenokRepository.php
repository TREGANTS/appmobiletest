<?php

namespace App\Repository;

use App\Entity\Facturenok;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\ParameterType;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Facturenok|null find($id, $lockMode = null, $lockVersion = null)
 * @method Facturenok|null findOneBy(array $criteria, array $orderBy = null)
 * @method Facturenok[]    findAll()
 * @method Facturenok[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FacturenokRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Facturenok::class);
    }

    public function findFacturesFrnOk()
    {

        return $this->createQueryBuilder('f')
            ->innerJoin('App\Entity\Fournisseur', 'fr', 'WITH', 'f.fournisseur = fr.id')
            ->andWhere('fr.rejet = :val')
            ->setParameter('val', 'false')
            ->groupBy('f.id')
            ->orderBy('f.id', 'ASC')
            ->getQuery()
            //->getDQL()
            ->getResult()
            ;
    }

    public function findlistFacturesnok($facturesnokid){

        return $this->createQueryBuilder('f')
                ->andWhere('f.id IN(:id)')
                ->setParameter('id', $facturesnokid)
                ->orderBy('f.id', 'ASC')
                ->getQuery()
                ->getResult()
        ;
        ;
    }

    public function recupMaxdate(){

        $connexion = $this->getEntityManager()->getConnection();
        $sql = " select to_char((MAX(dateimport) - interval '3 months' ),'YYYY-MM-DD') as datemin from facturenok";

        //$statement = $connexion->executeQuery($sql);
       // return $statement->fetchAll();

        $stat= $connexion->prepare($sql);
        return($stat->executeQuery()->fetchAllAssociative());
    }

    public function nbFactFrnRejetByDatemax($datemax)
    {

        $connexion = $this->getEntityManager()->getConnection();

        $sql = " select count (*) as nbrecords from facturenok where fournisseur_id in (select id from fournisseur where rejet = true) and dateimport <=:datemax";
        //$statement = $connexion->executeQuery($sql);
        //return $statement->fetchAll();
        $stat= $connexion->prepare($sql);
        $stat->bindValue('datemax',$datemax,ParameterType::STRING);
        return($stat->executeQuery()->fetchAllAssociative());
    }

    public function delFactFrnRejetByDatemax($datemax)
    {

        $connexion = $this->getEntityManager()->getConnection();
        $sql = " delete from facturenok where fournisseur_id in (select id from fournisseur where rejet = true) and dateimport <=:datemax";
        //$statement = $connexion->executeQuery($sql);
        //  return $statement->rowCount();
        $stat= $connexion->prepare($sql);
        $stat->bindValue('datemax',$datemax,ParameterType::STRING);
        return($stat->executeQuery()->rowCount());
    }



    public function findFrnRejetByDatemax($datemax)
    {

        return $this->createQueryBuilder('f')
            ->innerJoin('App\Entity\Fournisseur', 'fr', 'WITH', 'f.fournisseur = fr.id')
            ->andWhere('fr.rejet = :val')
            ->andWhere('f.dateimport >= :val1')
            ->setParameter('val', 'true')
            ->setParameter('val1', $datemax)
            ->groupBy('f.id')
            ->orderBy('f.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

}
