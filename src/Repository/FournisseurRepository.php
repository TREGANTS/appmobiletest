<?php

namespace App\Repository;

use App\Entity\Fournisseur;
use Cnam\TwigBootstrapBundle\Table\QueryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Fournisseur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Fournisseur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Fournisseur[]    findAll()
 * @method Fournisseur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FournisseurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Fournisseur::class);
    }


    public function findByTableQuery(QueryInterface $tableQuery): Paginator{
        $queryBuilder = $this->createQueryBuilder('f')
            ->leftJoin('f.nomenclatureFournisseurs','nf')
            ->Select('nf','f');

        if ($tableQuery->getSearch()) {
            $queryBuilder->andWhere('nf.nomenclature = :idN AND nf.annee=2020')
                ->setParameter('idN', $tableQuery->getSearch())
            ;
        }
        $queryBuilder->setMaxResults($tableQuery->getLimit());
        $queryBuilder->setFirstResult($tableQuery->getOffset());
        return new Paginator($queryBuilder->getQuery());
    }

    public function findAllNonRejetes(){
        //attention même requête que finffournisseurvalid mais renvoie le querybuilder
        //pour nomenclatureFournisseurType
        $qb = $this->createQueryBuilder('f')
            ->select('f')
            ->andWhere('f.rejet=false')
            ->addOrderBy('f.nom', 'ASC');

        return ($qb);
    }

    public function findFournisseurValid()
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.rejet = :val1')
            ->setParameter('val1', 'false')
            ->orderBy('f.nom', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAllAsc()
    {

       return $this->findBy(array(), array('nom' => 'ASC'));
    }

}
