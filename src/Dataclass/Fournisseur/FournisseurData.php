<?php

namespace App\Dataclass\Fournisseur;

use Symfony\Component\Validator\Constraints as Assert;

class FournisseurData
{
    /**
     * @Assert\NotBlank()
     */
    public $numTiers;
    /**
     * @Assert\NotBlank()
     */
    public $nom;

    public $rejet;

}