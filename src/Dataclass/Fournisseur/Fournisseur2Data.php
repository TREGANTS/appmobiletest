<?php

namespace App\Dataclass\Fournisseur;

use Symfony\Component\Validator\Constraints as Assert;

class Fournisseur2Data
{
    /**
     * @Assert\NotBlank()
     */
    public $fournisseur;
    /**
     * @Assert\NotBlank()
     */
    public $annee;



}