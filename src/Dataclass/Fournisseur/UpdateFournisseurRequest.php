<?php


namespace App\Dataclass\Fournisseur;

use App\Entity\Fournisseur;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateFournisseurRequest
{

    /**
     * @Assert\NotBlank()
     */
    public $numTiers;
    /**
     * @Assert\NotBlank()
     */
    public $nom;

    public $rejet;


    public static function fromFournisseur(Fournisseur $fournisseur): self
    {
        $fournisseurRequest = new self();
        $fournisseurRequest->numTiers= $fournisseur->getNumTiers();
        $fournisseurRequest->nom = $fournisseur->getNom();
        $fournisseurRequest->rejet= $fournisseur->getRejet();

        return $fournisseurRequest;
    }
}