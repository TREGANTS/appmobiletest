<?php


namespace App\Dataclass\Facturenok;


use App\Entity\Facture;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateFacturenokRequest
{
    /**
     * @Assert\NotBlank()
     */
    private $annee;
    /**
     * @Assert\NotBlank()
     */
    private $fournisseur;

    /**
     * @Assert\NotBlank()
     */
    private $montant;

    private $cheminDoc;

    private $nomenclatureFournisseur;

    /**
     * @Assert\NotBlank()
     */
    private $dateimport;

    /**
     * @Assert\NotBlank()
     */
    private $organisme;

    private $compte;

    private $description;
    /**
     * @Assert\NotBlank()
     */
    private $quantieme;

    private $datefacture;


    public static function fromFacture(Facture $facturenok): self
    {
        $facturenokRequest = new self();

        $facturenokRequest->annee= $facturenok->getAnnee();
        $facturenokRequest->fournisseur= $facturenok->getFournisseur();
        $facturenokRequest->montant= $facturenok->getMontant();
        $facturenokRequest->cheminDoc=$facturenok->getCheminDoc();
        $facturenokRequest->nomenclatureFournisseur=$facturenok->getNomenclatureFournisseur();
        $facturenokRequest->dateimport=$facturenok->getDateimport();
        $facturenokRequest->organisme=$facturenok->getOrganisme();
        $facturenokRequest->compte=$facturenok->getCompte();
        $facturenokRequest->description=$facturenok->getDescription();
        $facturenokRequest->quantieme=$facturenok->getQuantieme();
        $facturenokRequest->datefacture=$facturenok->getDatefacture();
        return $facturenokRequest;
    }
}