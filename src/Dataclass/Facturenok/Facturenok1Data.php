<?php

namespace App\Dataclass\Facturenok;

use Symfony\Component\Validator\Constraints as Assert;

class Facturenok1Data
{
    /**
     * @Assert\NotBlank(message="L'indication d'un fournisseur est obligatoireeee")
     * @Assert\Valid()
     */
    public $fournisseur;

    /**
     * @Assert\NotBlank(message="L'indication d'une nomenclature est obligatoireee")
     */
    public $nomenclature;

}