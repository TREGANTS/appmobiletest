<?php

namespace App\Dataclass\FamilleNomenclature;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;

class FamilleNomenclatureData
{
    /**
     * @Assert\NotBlank()
     */
    public $intitule;


}