<?php

namespace App\Dataclass\FamilleNomenclature;

use App\Entity\FamilleNomenclature;
use Symfony\Component\Validator\Constraints as Assert;
class UpdateFamilleNomenclatureRequest
{
    /**
     * @Assert\NotBlank()
     */
    public $intitule;



    public static function fromFamilleNomenclature(FamilleNomenclature $familleNomenclature): self
    {
        $familleNomenclatureRequest = new self();
        $familleNomenclatureRequest->intitule = $familleNomenclature->getIntitule();

        return $familleNomenclatureRequest;
    }
}


