<?php


namespace App\Dataclass\Seuil;

use App\Entity\Seuil;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateSeuilRequest
{

      /**
     * @Assert\NotBlank()
     */
    public $nom;
    /**
     * @Assert\NotBlank()
     */
    public $seuil;

    /**
     * @Assert\NotBlank()
     */
    public $seuilAlerte;


    public static function fromSeuil(Seuil $seuil): self
    {
        $seuilRequest = new self();
        $seuilRequest->nom = $seuil->getNom();
        $seuilRequest->seuil = $seuil->getSeuil();
        $seuilRequest->seuilAlerte= $seuil->getSeuilAlerte();
        return $seuilRequest;
    }
}