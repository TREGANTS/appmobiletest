<?php

namespace App\Dataclass\Seuil;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

class SeuilData
{
    /**
     * @Assert\NotBlank()
     */
    public $nom;
    /**
      * @Assert\NotBlank()
      */
    public $seuil;

    /**
     * @Assert\NotBlank()
     * @Assert\LessThan(propertyPath="seuil", message="La valeur du seuil d'alerte doit être inférieure  à celle du seuil")
     */
    public $seuilAlerte;


}
