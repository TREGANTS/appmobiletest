<?php
namespace App\Tools;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class outils
{
    public static function violations_to_array(ConstraintViolationListInterface $violationsList, $propertyPath = null)
    {
        //transformation en tableau associatif
        $output = array();

        foreach ($violationsList as $violation) {
            $output[$violation->getPropertyPath()][] = $violation->getMessage();
        }

        if (null !== $propertyPath) {
            if (array_key_exists($propertyPath, $output)) {
                $output = array($propertyPath => $output[$propertyPath]);
            } else {
                return array();
            }
        }

        return $output;
    }
}