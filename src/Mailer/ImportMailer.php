<?php

namespace App\Mailer;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
class ImportMailer
{
   /*
    private MailerInterface $mailer;
    private ParameterBagInterface $params;
   */
    private  $mailer;
    private  $params;
    public function __construct(MailerInterface $mailer,ParameterBagInterface $params)
    {
        $this->mailer = $mailer;
        $this->params= $params;
    }

    public function sendReport(array $summary): void
    {

        $email = (new TemplatedEmail())

           // ->from('root@assurance-maladie.fr')
            // ->to('eric.noirot@assurance-maladie.fr','ennio311@gmail.com')
            ->from($_SERVER['MAILER_FROM'])
            ->to($_SERVER['MAILER_DEST'])
            ->subject('NOMAC : Rapport Import des factures')
            ->htmlTemplate('emails/import/report.html.twig')
            ->context([
                'summary' => $summary,
            ])
        ;

        $this->mailer->send($email);
    }


    public function sendReportPurge(array $summary): void
    {

        $email = (new TemplatedEmail())

            ->from($_SERVER['MAILER_FROM'])
            ->to($_SERVER['MAILER_DEST'])
            ->subject('NOMAC : Rapport Purge mensuelle des factures de fournisseurs rejetés')
            ->htmlTemplate('emails/import/reportpurge.html.twig')
            ->context([
                'summary' => $summary,
            ])
        ;

        $this->mailer->send($email);
    }



}
