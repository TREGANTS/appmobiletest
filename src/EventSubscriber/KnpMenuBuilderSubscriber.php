<?php
namespace App\EventSubscriber;

use KevinPapst\AdminLTEBundle\Event\KnpMenuEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;

class KnpMenuBuilderSubscriber implements EventSubscriberInterface
{
    private $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            KnpMenuEvent::class => ['onSetupMenu', 100],
        ];
    }
    public function onSetupMenu(KnpMenuEvent $event)
    {
       $menu = $event->getMenu();

        $menu->addChild('accueil', [
            'route' => 'home',
            'label' => 'Accueil',
            'childOptions' => $event->getChildOptions()
        ])->setLabelAttribute('icon', 'fas fa-home');

        $menu->addChild('menu_header_consultation', [
            'label' => 'CONSULTATION',
            'childOptions' => $event->getChildOptions()
        ])->setAttribute('class', 'header');

        $menu->addChild('tableau_achats', [
            'route' => 'resultat_index',
            'routeParameters'=>['an'=>date('Y')],
            'label' => 'Tableau des achats',
            'childOptions' => $event->getChildOptions()
            //     ])->setAttributes(['icon'=>'fas fa-mail']);
        ])->setLabelAttribute('icon', 'fas fa-list-alt');

        $menu->addChild('achats_frn_rejet', [
            'route' => 'factures_frn',
            'label' => 'Factures fournisseurs éliminés',
            'childOptions' => $event->getChildOptions()
        ])->setLabelAttribute('icon', 'fas fa-unlink');

        $menu->addChild('Détail_factures', [
            'route' => 'factures_detail',
            'label' => 'Factures par fournisseur',
            'childOptions' => $event->getChildOptions()
        ])->setLabelAttribute('icon', 'fas fa-money-check');

        if ($this->security->isGranted('ROLE_GEST')or $this->security->isGranted('ROLE_UTIL') ) {

            $menu->addChild('facturenok2', [
                'route' => 'facturenok_index',
                'label' => 'Factures a intégrer',
                'childOptions' => $event->getChildOptions()
            ])->setLabelAttribute('icon', 'fas fa-money-check');
        }
        $menu->addChild('Montant_fournisseurs', [
            'route' => 'factures_fournisseurs_montant',
            'label' => 'Montant Factures par fournisseur',
            'childOptions' => $event->getChildOptions()
        ])->setLabelAttribute('icon', 'fas fa-money-check');

        if ($this->security->isGranted('ROLE_ADMIN')or $this->security->isGranted('ROLE_GEST')) {

            $menu->addChild('administration', [
                'label' => 'ADMINISTRATION',
                'childOptions' => $event->getChildOptions()
            ])->setAttribute('class', 'header')
               ->setLabelAttribute('icon', 'fas fa-cogs');


           $menu->addChild('famillenomenclature', ['label' => 'Familles','route' => 'famille_nomenclature_index'])
               ->setLabelAttribute('icon',  'fas fa-list');
           $menu->addChild('nomenclature', [
               'label' => 'Nomenclatures',
               'route' => 'nomenclature_index',
               'childOptions' => $event->getChildOptions(),
               'routeParameters'=>['exercice'=>date('Y')]]) ->setLabelAttribute('icon',  'fas fa-list');
           $menu->addChild('fournisseur', [
               'label' => 'Fournisseurs',
               'route' => 'fournisseur_index',
               ]
           )->setLabelAttribute('icon', 'fas fa-users');
           $menu->addChild('seuil', ['label' => 'Seuils','route' => 'seuil_index']) ->setLabelAttribute('icon',  'fas fa-list');;
           $menu->addChild('facture', ['label' => 'Factures','route' => 'facture_index']) ->setLabelAttribute('icon',  'fas fa-money-check');;
           $menu->addChild('facturenok', ['label' => 'Factures a intégrer','route' => 'facturenok_index']) ->setLabelAttribute('icon',  'fas fa-money-check');;
           $menu->addChild('categoriemarche', ['label' => 'Catégories de Marché','route' => 'categorie_marche_index']) ->setLabelAttribute('icon',  'fas fa-list');;
        }
    }


}