<?php


namespace App\EventSubscriber;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class RedirectBadRoutingToHomepageSubscriber implements EventSubscriberInterface
{
    private $router;

    public function __construct(RouterInterface $router)
    {

        $this->router=$router;
    }
    public static function getSubscribedEvents()
    {
        // TODO: Implement getSubscribedEvents() method.
        return [
            KernelEvents::EXCEPTION => ['onKernelException', 2],
        ];
    }

    public function onKernelException(ExceptionEvent $event)
    {

        //  dd($exception);
        //  $exception = $event->getException();
        $exception=$event->getThrowable();
       /* dump($exception);
        die;*/
        if (!$exception instanceof NotFoundHttpException && !$exception instanceof AccessDeniedException) {
                 return;
        }

             $session = $event->getRequest()->getSession();
             $message = $exception instanceof NotFoundHttpException ? "L'adresse URL renseignée n'est pas reconnue" : $exception->getMessage();
             $session->getFlashBag()->set('danger', $message);

             $response = new RedirectResponse(
                 $this->router->generate('homepage')

             );

             $event->setResponse($response);
         }

}