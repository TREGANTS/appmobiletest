<?php
namespace App\EventSubscriber;
use App\Security\Userttttt;
//use cnam\authsrvsec-bundle\Security\User\User;
use KevinPapst\AdminLTEBundle\Event\ShowUserEvent;
use KevinPapst\AdminLTEBundle\Event\NavbarUserEvent;
use KevinPapst\AdminLTEBundle\Event\SidebarUserEvent;
use KevinPapst\AdminLTEBundle\Model\UserModel;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;

class NavbarUserSubscriber implements EventSubscriberInterface
{
    protected $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            NavbarUserEvent::class => ['onShowUser', 100],
            SidebarUserEvent::class => ['onShowUser', 100],
        ];
    }

    public function onShowUser(ShowUserEvent $event)
    {

        if (null === $this->security->getUser()) {
            return;
        }



        $myUser = $this->security->getUser();

//dd($myUser);
        $user = new UserModel();

//dd($myUser->getIdentifiant());
        $user
            //->setId($myUser->getId())
            ->setId($myUser->getIdentifiant())
            ->setName($myUser->getNom().' '.$myUser->getPrenom())
//            ->setName($myUser->getUsername())
            ->setUsername($myUser->getUsername())
            ->setIsOnline(true)
            ->setTitle('demo user')

        //    ->setAvatar($myUser->getAvatar())
       //     ->setMemberSince($myUser->getRegisteredAt())
        ;
//dd($user);
        $event->setUser($user);
        $event->setShowLogoutLink(false);
        $event->setShowProfileLink(true);


    }


}