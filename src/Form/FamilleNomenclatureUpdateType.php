<?php

namespace App\Form;

# use App\Entity\FamilleNomenclature;
#use App\FamilleNomenclature\FamilleNomenclatureData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Dataclass\FamilleNomenclature\UpdateFamilleNomenclatureRequest;

class FamilleNomenclatureUpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('intitule',TextType::class,array(
                'label' => 'Intitulé',
            ))
        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UpdateFamilleNomenclatureRequest::class,
        ]);
    }

}
