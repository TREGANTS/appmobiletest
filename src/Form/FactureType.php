<?php

namespace App\Form;

use App\Entity\Facture;
use App\Form\Type\Nomenclature1Type;
use App\Form\Type\Fournisseur1Type;
#use Symfony\Bridge\Doctrine\Form\Type\EntityType;
#use App\Entity\Fournisseur;
use App\Form\Type\NomenclatureFournisseur1Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class FactureType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $facture  = new Facture();
        # recup l'objet facture à partir  du tableau $options

        $facture = $options['data'];

        $builder

            ->add('fournisseur',Fournisseur1Type::class,[
                    # récup identiant fournisseur
                    'fournisseur_id'=>  $facture->getFournisseur()->getId(),
                    'disabled' => true
                ]
            )

            ->add('nomenclatureFournisseur',NomenclatureFournisseur1Type::class,[
                    # récup identiant fournisseur
                    'fournisseur_id'=>  $facture->getFournisseur()->getId(),
                   // 'datefacture'=>  $facture->getDatefacture(),
                    'annee'=>  $facture->getAnnee(),
                    //'multiple' => false,
                    //'expanded' => false,
                    'required' => true,
                    'placeholder' => '-- Choisir une nomenclature --'


                ]
            )

            ->add('annee',IntegerType::class,[
                    'disabled' => true
                ]
            )
            ->add('montant',MoneyType::class,[
                    'disabled' => true
                ]
            )


            ->add('dateimport',DateType::class,[
                    'label' => 'Date d\'import',
                    'widget' => 'single_text',
                    'disabled' => true
                ]
            )

        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Facture::class,
        ]);
    }
}
