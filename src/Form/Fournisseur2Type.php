<?php

namespace App\Form;

use App\Dataclass\Fournisseur\Fournisseur2Data;
use App\Entity\Fournisseur;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Fournisseur2Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fournisseur', EntityType::class, [
                'class' => Fournisseur::class,
                'choice_label' => function ($fournisseur) {
                    return $fournisseur->getNom().' - ('.$fournisseur->getNumTiers().')' ;},
                'multiple' => false,
                'expanded' => false,
                'required' => false,
                'placeholder'=>'-- Choisir le fournisseur --',

            ])
            ->add('annee',ChoiceType::class, [
                 'choices'  => [
                     Date('Y') => Date('Y'),
                     Date('Y') - 1 => Date('Y') - 1,
                     Date('Y') - 2 => Date('Y') - 2,
                     Date('Y') - 3 => Date('Y') - 3,
                     Date('Y') - 4 => Date('Y') - 4,
                 ]
               ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Fournisseur2Data::class,
        ]);
    }
}
