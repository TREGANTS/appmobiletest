<?php

namespace App\Form;

use App\Dataclass\Facturenok\Facturenok1Data;
use App\Entity\Facturenok;
use App\Entity\Fournisseur;
use App\Form\Type\NomenclatureFournisseur1Type;
use App\Form\Type\Fournisseur1Type;
use App\Form\Type\FacturenokFrnType;
use App\Form\Type\Nomenclature1Type;
#use Symfony\Bridge\Doctrine\Form\Type\EntityType;
#use App\Entity\Fournisseur;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;
// 1. Use the CategoriesToNumbersTransformer
# use App\Form\DataTransformer\NomenclaturesToNumbersTransformer;

class FournisseurFacturenok2Type extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $fournisseur  = new Fournisseur();

        # recupération id fournisseur
        $idfournisseur = $options['data']['idfournisseur'];
        # récupération année
        $annee= $options['data']['annee'];
       // dd($idfournisseur,$annee,$facturenok);
        $builder

            ->add('fournisseur',Fournisseur1Type::class,[
                   'fournisseur_id'=> $idfournisseur,
                    'disabled' => false
                ]
            )

            ->add('nomenclatureFournisseur',NomenclatureFournisseur1Type::class,[
                'fournisseur_id'=> $idfournisseur,
                'annee'=>  $annee,
                //'multiple' => false,
                //'expanded' => false,
                'label' =>'Nomenclature',
                'required' => true,
                'placeholder' => '-- Choisir une nomenclature --'


                ]
            )
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           // 'data_class' => Facturenok1Data::class,
        ]);
    }
}
