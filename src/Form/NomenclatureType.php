<?php

namespace App\Form;

use App\Entity\FamilleNomenclature;

use App\Entity\Nomenclature;
use App\Entity\Seuil;
use App\Repository\FamilleNomenclatureRepository;
use App\Repository\SeuilRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NomenclatureType extends AbstractType
{
    //ajout d'une nouvelle nomenclature
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $annee=$options['annee'];
        $builder

            ->add('numero', TextType::class, [
                'required'=>false,
                'label'=>'Numéro'
            ])
            ->add('intitule', TextType::class, [
                'label' => 'Intitulé',
                'required'=>false

            ])
            ->add('famille', EntityType::class, [
                'class' => FamilleNomenclature::class,
                'choice_label' => 'Intitule',
                'multiple' => false,
                'expanded' => false,
                'required' => false,
                'placeholder' => '-- Choisir la famille --',
                'query_builder'=>function(FamilleNomenclatureRepository $familleNomenclatureRepository) use ($annee){
                    return $familleNomenclatureRepository->createQueryBuilder('s')
                      ->andWhere('s.annee = :val')
                      ->setParameter('val', $annee);
              }
            ])

          ->add('seuil', EntityType::class, [
              'class' => Seuil::class,
              'choice_label' => 'Nom',
              'multiple' => false,
              'expanded' => false,
              'required' => false,
              'query_builder'=>function(SeuilRepository $seuilRepository) use ($annee){
                //    return $seuilRepository->findByAnnee((Int) date("Y"));
                  return $seuilRepository->createQueryBuilder('s')
                      ->andWhere('s.annee = :val')
                      ->setParameter('val', $annee);
              },
              'placeholder' => '-- Choisir le seuil associé --'])

            ->add('detailIntitule', TextareaType::class, [
                'label' => 'Détails',
                'required' => false
            ])

            ->add('nomenclatureFournisseurs', CollectionType::class, [
                'entry_type' => NomenclatureFournisseurType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'required' => 'false',
            ])
             ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
          $resolver->setDefaults([
                'data_class' => Nomenclature::class,
                'annee'=>null
            ]);
        }

}