<?php


namespace App\Form\Type;


use App\Entity\NomenclatureFournisseur;
use App\Repository\NomenclatureFournisseurRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NomenclatureFournisseur1Type extends AbstractType
{

    public function getParent()
    {
        return EntityType::class;

    }
    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'class' => NomenclatureFournisseur::class,
            'query_builder' => function (Options $options) {


                $fournisseurId = $options['fournisseur_id'];
                $annee = $options['annee'];
               // $anne = 2021;
             //   dd($fournisseurId,$annee);
                return function (NomenclatureFournisseurRepository $er)use($fournisseurId,  $annee) {
                    return $er->createQueryBuilder('u')
                        ->innerJoin('App\Entity\Nomenclature', 'n', 'WITH', 'u.nomenclature = n.id')
                       ->andWhere('u.fournisseur = :id')
                        ->andWhere('n.annee = :an')
                       ->setParameter('id', $fournisseurId)
                        ->setParameter('an',   $annee);
                };
            },
            'choice_label' => function ($fournisseurId) {

             return $fournisseurId->getNomenclature()->getNumero().'-'. $fournisseurId->getNomenclature()->getIntitule() ;},
            'fournisseur_id'=>null,
            'annee'=>null,
        ]);


    }

}