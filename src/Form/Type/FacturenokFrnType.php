<?php


namespace App\Form\Type;


use App\Entity\Facturenok;
use App\Entity\Fournisseur;
use App\Entity\NomenclatureFournisseur;
use App\Repository\FacturenokRepository;
use App\Repository\NomenclatureFournisseurRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FacturenokFrnType extends AbstractType
{

    public function getParent()
    {
        return EntityType::class;

    }
    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'class' => Facturenok::class,
            'query_builder' => function (Options $options) {

               // dd($options);
                $facturesnokid = $options['facturenok_id'];

                return function (FacturenokRepository $er)use($facturesnokid) {
                    return $er->createQueryBuilder('u')
                       ->andWhere('u.id IN(:id)')
                       ->setParameter('id', $facturesnokid);

                };
            },
            'choice_label' => function ($facturesnokid) {
             return $facturesnokid->getMontant().'/'.'datefacture'.'/'.$facturesnokid->getOrganisme().'/'. $facturesnokid->getCompte().'/'. $facturesnokid->getDescription()  ;},
             // return $facturesnokid->getMontant().'-'. $facturesnokid->getDatefacture().'-'.$facturesnokid->getOrganisme().'-'. $facturesnokid->getCompte().'-'. $facturesnokid->getDescription().'-'. $facturesnokid->getDateimport()  ;},
                'facturenok_id'=>null,

        ]);




    }

}