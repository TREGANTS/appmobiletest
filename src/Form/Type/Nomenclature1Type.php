<?php


namespace App\Form\Type;


use App\Entity\Nomenclature;
use App\Repository\NomenclatureRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Nomenclature1Type extends AbstractType
{

    public function getParent()
    {
        return EntityType::class;

    }
public function configureOptions(OptionsResolver $resolver)
{
    $resolver->setDefaults([
        'class' => Nomenclature::class,
        'query_builder' => function (Options $options) {
            $fournisseurId = $options['fournisseur_id'];

            return function (NomenclatureRepository $er)use($fournisseurId) {
                return $er->createQueryBuilder('u')
                    ->innerJoin('App\Entity\NomenclatureFournisseur', 'nf', 'WITH', 'u.id = nf.nomenclature')
                    ->andWhere('nf.fournisseur = :id')
                    ->setParameter('id', $fournisseurId)
                    ->orderBy('u.numero', 'ASC');
            };
        },
        'choice_label' => function ($fournisseurId) {
            return $fournisseurId->getNumero().'-'. $fournisseurId->getIntitule() ;},
        'fournisseur_id'=>null,
    ]);


}

}