<?php

namespace App\Form\Type;

use App\Entity\Fournisseur;
use App\Repository\FournisseurRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Fournisseur1Type extends AbstractType
{
    public function getParent()
    {
        return EntityType::class;

    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => Fournisseur::class,
            'query_builder' => function (Options $options) {
                $fournisseurId = $options['fournisseur_id'];
                return function (FournisseurRepository $er)use($fournisseurId) {
                    return $er->createQueryBuilder('u')
                       ->andWhere('u.id = :id')
                       ->setParameter('id', $fournisseurId);

                };
            },
            //'choice_label' => 'nom',
            'choice_label' => function ($fournisseurId) {
                return $fournisseurId->getNom().' - ['.$fournisseurId->getNumTiers().']' ;},

            'fournisseur_id'=>null,
        ]);


    }
}