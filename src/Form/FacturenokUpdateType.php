<?php

namespace App\Form;


use App\Dataclass\Facturenok\UpdateFacturenokRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FacturenokUpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder

            ->add('annee', TextType::class)
            ->add('fournisseur', TextType::class)
            ->add('montant', TextType::class)
            ->add('cheminDoc', TextType::class)
            ->add('nomenclatureFournisseur');
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UpdateFacturenokRequest::class,
        ]);
    }
}
