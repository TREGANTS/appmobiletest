<?php

namespace App\Form;


use App\Dataclass\Fournisseur\UpdateFournisseurRequest;

use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FournisseurUpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
           $builder
               /* ->add('numTiers', IntegerType::class,array(
              'label' => 'Numéro Tiers',
          )) */
               ->add('numTiers', IntegerType::class,[
                   'label' => 'Numéro Tiers',
               ])
               ->add('nom',TextType::class)

               ->add('rejet',CheckboxType::class, [
                   'required' => false
               ])

           ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UpdateFournisseurRequest::class,
        ]);
    }
}
