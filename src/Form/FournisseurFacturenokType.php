<?php

namespace App\Form;

use App\Dataclass\Facturenok\Facturenok1Data;
use App\Entity\Facturenok;
use App\Entity\Fournisseur;
use App\Form\Type\NomenclatureFournisseur1Type;
use App\Form\Type\Fournisseur1Type;
use App\Form\Type\FacturenokFrnType;
use App\Form\Type\Nomenclature1Type;
#use Symfony\Bridge\Doctrine\Form\Type\EntityType;
#use App\Entity\Fournisseur;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;
// 1. Use the CategoriesToNumbersTransformer
# use App\Form\DataTransformer\NomenclaturesToNumbersTransformer;

class FournisseurFacturenokType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //dd($options);

        $fournisseur  = new Fournisseur();
        //dd($options['data']);
        # recup l'objet facturenok a partir  du tableau $options
        $facturenok = $options['data'][0];
       // $facturenok =  29177;
        # recupération id fournisseur
        $idfournisseur = $options['data'][1];
        # récupération année
        $annee= $options['data'][2];
       // dd($idfournisseur,$annee,$facturenok);
        $builder

            ->add('fournisseur',Fournisseur1Type::class,[
                    # récup identiant fournisseur
                   // 'fournisseur_id'=>  $facturenok->getFournisseur()->getId(),
                   'fournisseur_id'=> $idfournisseur,
                    'disabled' => false
                ]
            )

            ->add('nomenclatureFournisseur',NomenclatureFournisseur1Type::class,[

                # récup identiant fournisseur
               // 'fournisseur_id'=>  $facturenok->getFournisseur()->getId(),
                 'fournisseur_id'=> $idfournisseur,
               //  'annee'=>  $facturenok->getAnnee(),
                    'annee'=>  $annee,
                //'multiple' => false,
                //'expanded' => false,
                'label' =>'Nomenclature',
                'required' => true,
                'placeholder' => '-- Choisir une nomenclature --'


                ]
            )

            ->add('factures',FacturenokFrnType::class,[
                    # récup identiant fournisseur
                    // 'fournisseur_id'=>  $facturenok->getFournisseur()->getId(),
                    'facturenok_id'=> $facturenok,
                    //'multiple' => true,
                    'expanded' => true,
                    'disabled' => true

                ]
            )


        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           // 'data_class' => Facturenok1Data::class,
        ]);
    }
}
