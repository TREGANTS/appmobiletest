<?php

namespace App\Form;

use App\Entity\CategorieMarche;
use App\Entity\NomenclatureFournisseur;
use App\Repository\CategorieMarcheRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
//utilisée dan le tableua des résultats
class NomenclatureFournisseurUpdateType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('typeMarche',ChoiceType::class,[
                    'choices' => [
                        'Local'=>'local',
                        'Régional'=>'regional',
                        'National'=>'national'
                    ],
                    'required'=>false,
                    'placeholder'=>'-- Choisir le type de marché --',
                    'label'=>'Type de marché' ]
            )
            ->add('numMarche',TextType::class,[
                'attr'=>[
                    'class'=>'form-control'
                ],
                'required'=>false,
                'label'=>'N° de marché'
            ])

            ->add('categorieMarche',EntityType::class,[
                'class'=>CategorieMarche::class,
                'label'=>'Catégorie de marché',
                'choice_label'=>'nom',
                'multiple' => false,
                'expanded' => false,
                'required' => false,
                'placeholder'=>'-- Choisir la catégorie de marché --',
                'query_builder' => function (CategorieMarcheRepository  $categorieMarcheRepository){
                    return $categorieMarcheRepository->findAll1();
                }
            ])

            ->add('obs1', TextareaType::class, [
                'label' => 'Observations 1',
                'required' => false,
                'row_attr' => ['rows' => 3]
            ])
            ->add('obs2', TextareaType::class, [
                'label' => 'Observations 2',
                'required' => false,
                'row_attr' => ['rows' => 3]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => NomenclatureFournisseur::class,
        ]);
    }
}
