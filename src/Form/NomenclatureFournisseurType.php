<?php

namespace App\Form;

use App\Entity\CategorieMarche;
use App\Entity\Fournisseur;
use App\Entity\NomenclatureFournisseur;
use App\Repository\CategorieMarcheRepository;
use App\Repository\FournisseurRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NomenclatureFournisseurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('typeMarche',ChoiceType::class,[
                'choices' => [
                    'Local'=>'local',
                    'Régional'=>'regional',
                    'National'=>'national'
                ],
                'required'=>false,
                'placeholder'=>'-- Choisir le type de marché --',
                'label'=>'Type de marché']
                )
            ->add('numMarche',TextType::class,[
                'attr'=>[
                    'class'=>'form-control'
                ],
                'required'=>false,
                'label'=>'Numéro de marché',
            ])
            ->add('categorieMarche',EntityType::class,[
                'class'=>CategorieMarche::class,
                'choice_label'=>'nom',
                'multiple' => false,
                'expanded' => false,
                'required' => false,
                'label'=>'Catégorie de marché',
                'placeholder'=>'-- Choisir la catégorie de marché --',
                'query_builder' => function (CategorieMarcheRepository  $categorieMarcheRepository){
                    return $categorieMarcheRepository->findAll1();
                }
            ])
            ->add('fournisseur', EntityType::class, [
                'class' => Fournisseur::class,
                'choice_label' => function ($fournisseur) {
                    return $fournisseur->getNom().' - ('.$fournisseur->getNumTiers().')' ;},
                'multiple' => false,
                'expanded' => false,
                'required' => false,
                'placeholder'=>'-- Choisir le fournisseur --',
                'query_builder' => function (FournisseurRepository $fournisseurRepository) {
                    return $fournisseurRepository->findAllNonRejetes();
                },

            ])

        ->add('active',CheckboxType::class,[
            'required' => false
          ])



        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => NomenclatureFournisseur::class,
        ]);
    }
}
