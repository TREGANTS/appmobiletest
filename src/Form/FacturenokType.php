<?php

namespace App\Form;

use App\Entity\Facturenok;
use App\Form\Type\NomenclatureFournisseur1Type;
use App\Form\Type\Fournisseur1Type;
use App\Form\Type\Nomenclature1Type;
#use Symfony\Bridge\Doctrine\Form\Type\EntityType;
#use App\Entity\Fournisseur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;
// 1. Use the CategoriesToNumbersTransformer
# use App\Form\DataTransformer\NomenclaturesToNumbersTransformer;

class FacturenokType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $facturenok  = new Facturenok();

        # recup l'objet facturenok a partir  du tableau $options
        $facturenok = $options['data'];

        $builder

            ->add('fournisseur',Fournisseur1Type::class,[
                    # récup identiant fournisseur
                    'fournisseur_id'=>  $facturenok->getFournisseur()->getId(),
                    'disabled' => true
                ]
            )


            ->add('nomenclatureFournisseur',NomenclatureFournisseur1Type::class,[

                # récup identiant fournisseur
                'fournisseur_id'=>  $facturenok->getFournisseur()->getId(),
                 'annee'=>  $facturenok->getAnnee(),
                //'multiple' => false,
                //'expanded' => false,
                'label' =>'Nomenclature',
                'required' => true,
                'placeholder' => '-- Choisir une nomenclature --'


                ]
            )

            ->add('annee',IntegerType::class,[
                 'disabled' => true
                ]
            )
            ->add('montant',MoneyType::class,[
                    'disabled' => true
                ]
            )

            ->add('datefacture',DateType::class,[
                    'label' => 'Date de paiement',
                    'widget' => 'single_text',
                    'disabled' => true
                ]
            )
            ->add('organisme',IntegerType::class,[
                    'disabled' => true
                ]
            )
            ->add('compte',IntegerType::class,[
                    'disabled' => true
                ]
            )
            ->add('description',TextType::class,[
                    'disabled' => true
                ]
            )

            ->add('dateimport',DateType::class,[
                    'label' => 'Date d\'import',
                    'widget' => 'single_text',
                    'disabled' => true
                ]
            )


        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Facturenok::class,
        ]);
    }
}
