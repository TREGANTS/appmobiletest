<?php

namespace App\Form;

use App\Dataclass\Seuil\SeuilData;
use Doctrine\DBAL\Types\FloatType;
use Symfony\Component\Form\AbstractType;
#use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
#use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
#use Symfony\Component\Form\Extension\Core\Type\TelType;
#use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SeuilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class)
            ->add('seuil', MoneyType::class)
            ->add('seuilAlerte', MoneyType::class,array(
                'label' => 'Alerte Seuil',
            ))
         ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SeuilData::class,
        ]);
    }
}
