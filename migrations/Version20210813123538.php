<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210813123538 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql( 'CREATE view view_total_fournisseur as select nf.nomenclature_id, nf.fournisseur_id, nf.annee, sum(f.montant)
from nomenclature_fournisseur nf inner join facture f on f.nomenclature_fournisseur_id=nf.id
group by nf.nomenclature_id,nf.fournisseur_id,nf.annee');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP VIEW view_total_fournisseur');
    }

}