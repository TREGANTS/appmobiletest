<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211004091729 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
  //      $this->addSql('DROP INDEX uniq_a3cc793c376925a6');
        $this->addSql('ALTER TABLE famille_nomenclature ALTER annee DROP DEFAULT');
        $this->addSql('ALTER TABLE famille_nomenclature ALTER annee SET NOT NULL');
//        $this->addSql('ALTER TABLE famille_nomenclature ADD annee INT DEFAULT 2021');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE famille_nomenclature ALTER annee SET DEFAULT 2021');
        $this->addSql('ALTER TABLE famille_nomenclature ALTER annee DROP NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_a3cc793c376925a6 ON famille_nomenclature (intitule)');
    }
}
