--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.15
-- Dumped by pg_dump version 9.6.15

-- Started on 2021-11-12 10:54:01 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 13311)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3176 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 205 (class 1255 OID 16658)
-- Name: quantiemetodate(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.quantiemetodate(annee integer, quantieme integer) RETURNS date
    LANGUAGE plpgsql
    AS $$
declare
   quantiemeRectifie integer;
   jourdelan varchar(20);
   depassement varchar(20);
   resultat date;
begin
   if quantieme>365 then
   		quantiemeRectifie=366;
	Else
		quantiemeRectifie=quantieme;
	end if;
	jourdelan='01/01/'||cast(annee as char(4));
	depassement=cast(quantiemeRectifie-1 as varchar(3))||' day';
	resultat=jourdelan::date +depassement::interval;
   
   return resultat;
end;
$$;


ALTER FUNCTION public.quantiemetodate(annee integer, quantieme integer) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 185 (class 1259 OID 16659)
-- Name: categorie_marche; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categorie_marche (
    id integer NOT NULL,
    nom character varying(255) NOT NULL
);


ALTER TABLE public.categorie_marche OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 16662)
-- Name: categorie_marche_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categorie_marche_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categorie_marche_id_seq OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 16664)
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 16668)
-- Name: facture; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.facture (
    id integer NOT NULL,
    fournisseur_id integer NOT NULL,
    annee integer NOT NULL,
    montant numeric(10,2) DEFAULT NULL::numeric,
    chemin_doc character varying(255) DEFAULT NULL::character varying,
    organisme integer NOT NULL,
    compte integer,
    description text,
    quantieme smallint NOT NULL,
    dateimport date NOT NULL,
    nomenclature_fournisseur_id integer NOT NULL,
    datefacture date
);


ALTER TABLE public.facture OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 16676)
-- Name: facture_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.facture_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.facture_id_seq OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 16678)
-- Name: facturenok; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.facturenok (
    id integer NOT NULL,
    fournisseur_id integer,
    nomenclature_fournisseur_id integer,
    annee integer NOT NULL,
    montant numeric(10,2) DEFAULT NULL::numeric,
    chemin_doc character varying(255) DEFAULT NULL::character varying,
    organisme integer NOT NULL,
    compte integer,
    description text,
    quantieme smallint NOT NULL,
    dateimport date NOT NULL,
    datefacture date
);


ALTER TABLE public.facturenok OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 16686)
-- Name: facturenok_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.facturenok_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.facturenok_id_seq OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 16688)
-- Name: famille_nomenclature; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.famille_nomenclature (
    id integer NOT NULL,
    intitule character varying(255) NOT NULL,
    annee integer NOT NULL
);


ALTER TABLE public.famille_nomenclature OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 16691)
-- Name: famille_nomenclature_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.famille_nomenclature_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.famille_nomenclature_id_seq OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 16693)
-- Name: fournisseur; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fournisseur (
    id integer NOT NULL,
    num_tiers bigint NOT NULL,
    nom character varying(255) NOT NULL,
    rejet boolean DEFAULT false NOT NULL
);


ALTER TABLE public.fournisseur OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 16697)
-- Name: fournisseur_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fournisseur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fournisseur_id_seq OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 16699)
-- Name: nomenclature; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nomenclature (
    id integer NOT NULL,
    famille_id integer NOT NULL,
    numero character varying(10) NOT NULL,
    intitule character varying(350) NOT NULL,
    detail_intitule text,
    seuil_id integer NOT NULL,
    annee integer NOT NULL
);


ALTER TABLE public.nomenclature OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 16705)
-- Name: nomenclature_fournisseur; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nomenclature_fournisseur (
    id integer NOT NULL,
    fournisseur_id integer NOT NULL,
    nomenclature_id integer NOT NULL,
    type_marche character varying(50) DEFAULT NULL::character varying,
    num_marche character varying(80) DEFAULT NULL::character varying,
    obs1 text,
    obs2 text,
    active boolean DEFAULT true NOT NULL,
    categorie_marche_id integer,
    annee integer
);


ALTER TABLE public.nomenclature_fournisseur OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16714)
-- Name: nomenclature_fournisseur_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nomenclature_fournisseur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nomenclature_fournisseur_id_seq OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16716)
-- Name: nomenclature_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nomenclature_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nomenclature_id_seq OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 16718)
-- Name: seuil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seuil (
    id integer NOT NULL,
    nom character varying(255) NOT NULL,
    seuil numeric(10,2) NOT NULL,
    seuil_alerte numeric(10,2) NOT NULL,
    annee integer NOT NULL
);


ALTER TABLE public.seuil OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16721)
-- Name: seuil_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seuil_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seuil_id_seq OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 16723)
-- Name: view_factures_montant; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_factures_montant AS
 SELECT facture.fournisseur_id,
    facture.annee,
    sum(facture.montant) AS sum
   FROM public.facture
  GROUP BY facture.fournisseur_id, facture.annee;


ALTER TABLE public.view_factures_montant OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16727)
-- Name: view_total_fournisseur; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_total_fournisseur AS
 SELECT f.nomenclature_fournisseur_id,
    f.annee,
    sum(f.montant) AS total
   FROM public.facture f
  GROUP BY f.nomenclature_fournisseur_id, f.annee;


ALTER TABLE public.view_total_fournisseur OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16731)
-- Name: view_total_nomac; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_total_nomac AS
 SELECT nf.nomenclature_id,
    f.annee,
    sum(f.montant) AS total
   FROM (public.nomenclature_fournisseur nf
     JOIN public.facture f ON ((f.nomenclature_fournisseur_id = nf.id)))
  GROUP BY nf.nomenclature_id, f.annee;


ALTER TABLE public.view_total_nomac OWNER TO postgres;

--
-- TOC entry 3152 (class 0 OID 16659)
-- Dependencies: 185
-- Data for Name: categorie_marche; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.categorie_marche (id, nom) VALUES (2, 'Appel offres');
INSERT INTO public.categorie_marche (id, nom) VALUES (3, 'Marche sans publicite');
INSERT INTO public.categorie_marche (id, nom) VALUES (1, 'MAPA');


--
-- TOC entry 3177 (class 0 OID 0)
-- Dependencies: 186
-- Name: categorie_marche_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categorie_marche_id_seq', 7, true);


--
-- TOC entry 3154 (class 0 OID 16664)
-- Dependencies: 187
-- Data for Name: doctrine_migration_versions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20210611072136', '2021-06-11 07:21:53', 66);
INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20210719092723', '2021-07-19 09:28:58', 49);
INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20210728100339', '2021-08-02 08:19:16', 113);
INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20210803085128', '2021-08-03 08:51:56', 93);
INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20210805084244', '2021-08-05 08:48:57', 37);
INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20210813123538', '2021-09-09 08:58:17', 55);
INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20210909151357', '2021-09-09 15:33:04', 36);
INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20210910121730', '2021-09-10 12:22:56', 35);
INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20210910125335', '2021-09-10 12:55:04', 26);
INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20210805062133', '2021-09-21 10:11:33', 27);
INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20210805125759', '2021-09-21 10:11:33', 23);
INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20210921100412', '2021-09-21 10:15:22', 23);
INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20210927123948', '2021-09-27 12:40:05', 75);
INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20210927125549', '2021-09-27 12:56:00', 58);


--
-- TOC entry 3155 (class 0 OID 16668)
-- Dependencies: 188
-- Data for Name: facture; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3178 (class 0 OID 0)
-- Dependencies: 189
-- Name: facture_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.facture_id_seq', 18, true);


--
-- TOC entry 3157 (class 0 OID 16678)
-- Dependencies: 190
-- Data for Name: facturenok; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3179 (class 0 OID 0)
-- Dependencies: 191
-- Name: facturenok_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.facturenok_id_seq', 5719, true);


--
-- TOC entry 3159 (class 0 OID 16688)
-- Dependencies: 192
-- Data for Name: famille_nomenclature; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (100, 'Assurances', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (45, 'Chauffage et climatisation, réservoirs, citernes', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (2, 'Denrées alimentaires', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (50, 'Électricité, gaz, eau', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (54, 'Machines de bureau et équipements informatiques', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (52, 'Machines et équipements', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (44, 'Matériels de sport et matériels de jeux d''enfants pour jardins publics ou similaires', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (36, 'Matériels de transport', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (46, 'Matériels et équipements électriques et d''éclairage (hors quincaillerie)', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (39, 'Mobilier', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (67, 'Outils et machines (non prévus ailleurs)', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (12, 'Papier et produits de l''édition', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (63, 'Petites fournitures de bureau', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (61, 'Produits d''entretien à usage domestique et articles de droguerie', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (33, 'Produits de santé', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (16, 'Produits de santé (Les codes ATC correspondent à la classification Anatomical Therapeutic Chemical Classification)', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (8, 'Produits textiles, cuirs, habillement', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (35, 'Quincaillerie, outillage, produits en plastique, métal, ou verre (hors construction)', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (215, 'Service de déménagement', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (227, 'Service de recrutement de personnel en intérim (autre que médical)', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (214, 'Services annexes à l''agriculture, la pêche, l''élevage, l''horticulture, la chasse et l''industrie agro-alimentaire', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (163, 'Services d''assainissement, de voirie et de traitement des déchets', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (218, 'Services d''assistance et d''aide aux agents', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (180, 'Services d''éducation, services de qualification et d''insertion professionnelles, services de formation professionnelle', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (138, 'Services d''études, de conseil et d''assistance', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (129, 'Services d''hôtellerie et de restauration', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (222, 'Services d''impression', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (145, 'Services de communication', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (186, 'Services de contrôle, d''analyse et d''essai de produits, matériaux, fluides ou équipements (hors construction)', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (199, 'Services de maintenance (non prévus ailleurs)', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (156, 'Services de nettoyage', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (131, 'Services de sécurité', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (86, 'Services de télécommunications', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (90, 'Services des postes (hors monopole postal)', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (78, 'Services des télécommunications', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (102, 'Services financiers et comptables', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (184, 'Services immobiliers', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (110, 'Services informatiques', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (166, 'Services juridiques', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (139, 'Services liés à la réalisation d''opérations de construction (bâtiments, infrastructures, ouvrages industriels)', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (173, 'Services récréatifs, culturels et sportifs', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (169, 'Services sanitaires et sociaux', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (72, 'Transports des personnes', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (212, 'Travaux de la chaîne graphique, d''impression et de reprographie', 2020);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (386, 'Chauffage et climatisation, réservoirs, citernes', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (387, 'Denrées alimentaires', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (388, 'Électricité, gaz, eau', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (389, 'Machines de bureau et équipements informatiques', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (390, 'Machines et équipements', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (391, 'Matériels de sport et matériels de jeux d''enfants pour jardins publics ou similaires', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (392, 'Matériels de transport', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (393, 'Matériels et équipements électriques et d''éclairage (hors quincaillerie)', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (394, 'Mobilier', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (395, 'Outils et machines (non prévus ailleurs)', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (396, 'Papier et produits de l''édition', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (397, 'Petites fournitures de bureau', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (398, 'Produits d''entretien à usage domestique et articles de droguerie', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (399, 'Produits de santé', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (400, 'Produits de santé (Les codes ATC correspondent à la classification Anatomical Therapeutic Chemical Classification)', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (401, 'Produits textiles, cuirs, habillement', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (402, 'Quincaillerie, outillage, produits en plastique, métal, ou verre (hors construction)', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (403, 'Service de déménagement', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (404, 'Service de recrutement de personnel en intérim (autre que médical)', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (405, 'Services annexes à l''agriculture, la pêche, l''élevage, l''horticulture, la chasse et l''industrie agro-alimentaire', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (406, 'Services d''assainissement, de voirie et de traitement des déchets', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (407, 'Services d''assistance et d''aide aux agents', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (408, 'Services d''éducation, services de qualification et d''insertion professionnelles, services de formation professionnelle', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (409, 'Services d''études, de conseil et d''assistance', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (410, 'Services d''hôtellerie et de restauration', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (411, 'Services d''impression', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (412, 'Services de communication', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (413, 'Services de contrôle, d''analyse et d''essai de produits, matériaux, fluides ou équipements (hors construction)', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (414, 'Services de maintenance (non prévus ailleurs)', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (415, 'Services de nettoyage', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (416, 'Services de sécurité', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (417, 'Services de télécommunications', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (418, 'Services des postes (hors monopole postal)', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (419, 'Services des télécommunications', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (420, 'Services financiers et comptables', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (421, 'Services immobiliers', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (422, 'Services informatiques', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (423, 'Services juridiques', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (424, 'Services liés à la réalisation d''opérations de construction (bâtiments, infrastructures, ouvrages industriels)', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (425, 'Services récréatifs, culturels et sportifs', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (426, 'Services sanitaires et sociaux', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (427, 'Transports des personnes', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (428, 'Travaux de la chaîne graphique, d''impression et de reprographie', 2021);
INSERT INTO public.famille_nomenclature (id, intitule, annee) VALUES (385, 'Assurances', 2021);


--
-- TOC entry 3180 (class 0 OID 0)
-- Dependencies: 193
-- Name: famille_nomenclature_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.famille_nomenclature_id_seq', 430, true);


--
-- TOC entry 3161 (class 0 OID 16693)
-- Dependencies: 194
-- Data for Name: fournisseur; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (10, 356000000, 'La poste', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (16, 403106537, 'SFR SA', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (14, 424097525, 'AGOR AVITA', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (15, 428706097, 'Orange France', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (45, 428916209, 'INGEBAT', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (48, 508973161, 'WEBIKEO', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (23, 552062663, 'GENERALI', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (24, 582013736, 'DIOT', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (33, 642058739, 'BULL ATOS TECHNOLOGIE', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (25, 784395725, 'SATEC', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (50, 6780042502394, 'ONET PROPRETE ET SERVICES', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (27, 18002002600019, 'Caisse des dépôts et des consignations', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (56, 31768433000028, 'Camille Avocats', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (67, 31774946300038, 'CSIERESO', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (64, 32379935300038, 'SANS et Fils Déménagement', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (31, 32380356900025, 'JLB Informatique', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (38, 32833986600043, 'FALCOU', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (46, 33832946900070, 'OTEIS', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (30, 34361675100032, 'ESII', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (36, 34742254500025, 'AUDIOTEC', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (55, 35141627300027, 'SCP Vinsonneau Palies Noy Gauer', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (17, 35198708600037, 'TCS', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7, 37877854200266, 'NEOPOST France', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (58, 38033443300055, 'NOVARCHIVE', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (60, 38371167801639, 'SCHINDLER', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (44, 38440789600198, 'KARDHAM', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (43, 38778917500081, 'MEWS INNOVATION', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (13, 39748093002763, 'BOUYGUES TELECOM', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (1, 41033076500547, 'ANTALIS', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (29, 41177078700033, 'ALBATEC', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (35, 42994441600024, 'SPIGRAPH', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (74, 43399935600028, 'Manpower', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (28, 43407571900048, 'LYRA Network', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (61, 43890784200035, 'IPM France', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (2, 44239544800057, 'TOTAL DIRECT ENERGIE', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (39, 44341243200017, 'C&N Traiteurs', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (72, 45341407000012, 'Imprimerie Coste & fils', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (59, 48742094500023, 'La maison du vélo', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (34, 49069792700013, 'TIBCO', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (41, 49194836000046, 'ESIG', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (57, 49512517100043, 'CAPSTAN', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (52, 50177759300028, 'GREENBURO', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (4, 50294147900011, 'Gaz de Bordeaux', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (51, 50973545200021, 'AXIS SERVICES', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (63, 52198336100010, 'HOTRAVAIL', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (68, 54000818200029, 'IPSELIS', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (3, 54210765113030, 'ENGIE', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6, 56204623500478, 'PITNEY BOWES', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (53, 70198020300510, 'SUEZ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (62, 77558051700065, 'BASTIDE MANUTENTION', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (66, 77682520000020, 'SSTMC', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (65, 77693870600020, 'ASTIA', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (73, 77695040400025, 'Randstad', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (47, 79018467501175, 'Bureau Véritas Exploitation SAS', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (70, 82931425100028, 'RAULT EPPE SOLUTIONS SAS', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (32, 83212242800028, 'YOURLABS BUSINESS SERVICE', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (54, 83233607700199, 'PROSERVE DASRI', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5, 95551002900718, 'FIDUCIAL', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (11, 811934363000276, 'NXO France', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7126, 1, 'SCP FOUSSARD - FROGER           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7256, 29, 'SCP GEORGEL - PADILLA - LECLERCQ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7111, 31, 'THEVENOT MAYS BOSSON            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7199, 36, 'CAPSTAN AVOCATS                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7268, 140, 'GINHOLAC THIERRY                ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7368, 200, 'SCP ARIBAUT-ABADIE              ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7329, 299, 'TELMON NORBERT                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7229, 386, 'LENOIR CH - RODRIGUEZ J SCP     ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8287, 421, 'SCP LLORET-MARCELLIN-CHARRIE    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7282, 704, 'GUIRAUD MICHEL                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7305, 711, 'CALAZEL DOMINIQUE               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7286, 767, 'HERIN FABRICE                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7287, 877, 'VAHDAT OLIVIER DR               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7175, 932, 'VINSONNEAU PALIES NOY GAUER ASS.', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7125, 933, 'CAMILLE ET ASSOCIES             ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7381, 999, 'Prestataires-Déclaration DAS    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7300, 1057, 'MALECAZE FRANCOIS               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7310, 1123, 'BISMUTH SERGE                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7328, 1130, 'METTON GILBERT                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7215, 2063, 'KATO & LEFEBVRE                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7132, 2084, 'ASSI MAYA                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7172, 2090, 'CB2P AVOCATS                    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7604, 54200415, 'GEODIS TOURS MARCHES PUBLICS    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6917, 56800659, 'SNEF                            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8133, 67800425, 'ONET SERVICES                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7434, 130009186, 'BAPOIA                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8177, 173102112, 'RECETTE DIVISIONNAIRE DES IMPOTS', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7075, 180037012, 'ANACT                           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7990, 193113826, 'UT1 SCIENCES SOCIALES           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8075, 197534316, 'FONDATION NATION.DES SCIENCES PO', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6081, 200023596, 'S.M.E.A.                        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6068, 213103955, 'EAU DE MURET                    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8159, 243100344, 'SIVOM ST GAUDENS                ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6215, 300188778, 'SONEPAR SUD OUEST               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5783, 302475041, 'PSA RETAIL TOULOUSE E.U         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6938, 303217368, 'ESPACE TOY TOULOUSE             ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7653, 303732838, 'VOYAGES 31                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5812, 304381379, 'SELECT T.T. - APPEL MEDICAL     ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5802, 304747090, 'EDITIONS DU MEDECIN GENERALISTE ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6862, 306314717, 'CUBILIE                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6911, 308250570, 'ELECTRICITE INDU JP FAUCHE      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7046, 310644877, 'KARDEX REMSTAR                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5779, 312127186, 'SELI                            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6936, 312212301, 'RENAULT TOULOUSE                ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7991, 312326275, 'ORGANISATION VOYAGES PLANCHE    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7047, 312694540, 'GROUPE RCB                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8095, 315963108, 'BGE SUD-OUEST                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7312, 317749463, 'CSIERESO                        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5887, 318276771, 'IMPRIMERIE LECHA                ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7062, 318826187, 'HARVARD BUSINESS REVIEW         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6710, 320797913, 'MONCEAU INVESTISSEMENTS IMMO.   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5863, 321486631, 'SCP DR ROUQUETTE-LABORIE-ROUX   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6099, 321539660, 'VOUSSERT                        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6177, 322546672, 'R.C.I.                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7606, 323799353, 'DEMECO                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7036, 323803569, 'J.L.B. INFORMATIQUE             ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7995, 324345420, 'ADRAR FORMATION                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5795, 327503827, 'EVERNEX                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7354, 328538335, 'ASSURANCE MUTUELLE DES MOTARDS  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7100, 329599195, 'GABILLIET PHILIPPE              ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6791, 329892368, 'DIAC LOCATION                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6229, 330440983, 'INAPA FRANCE                    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7373, 330540907, 'AMV ASSURANCE                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7020, 330730607, 'VIPS                            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5737, 331289686, 'ANDRE CALLE                     ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6196, 331552489, 'RECA                            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6552, 331566430, 'ECONOCOM PRODUCTS AND SOLUTIONS ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6550, 333784262, 'E.S.I.                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6574, 334428604, 'SECURIMED                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6644, 334473352, 'FNAC                            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8280, 334608429, 'S.A.S. ROUZES                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7562, 334668852, 'MANUTAN                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6875, 337915607, 'ROUSSEL                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7834, 338246317, 'PROSEGUR SECURITE HUMAINE SAS   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5695, 338260383, 'MARIE BOULANGE                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7299, 338329469, 'OTEIS                           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7057, 338885718, 'INFOGREFFE                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7444, 339200669, 'REGIE NETWORKS                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5654, 339403933, 'DARTY OUEST                     ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8010, 339606980, 'VAELIA                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5892, 339701138, 'IPSIS - E.S.A.T. ELISA 31       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6432, 339806036, 'OUTILS OCEANS                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6597, 339901522, 'GEDIVEPRO                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7457, 340949031, 'M6 PUBLICITE                    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6902, 341039857, 'SARP SUD OUEST                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7357, 341672681, 'FILIA-MAIF                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7762, 343059564, 'S.F.R.                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7021, 343616751, 'ESII                            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6101, 343958138, 'JM BRUNEAU                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7453, 344572300, 'SARL  ARC EN CIEL               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7414, 345404040, 'CMI MEDIA                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6924, 347422545, 'AUDIOTEC                        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5772, 347665648, 'SARL SUP PEINTURE               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6699, 349084103, 'BATIGESTION                     ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7403, 349379024, 'INTELLIGENCE MEDIA-PUISSANCE 8  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7770, 350093704, 'I.E.C.                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7838, 350139051, 'SECURITAS ALERT SERVICES        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7380, 350663860, 'BPCE ASSURANCES SINISTRES       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6968, 351122908, 'ELECTRO-HYDRO                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7931, 351252416, 'PICTO                           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7699, 351671102, 'LE BISTROT D''ERIC               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7602, 351987086, 'T.C.S.                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6777, 378778542, 'QUADIENT FRANCE                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6517, 378843437, 'ECHELLE 31                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7818, 378901946, 'WORLDLINE                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7379, 379834906, 'GROUPAMA MEDITERRANEE SINISTRES ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7773, 380129866, 'ORANGE BUSINESS SERVICES        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8042, 380807479, 'FRANCE DIRIGEANTS               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6583, 380895375, 'SOPHISSAC                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7050, 381179357, 'DIAGONAL SAS                    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6065, 382514339, 'S.E.M. PYRENEES SERVICES PUBLICS', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7702, 382597821, 'NESPRESSO FRANCE                ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6997, 382814077, 'ISOGARD                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7560, 383064557, 'BRADY GROUPE S.A.S - SETON      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6140, 384560942, 'LEROY MERLIN                    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6156, 385321039, 'BLACK BOX FRANCE                ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6861, 387671639, 'ESPACE STORES                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6535, 388055493, 'INMAC WSTORE                    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7972, 388639718, 'A.F.C.I.                        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8282, 388739179, 'EIFFAGE ENERGIE SUD OUEST       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8284, 390354041, 'DL GARONNE                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5848, 391045648, 'PATISSERIE CONTE                ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6520, 391629193, 'RAYONPOLE                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7975, 392761698, 'COMPAGNIE THEATRALE L''ESQUISSE  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6946, 393594247, 'GARAGE DU PORT ST SAUVEUR       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7048, 393666581, 'FILHET-ALLARD & CIE             ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6526, 397480930, 'BOUYGUES TELECOM                ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7378, 398972901, 'GMF                             ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6950, 398997270, 'D.S.I                           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7037, 399243922, 'HORO QUARTZ                     ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8216, 403080823, 'GROUPE MONITEUR                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6217, 403092968, 'AU FORUM DU BATIMENT M.BLANC    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7515, 403554181, 'LDLC - PRO                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7458, 404391542, 'REGIE RADIO REGIONS EUROPE REGIE', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8180, 404967424, 'AA COURSES                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7863, 407991876, 'A7 PROTECTION                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5775, 408169985, 'GORRIZ                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6067, 410034607, 'SUEZ                            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6246, 410330765, 'ANTALIS SNC                     ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7001, 410659825, 'SPIGRAPH                        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6879, 410782494, 'LACROIX PORTES AUTOMATIQUES     ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6657, 411770787, 'ALBATEC                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5734, 413446857, 'LIGNE T                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7708, 413901760, 'ELIOR ENTREPRISES               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8281, 414313551, 'PLETT PEINTURE                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7333, 414502799, 'OLIVIER BAS                     ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6185, 414599985, 'EQUILAB                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6253, 415152768, 'BLAGNAC TAMPONS                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5717, 389785023, 'CHATEAUD''EAU', true);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7870, 421112848, '1001 COPIES                     ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5818, 421226259, 'CETAF                           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7525, 421715731, 'SAS ONE DIRECT                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5800, 422422063, 'G.M.S.                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8285, 423775436, 'JACKY MASSOUTIER & FILS         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7281, 423941723, 'ATCHIK SAS                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7429, 424081099, 'MANCHE ATLANTIQUE PRESSE        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7964, 424335693, 'MAILEVA                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7067, 424761419, 'OVH.COM                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7322, 428761886, 'SEMAPHORES SA                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7013, 429944416, 'SPIGRAPH                        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7954, 432711521, 'EMAIL MARKETING                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7825, 433221074, 'AMUNDI ESR                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7307, 433250834, 'DEKRA                           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6577, 433499670, 'MODOID WORK SYSTEM              ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7081, 433891850, 'LE MONDE                        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8260, 433900149, 'SOUTH PAINTERS SARL             ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5688, 433927332, 'TRANSGOURMET                    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7102, 433999356, 'RANDSTAD                        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7819, 434075719, 'LYRA NETWORK                    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5993, 434109807, 'EDENKIA                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8048, 434730446, 'FORMASUD                        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7408, 437737901, 'HEBDOS COMMUNICATION            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6601, 437937493, 'AERTEC                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6919, 438754061, 'BERGAMIN & FILS                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8188, 439410101, 'PHARMACIE SAINT-ETIENNE         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7489, 439882960, 'PARCS DES FLEURS                ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6934, 440271229, 'CARROSSERIE DUCAMIN             ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5607, 442395448, 'TOTAL DIRECT ENERGIE            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7464, 442949533, 'OCCITANE DE PUBLICITE           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7323, 443717830, 'LA MELEE                        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8231, 443884580, 'TRANQUIL IT                     ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5828, 444526842, 'IFORMA                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6905, 444608442, 'ENEDIS                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8032, 444796932, 'XINEO CONSULTANTS               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5815, 448697318, 'JOURDAN marc Psychologue        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6142, 451321335, 'CARREFOUR LABEGE                ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6110, 451392930, 'DOM SANTE                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6888, 451468821, 'SOS VITRINE                     ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6631, 451678973, 'CASTORAMA                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6382, 453414070, 'IMPRIMERIE COSTE ET FILS        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8273, 453614810, 'MB FORMATION                    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5770, 478516644, 'PLANETE MEDICALE                ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6186, 478895519, 'BEZIAN                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7524, 480011170, 'LUQUET & DURANTON               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8016, 480081306, 'WOLTERS KLUWER FRANCE           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7479, 480240381, 'TMARQUAGE                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8002, 480626787, 'CNFCE                           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7073, 480706159, 'ESPACE SOCIAL EUROPEEN          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7723, 482420247, 'LOGIDOC SOLUTIONS               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8014, 482761160, 'ORSYS FORMATION                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6999, 487420945, 'LA MAISON DU VELO               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7389, 488553249, 'HIMA NEWS                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7280, 490471539, 'ANIMAE                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7003, 490697927, 'TIBCO SERVICES                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7846, 491948360, 'SARL ESIG SECURITE              ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7920, 492351267, 'SARL G.2J                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7279, 492524749, 'TOULOUSE METROPOLE EMPLOI       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7367, 493147003, 'MATMUT ASSURANCES               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8261, 493222434, 'L DANSE                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7671, 493292403, 'AEROPORT TOULOUSE BLAGNAC       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7539, 499091551, 'AB POST                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7483, 499393452, 'AGENCE VERYWELL                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7000, 500306725, 'SARL ON TECH                    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6897, 500642129, 'GB AGENCEMENT                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7940, 501777593, 'GREEN BURO                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5805, 502494388, 'L''INFORMATION DENTAIRE          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7436, 502663453, 'MEDIA MEETING REGIE             ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5620, 502941479, 'GAZ DE BORDEAUX                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7866, 504807397, 'IPM FRANCE                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6670, 508005436, 'AMBIANCE DECO MURALE - CADREA   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5921, 508774577, 'PROHYGIENE                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8144, 509735452, 'SAS AXIS SERVICES               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6257, 510514748, 'DIAZO-REPRO                     ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6531, 510742463, 'UBIC                            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6907, 511677692, 'TECHNICLIMATIC                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7304, 513180273, 'AGENCE LUCIE                    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7869, 518365234, 'IN-PRINT                        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7660, 520807876, 'TISSEO                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6952, 521983361, 'HOTRAVAIL                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7402, 524077286, 'MEDIAS DU SUD                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8286, 524698206, 'KALITEC GENIE CLIMATIQUE        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6054, 525390811, 'BLAGNAC ENERGIES VERTES         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7446, 528341837, 'SCM LOCAL                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7913, 530628163, 'NOVARCHIVE                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7674, 531680445, 'TOTAL MARKETING FRANCE          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7438, 534188024, 'LOCAL-MEDIA SAS                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6813, 534317391, 'SCI COMPANS 2011                ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8232, 534369863, 'VOXYGEN                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6698, 534984968, 'BATIPART PALMER                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7288, 538349531, 'SO CO NER                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7324, 540008182, 'IPSELIS                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5803, 542037031, 'ELSEVIER MASSON                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6872, 542057336, 'ASTEN                           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7371, 542073580, 'MAAF                            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6954, 542107800, 'OTIS                            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7372, 542110291, 'ALLIANZ I.A.R.D.                ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6846, 550802771, 'PATRIMOINE SA LANGUEDOCIENNE    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6837, 552046484, 'CDC HABITAT SOCIAL              ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7085, 552072308, 'GROUPE REVUE FIDUCIAIRE         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5972, 552081317, 'ELECTRICITE DE FRANCE           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6371, 562046235, 'PITNEY BOWES                    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7064, 570804542, 'LA DEPECHE                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7069, 572014199, 'ALTARES                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6069, 572025526, 'VEOLIA EAU-C.G.E.               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7508, 572195550, 'EDITIONS DALLOZ                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5645, 580802551, 'MIDICA                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7074, 582071437, 'LES ECHOS                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6953, 592052302, 'KONE                            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7417, 622044501, 'JC DECAUX                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6801, 630802262, 'ALTEAL                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8289, 642044366, 'LE CHEQUE DEJEUNER UP           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7888, 701980203, 'SITA SUD-OUEST                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6963, 702000522, 'CHUBB SECURITE                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6581, 710803024, 'SODISCOL                        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7055, 712012129, 'LE QUOTIDIEN DU MEDECIN  SESC   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6926, 712056266, 'D.E.F.                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8202, 712780642, 'PYRENET                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6584, 725780084, 'ROCLE PIERRE                    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7096, 732011408, 'EDITIONS LEGISLATIVES           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6709, 732073887, 'NEXITY                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5674, 738205269, 'CANON FRANCE SAS                ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7491, 753115955, 'JARDINS DU GIROU                ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6978, 775580517, 'BASTIDE MANUTENTION             ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6214, 775586852, 'SIDER                           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7384, 775652126, 'MMA IARD ASSURANCES MUTUELLES   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8008, 775667231, 'IFACI                           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6494, 775703614, 'FRANKEL                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7345, 775709702, 'MAIF                            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8204, 776938706, 'ASTIA TOULOUSE                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5910, 776951881, 'IREPS OCCITANIE                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7982, 776952087, 'CFPCT TOULOUSE-PALAYS           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7369, 781452511, 'MACIF REGION CENTRE SINISTRE    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8067, 784314940, 'FUTURIBLES INTERNATIONAL        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8087, 788559441, 'VIEDANSE CONSULTING             ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7059, 790095673, 'EDITIONS WEKA                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6586, 790151310, 'MEDICCENTRE INDUSTRIE           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7321, 790184675, 'BUREAU VERITAS                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8090, 790243158, 'INSPIRATIONS MANAGEMENT         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7088, 794031575, 'NOUVELLE LIBRAIRIE TOULOUSAINE  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7486, 794061838, 'JEMAPUB.FR                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7886, 798951992, 'ALARME ELECTRONIQUE TOULOUSE    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7980, 799222039, 'COMUNDI                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7484, 799861638, 'POZZA PUB                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7433, 802388017, 'TRENTE ET UN SCOP ARL - BOUDU   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5655, 803136340, 'CUISINE DIFFUSION TLS - CUISINEO', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7720, 808578116, 'SAS EDIISCAN                    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8227, 808578801, 'KLAXOON                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7098, 812350684, 'KACTUS - POUR HARRY COW         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7099, 812823995, 'OCCETERRA                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5799, 812941805, 'EDIIS CRM                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8238, 813233608, 'PROACTIVE SIGNS SAS             ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7061, 813649571, 'S2C - STRATEGIES                ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7712, 814348421, 'BACARO - SARL FULLPROOF         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8111, 815158712, 'INSTITUT 4.10                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8266, 818256802, 'SAS HARRY COW                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6538, 820745586, 'ERGOSANTE SUD                   ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8239, 820867877, 'DOCTRINE - FORSETI SAS          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8205, 821177425, 'SANOFI PASTEUR EUROPE           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8081, 822396172, 'EVIDENCE CONCEPTS ET CONSULTANTS', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7092, 823175617, 'SOCIAL RH PUBLICATIONS          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5889, 823336077, 'PROSERVE DASRI STE EULALIE      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6488, 828274464, 'SMARTUP CITIES                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6405, 829314251, 'RAULT EPPE SOLUTIONS            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7395, 830897005, 'BIGHAPPY                        ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7043, 832122428, 'YOURLABS BUSINESS SERVICE       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5881, 832336077, 'PROSERVE DASRI                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7893, 833582950, 'BURO ERGO                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6075, 842404659, 'EAU DE TOULOUSE METROPOLE       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6690, 851703462, 'CASO PATRIMOINE- SCI 5 AV. OCCIT', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7998, 851793950, 'EPISSURE FORMATION SAS          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5652, 878236280, 'CONCEPT AMENAGEMENT             ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8049, 887679116, 'ARTORIS                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6329, 937080414, 'RAJA                            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6317, 955510029, 'FIDUCIAL BUREAUTIQUE            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8153, 13000835200895, 'SIP TOULOUSE RANGUEIL           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8174, 17310211200062, 'TRESORERIE DE CASTANET-TOLOSAN  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8161, 17310211200179, 'TRESORERIE DE L''UNION           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8167, 17310211200211, 'TRESORERIE MURET                ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8152, 17310211200450, 'TRESORERIE PRINCIPALE COLOMIERS-', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6705, 17310211200575, 'TRESORERIE D''AUCAMVILLE         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8151, 17310211200583, 'TRESORERIE DE BLAGNAC           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8165, 17310211200617, 'TRESORERIE PRINCIPALE ST GAUDENS', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8158, 24310063300052, 'SICOVAL                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7561, 38306455700101, 'SIGNALS                         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (7715, 45132133502144, 'CARREFOUR PORTET SUR GARONNE    ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (6121, 45167897301382, 'CASTORAMA ST.ORENS 2            ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8186, 77682520000038, 'SCE SANTE AU TRAVAIL ST-GAUDENS ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (5683, 387997737, 'RIVES MEDICAL', true);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8045, 776056467, 'UGAP', true);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (49, 330285875, 'CENTRE FRANCAIS D''EXPLOITATION DU DROIT DE COPIE', true);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (8290, 8900980908908, 'OOOOOOOXX', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18869, 2066, 'VOINCHET SOPHIE                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18890, 183109073, 'GIP - FCIP                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18876, 301921003, 'S.A ENTREPRISE TRAVAUX PLATRERIE', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18868, 404690554, 'LAMATHERM                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18887, 429987548, 'DAWAN IT-CONSULTING             ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18873, 449002419, 'EURL ISOLA B PLUS               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18879, 449475524, 'LAGREU CORBALAN FRANCOISE PHARMA', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18874, 503039331, 'GV ETUDES                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18886, 519158380, 'DEPECHE EVENTS                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18864, 520650193, 'CROS P.V.R                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18882, 542107651, 'ENGIE                           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18889, 562016774, 'SAS SIEMENS                     ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18859, 620800987, 'SITAF                           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18866, 660802398, 'CRESPY                          ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18858, 670802255, 'APPLICATION DU BOIS ET CONCEPT  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18863, 682003991, 'ADECCO MEDICAL                  ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18862, 700802234, 'CIMSO                           ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18885, 722057460, 'AXA FRANCE IARD                 ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18861, 730800562, 'CERM SOLS                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18871, 752217794, 'OUEST INVENTAIRES               ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18865, 808517536, 'GRESSET RAULT SOLUTIONS         ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18867, 814250361, 'SARL OPNA                       ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18872, 818939795, 'SYSTHERMIC                      ', false);
INSERT INTO public.fournisseur (id, num_tiers, nom, rejet) VALUES (18877, 882762925, 'S.A.R.L. JOINTEURS ASSOCIES     ', false);


--
-- TOC entry 3181 (class 0 OID 0)
-- Dependencies: 195
-- Name: fournisseur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fournisseur_id_seq', 18890, true);


--
-- TOC entry 3163 (class 0 OID 16699)
-- Dependencies: 196
-- Data for Name: nomenclature; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (6, 8, '14.04', 'Vêtements de travail et EPI', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (7, 8, '14.09', 'Articles textiles divers à usage unique (sauf fournitures hôtelières pour la petite enfance : 37) :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (8, 8, '14.10', 'Chaussures (quelle que soit la matière, sauf chaussures orthopédiques) :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (9, 12, '15.02', 'Papiers et cartons en l''état', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (10, 12, '15.03', 'Emballages en papier ou en carton', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (11, 12, '15.05', 'Livres non scolaires et documents imprimés', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (12, 12, '15.06', 'Journaux, revues et périodiques d''information générale et spécialisée', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (13, 12, '15.07', '- Abonnements électroniques de presse et à des publications en ligne (encyclopédies, dictionnaires).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (14, 12, '15.09', 'Imprimés simples pour communication interne :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (15, 12, '15.10', 'Autres imprimés :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (16, 16, '18.01', 'Spécialités pharmaceutiques avec autorisation de mise sur le marché (AMM) : voies digestives et métabolisme (codes ATC A).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (17, 33, '18.03', 'Spécialités pharmaceutiques avec AMM : système cardiovasculaire (codes ATC C).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (19, 33, '18.39', 'Consommables d''imagerie :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (20, 33, '18.43', 'Dispositifs médicaux d''équipements d''assistance fonctionnelle cardiaque :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (21, 33, '18.47', 'Dispositifs médicaux d''équipements d''exploration fonctionnelle :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (22, 33, '18.50', 'Équipements médicaux et techniques divers :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (23, 33, '18.54', 'Consommables de laboratoire et systèmes de prélèvement pour patients, en plastique, à usage unique et multiple.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (24, 33, '18.56', 'Réactifs biochimie et réactifs immunochimie.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (25, 33, '18.57', 'Réactifs hématologie-cytologie et réactifs hémostase.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (26, 33, '18.58', 'Réactifs immunohématologie.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (27, 33, '18.59', 'Réactifs anatomopathologie.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (28, 33, '18.60', 'Réactifs microbiologie, culture cellulaire et réactifs immunologie infectieuse.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (29, 33, '18.61', 'Réactifs biologie moléculaire.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (30, 33, '18.62', 'Réactifs d''anticorps monoclonaux.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (31, 33, '18.63', 'Réactifs et consommables pour FIV.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (32, 33, '18.64', 'Équipements généraux de laboratoire :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (33, 33, '18.71', 'Divers conditionnements pour services de soins :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (34, 33, '18.73', 'Petit matériel médical', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (35, 35, '20.01', 'Outillage, quincaillerie, produits en plastique ou en verre', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (36, 36, '24.01', 'Véhicules automobiles (accessoires et pièces détachées compris) :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (37, 36, '24.02', 'Camions et véhicules utilitaires (accessoires et pièces détachées compris) :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (38, 36, '24.03', 'Cyclomoteurs et cycles électriques et cycles (accessoires et pièces détachées compris) :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (39, 39, '25.01', 'Literie ', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (40, 39, '25.02', 'Sièges', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (41, 39, '25.03', 'Cloisons', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (42, 39, '25.04', 'Plans et tables', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (43, 39, '25.05', 'Mobilier de rangement :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (44, 44, '27.01', 'Matériels de sport', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (45, 45, '32.01', 'Chauffage et climatisation, réservoirs, citernes :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (46, 46, '33.01', 'Matériel électrique :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (47, 46, '33.02', 'Équipements électriques et d''éclairage :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (48, 50, '34.01', 'Électricité', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (50, 50, '34.02', 'Gaz', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (51, 50, '34.03', 'Eau', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (52, 52, '35.09', 'Appareils domestiques :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (53, 54, '36.01', 'Machines de bureau :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (54, 54, '36.02', 'Micro-ordinateurs et stations de travail :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (57, 54, '36.04', 'Extensions de puissance :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (58, 54, '36.05', 'Périphériques :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (59, 54, '36.06', 'Équipements de réseaux informatiques :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (60, 54, '36.07', 'Consommables et autres fournitures :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (61, 61, '37.01', 'Produits d''entretien à usage domestique et articles de droguerie :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (63, 63, '38,02', 'Enveloppes', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (62, 63, '38.01', 'Petites fournitures de bureau :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (64, 67, '40.01', 'Machines à ouvrir', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (67, 67, '40.02', 'Machine à affranchir', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (69, 72, '60.01', 'Transports ferroviaires des personnes (y compris bagages, animaux et véhicules accompagnés).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (70, 72, '60.02', 'Transports aériens des personnes (y compris bagages, animaux et véhicules accompagnés).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (71, 72, '60.04', 'Transports routiers et urbains des personnes non handicapées (y compris bagages, animaux et véhicules accompagnés) :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (72, 72, '60.05', 'Transports routiers et urbains des handicapés (y compris bagages, animaux et véhicules accompagnés).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (73, 72, '60.07', 'Agences de voyage', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (74, 72, '60.08', 'Location de tous véhicules avec chauffeurs, pilote ou équipage, pour transport de personnes.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (75, 72, '60.09', 'Acquisiton de véhicules', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (76, 72, '60.10', 'Carte carburant', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (77, 78, '63.01', 'Services de téléphonie filaire (abonnements et communications).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (79, 86, '63.02', 'Services de téléphonie mobile (abonnements et communications).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (80, 86, '63.03', 'Services de réseaux de transmission de données (abonnements et communications) :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (83, 86, '63.04', 'Maintenance des matériels de téléphonie et des équipements de télécommunication.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (84, 86, '63.05 ', 'Maintenance des équipements de réseaux de télécommunication.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (85, 86, '63.06', 'Installation et montage des matériels de téléphonie :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (87, 90, '64.01', 'Acheminement de lettres et colis à vitesse normale.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (88, 90, '64.02', 'Courrier express, activité de coursiers, courrier recommandé, services de boîtes postales, de poste restante ou de réexpédition.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (89, 90, '64.03', 'Personnalisation de documents par édition informatique, mise sous enveloppe ou film, pose d''étiquettes :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (90, 90, '64.04', 'Routage ', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (1, 2, '10.01', 'Produits alimentaires', NULL, 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (2, 2, '10.02', 'Pains, viennoiseries et pâtisseries', NULL, 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (5, 8, '14.03', 'Vêtements de dessus :', NULL, 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (86, 86, '63.07', 'Services de conseil en télécommunication :', NULL, 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (91, 100, '65.01', 'Assurances du patrimoine. - Contrats dommages aux biens.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (93, 100, '65.02', 'Assurances des personnes (maladie, accident, décès).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (95, 100, '65.03', 'Assurances automobiles (responsabilité civile, dommages aux véhicules, garantie du conducteur).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (97, 100, '65.04', 'Assurances construction (dommages-ouvrage, tous risques chantiers, responsabilité du constructeur).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (98, 100, '65.05', 'Assurances transports terrestres (responsabilité, personnes, corps, facultés).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (100, 100, '65.07', 'Autres assurances de responsabilité (hors 65.03 à 65.06).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (101, 100, '65.09', 'Activité de conseil en assurance (élaboration de cahiers de charges, évaluation du patrimoine, gestion des contrats et des dossiers de sinistres).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (102, 102, '66.01', 'Crédit-bail.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (103, 102, '66.02', 'Intermédiation financière et activité de conseil :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (104, 102, '66.03', 'Autres services d''auxiliaires financiers : gestion de chèques-restaurant ou vacances.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (105, 102, '66.04', 'Assistance et conseil en comptabilité, fiscalité, expertise comptable.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (106, 102, '66.05', 'Services bancaires :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (107, 102, '66.06', 'Service de paiement en ligne', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (108, 110, '67.01', 'Schéma directeur et audit en organisation.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (109, 110, '67.02', 'Assistance à maîtrise d''ouvrage.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (110, 110, '67.03', 'Assistance à maîtrise d''oeuvre.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (111, 110, '67.04', 'Achat et développement de progiciels.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (112, 110, '67.05', 'Achat et développement de logiciels.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (114, 110, '67.06', 'Maintenance logicielle :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (117, 110, '67.07', 'Traitements informatiques :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (118, 110, '67.08', 'Infogérance d''un système d''information :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (119, 110, '67.09', 'Services de banques de données :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (120, 110, '67.11', 'Maintenance des micro-ordinateurs, mini-ordinateurs, stations de travail, périphériques informatiques.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (121, 110, '67.12', 'Maintenance des équipements de réseaux informatiques.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (122, 110, '67.13', 'Maintenance de scanners', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (123, 110, '67.14', 'Maintenance de matériels audiovisuels', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (124, 129, '68.01', 'Hébergement en hôtel, pensions, demi-pension, auberges de jeunesse, refuges, camping ou autres.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (125, 129, '68.02', 'Services de restauration :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (126, 129, '68.03', 'Services des traiteurs (hors restauration collective) :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (130, 131, '69.01', 'Surveillance d''immeubles, garde, protection par vigiles', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (131, 131, '69.02', 'Télésurveillance / videosurveillance', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (132, 138, '70.01', 'Gestion de personnel : recrutement, conseil, organisation.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (133, 138, '70.02', 'Organisation des services : démarche qualité, audit, conseil, contrôle de gestion.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (134, 138, '70.03', 'Prestations de secrétariat et traduction.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (135, 138, '70.05', 'Enquêtes et sondages (hors communication).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (136, 138, '70.06', 'Études à caractère général (hors communication).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (137, 138, '70.07', 'Études et recherches scientifiques fondamentales et appliquées.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (138, 138, '70.08', 'Études à caractère technologique.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (139, 139, '71.01', 'Maîtrise d''oeuvre (hors services de maîtrise d''oeuvre pour les projets urbains) et ordonnancement, pilotage et coordination.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (140, 139, '71.02', 'Conduite d''opération.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (141, 139, '71.03', 'Études, analyses et contrôles nécessaires à la réalisation d''un ouvrage (à l''exclusion des analyses et essais des matériaux, produits et matériels d''installation et d''équipement immobilier - 80) :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (142, 145, '72.01', 'Agences et conseil en communication et publicité.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (143, 145, '72.02', 'Campagnes de communication (information, publicité, relations publiques).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (144, 145, '72.03', 'Achat et gestion d''espaces publicitaires.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (145, 145, '72.04', 'Organisation de colloques et événements (foires, salons).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (146, 145, '72.05', 'Réalisation de stands (salons, foires).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (147, 145, '72.06', 'Publications (conception).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (148, 145, '72.07', 'Études, sondages et enquêtes de communication.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (149, 145, '72.08', 'Services photographiques et audiovisuels de communication : y compris services des laboratoires photographiques et cinématographiques.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (150, 145, '72.09', 'Travaux graphiques de communication :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (151, 145, '72.10', 'Conception et réalisation de sites internet :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (152, 145, '72.11', 'Traitement de l''information :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (153, 145, '72.12', 'Mise en place de plates-formes téléphoniques.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (156, 156, '73.01', 'Nettoyage courant des locaux.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (157, 156, '73.04', 'Nettoyage spécifique des vitres.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (158, 156, '73.07', 'Désinfection, dératisation, désinsectisation. ', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (159, 156, '73.08', 'Nettoyage de véhicules.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (160, 156, '73.09', 'Blanchisserie, teinturerie.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (161, 156, '73.10', 'Location-entretien de linge (y compris hospitalier).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (162, 163, '74.03', 'Enlèvement, tri et stockage des ordures ménagères.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (164, 163, '74.05', 'Enlèvement, tri, stockage et traitement des déchets autres que ménagers ou nucléaires :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (165, 166, '75.01', 'Services de conseils juridiques : dans les différents domaines du droit, y compris en matière de propriété industrielle.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (166, 166, '75.02', 'Services d''établissements d''actes authentiques et des auxiliaires de justice.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (168, 166, '75.03', 'Services de représentation juridique.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (169, 169, '76.06', 'Prestations de services d''intérim de sages-femmes, d''infirmières et de personnels soignants et paramédicaux (1).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (170, 169, '76.15', 'Contrôle et analyses biologiques et autres analyses de laboratoire pour la santé humaine ou animale, à l''exclusion des analyses officielles réalisées dans le cadre des contrôles sanitaires vétérinaires relatifs à la protection de la santé humaine et de la santé animale.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (171, 173, '77.01', 'Services de conception, de production, de distribution, de projection, de traduction et de promotion ou de publicité de films ou d''oeuvres audiovisuelles et multimédias (comprend notamment les activités cinématographiques et de vidéo).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (229, 100, '100.52', 'toto', NULL, 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (154, 145, '72.13', 'Service de webinaires', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (172, 173, '77.02', 'Services de spectacles musicaux, de danse, de théâtre, de représentation artistique et de cirque, de spectacles de sons et lumières fournis par des producteurs ou des artistes amateurs ou professionnels', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (173, 173, '77.03', 'Services auxiliaires des activités de spectacle portant sur la réalisation et l''installation de décors, d''éclairages et de sonorisation, sur la conception et la réalisation de costumes, sur la scénographie, sur la traduction des spectacles et sur la vente de billets.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (174, 173, '77.05', 'Services d''agence de presse écrite, photographique, radio ou télédiffusée ou cinématographique.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (175, 173, '77.06', 'Services d''agence de reportage en direct aux stations de télévision.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (176, 173, '77.08', 'Services de gestion (acquisition, catalogage, conservation et recherche) d''archives publiques.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (177, 173, '77.14', 'Services d''organisation et de promotion des manifestations sportives.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (178, 180, '78.02', 'Services de qualification et d''insertion professionnelles :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (179, 180, '78.03', 'Formation professionnelle initiale destinée aux agents des collectivités publiques (hors services de qualification et d''insertion professionnelles).', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (180, 180, '78.04', 'Préparation aux concours ou examens professionnels destinée aux agents des collectivités publiques.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (181, 180, '78.05', 'Formation professionnelle continue destinée aux agents des collectivités publiques :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (182, 184, '79.01', 'Services des agences immobilières :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (183, 184, '79.02', 'Services d''administration d''immeubles :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (184, 184, '79.03', 'Conseil en immobilier.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (185, 184, '79.04', 'Services de promotion immobilière.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (186, 186, '80.01', 'Contrôle technique automobile.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (187, 186, '80.09', 'Analyses et essais d''équipements de mesure, de test et de santé :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (188, 199, '81.01', 'Maintenance des véhicules de transport de personnes :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (189, 199, '81.02', 'Maintenance des camions et des véhicules utilitaires.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (190, 199, '81.03', 'Maintenance des cycles et cyclomoteurs.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (191, 199, '81.10', 'Maintenance d''équipements mécaniques :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (192, 199, '81.11', 'Maintenance de machines d''usage général :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (193, 199, '81.15', 'Maintenance des appareils ménagers.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (194, 199, '81.16', 'Maintenance des machines de bureau (hors informatique) :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (195, 199, '81.17', 'Maintenance des machines et appareils électriques :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (196, 199, '81.18', 'Maintenance des matériels d''imagerie.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (197, 199, '81.23', 'Maintenance des équipements médicaux et techniques divers.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (198, 199, '81.25', 'Maintenance des équipements de laboratoire.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (199, 199, '81.26', 'Maintenance des machines et matériels de chauffage.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (200, 199, '81.27', 'Maintenance des matériels sanitaires et de plomberie.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (201, 199, '81.28', 'Maintenance d''installations de levage et de transport électro-mécaniques :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (202, 199, '81.29', 'Maintenance d''installations et d''équipements de protection contre l''incendie.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (203, 199, '81.30', 'Maintenance d''installations et d''équipements de contrôle des accès, de portes automatiques.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (204, 199, '81.31', 'Maintenance de matériels et équipements pour la gestion de biens immobiliers :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (205, 199, '81.32', 'Maintenance des bornes multi focntions', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (206, 199, '81.33', 'Maintenance de chariots élévateurs', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (207, 212, '82.01', 'Conception graphique, maquette.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (208, 212, '82.02', 'Pré-presse', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (209, 212, '82.03', 'Travaux d''impression offset.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (210, 212, '82.04', 'Autres travaux d''impression ou de reprographie.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (211, 212, '82.05', 'Travaux de façonnage de produits imprimés.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (212, 212, '82.06', 'Autres travaux de la chaîne graphique :', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (213, 212, '82.07', 'Reproduction d''enregistrements sonores et vidéo.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (214, 214, '84.02', 'Maintenance des espaces verts, parcs, jardins, plantations ornementales : y compris maintenance des jeux d''enfants installés dans les parcs et jardins.', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (215, 215, '85.01', 'Prestations de déménagements', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (216, 218, '86.01', 'Médecine du travail', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (218, 218, '86.02', 'Accompagnement social', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (219, 218, '86.03', 'Soutien psychologique', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (220, 218, '86.04', 'Tests de personnalité', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (221, 218, '86.05', 'Soutien psychologique par téléphone', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (222, 222, '87.01', 'Services d''impression', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (226, 227, '88.01', 'Intérim administratif et technique', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (227, 227, '88.02', 'Intérim informatique', '', 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (4, 8, '14.02', 'Linge de maison, articles d''ameublement et de literie :', NULL, 1, 2021);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (155, 145, '72.14', 'Service d''autorisation de reproduction d''œuvre protégée', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (18, 33, '18.10', 'Spécialités pharmaceutiques avec AMM : système respiratoire (codes ATC R).', 'ceci est un détail bien plus conséquentdshfshfdhsdfhlkjsdf sqdlkfjqslkdjflmkjsqdf
sdjflskdjfmlsdkjfmlkjsdf
dskjflksjdfmlksdjfmlk

oh oh oh', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (316, 8, '14.04', 'Vêtements de travail et EPI', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (317, 8, '14.09', 'Articles textiles divers à usage unique (sauf fournitures hôtelières pour la petite enfance : 37) :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (318, 8, '14.10', 'Chaussures (quelle que soit la matière, sauf chaussures orthopédiques) :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (319, 12, '15.02', 'Papiers et cartons en l''état', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (320, 12, '15.03', 'Emballages en papier ou en carton', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (321, 12, '15.05', 'Livres non scolaires et documents imprimés', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (322, 12, '15.06', 'Journaux, revues et périodiques d''information générale et spécialisée', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (323, 12, '15.07', '- Abonnements électroniques de presse et à des publications en ligne (encyclopédies, dictionnaires).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (324, 12, '15.09', 'Imprimés simples pour communication interne :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (325, 12, '15.10', 'Autres imprimés :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (326, 16, '18.01', 'Spécialités pharmaceutiques avec autorisation de mise sur le marché (AMM) : voies digestives et métabolisme (codes ATC A).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (327, 33, '18.03', 'Spécialités pharmaceutiques avec AMM : système cardiovasculaire (codes ATC C).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (329, 33, '18.39', 'Consommables d''imagerie :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (330, 33, '18.43', 'Dispositifs médicaux d''équipements d''assistance fonctionnelle cardiaque :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (331, 33, '18.47', 'Dispositifs médicaux d''équipements d''exploration fonctionnelle :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (332, 33, '18.50', 'Équipements médicaux et techniques divers :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (333, 33, '18.54', 'Consommables de laboratoire et systèmes de prélèvement pour patients, en plastique, à usage unique et multiple.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (334, 33, '18.56', 'Réactifs biochimie et réactifs immunochimie.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (335, 33, '18.57', 'Réactifs hématologie-cytologie et réactifs hémostase.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (336, 33, '18.58', 'Réactifs immunohématologie.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (337, 33, '18.59', 'Réactifs anatomopathologie.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (338, 33, '18.60', 'Réactifs microbiologie, culture cellulaire et réactifs immunologie infectieuse.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (339, 33, '18.61', 'Réactifs biologie moléculaire.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (340, 33, '18.62', 'Réactifs d''anticorps monoclonaux.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (341, 33, '18.63', 'Réactifs et consommables pour FIV.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (342, 33, '18.64', 'Équipements généraux de laboratoire :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (343, 33, '18.71', 'Divers conditionnements pour services de soins :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (344, 33, '18.73', 'Petit matériel médical', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (345, 35, '20.01', 'Outillage, quincaillerie, produits en plastique ou en verre', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (346, 36, '24.01', 'Véhicules automobiles (accessoires et pièces détachées compris) :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (347, 36, '24.02', 'Camions et véhicules utilitaires (accessoires et pièces détachées compris) :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (348, 36, '24.03', 'Cyclomoteurs et cycles électriques et cycles (accessoires et pièces détachées compris) :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (349, 39, '25.01', 'Literie ', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (350, 39, '25.02', 'Sièges', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (351, 39, '25.03', 'Cloisons', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (352, 39, '25.04', 'Plans et tables', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (353, 39, '25.05', 'Mobilier de rangement :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (354, 44, '27.01', 'Matériels de sport', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (355, 45, '32.01', 'Chauffage et climatisation, réservoirs, citernes :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (356, 46, '33.01', 'Matériel électrique :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (357, 46, '33.02', 'Équipements électriques et d''éclairage :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (358, 50, '34.01', 'Électricité', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (360, 50, '34.02', 'Gaz', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (361, 50, '34.03', 'Eau', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (362, 52, '35.09', 'Appareils domestiques :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (363, 54, '36.01', 'Machines de bureau :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (364, 54, '36.02', 'Micro-ordinateurs et stations de travail :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (367, 54, '36.04', 'Extensions de puissance :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (368, 54, '36.05', 'Périphériques :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (369, 54, '36.06', 'Équipements de réseaux informatiques :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (370, 54, '36.07', 'Consommables et autres fournitures :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (371, 61, '37.01', 'Produits d''entretien à usage domestique et articles de droguerie :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (373, 63, '38,02', 'Enveloppes', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (372, 63, '38.01', 'Petites fournitures de bureau :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (374, 67, '40.01', 'Machines à ouvrir', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (377, 67, '40.02', 'Machine à affranchir', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (379, 72, '60.01', 'Transports ferroviaires des personnes (y compris bagages, animaux et véhicules accompagnés).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (380, 72, '60.02', 'Transports aériens des personnes (y compris bagages, animaux et véhicules accompagnés).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (381, 72, '60.04', 'Transports routiers et urbains des personnes non handicapées (y compris bagages, animaux et véhicules accompagnés) :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (382, 72, '60.05', 'Transports routiers et urbains des handicapés (y compris bagages, animaux et véhicules accompagnés).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (383, 72, '60.07', 'Agences de voyage', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (384, 72, '60.08', 'Location de tous véhicules avec chauffeurs, pilote ou équipage, pour transport de personnes.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (385, 72, '60.09', 'Acquisiton de véhicules', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (386, 72, '60.10', 'Carte carburant', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (387, 78, '63.01', 'Services de téléphonie filaire (abonnements et communications).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (389, 86, '63.02', 'Services de téléphonie mobile (abonnements et communications).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (390, 86, '63.03', 'Services de réseaux de transmission de données (abonnements et communications) :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (393, 86, '63.04', 'Maintenance des matériels de téléphonie et des équipements de télécommunication.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (394, 86, '63.05 ', 'Maintenance des équipements de réseaux de télécommunication.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (395, 86, '63.06', 'Installation et montage des matériels de téléphonie :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (397, 90, '64.01', 'Acheminement de lettres et colis à vitesse normale.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (398, 90, '64.02', 'Courrier express, activité de coursiers, courrier recommandé, services de boîtes postales, de poste restante ou de réexpédition.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (399, 90, '64.03', 'Personnalisation de documents par édition informatique, mise sous enveloppe ou film, pose d''étiquettes :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (400, 90, '64.04', 'Routage ', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (311, 2, '10.01', 'Produits alimentaires', NULL, 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (312, 2, '10.02', 'Pains, viennoiseries et pâtisseries', NULL, 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (315, 8, '14.03', 'Vêtements de dessus :', NULL, 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (396, 86, '63.07', 'Services de conseil en télécommunication :', NULL, 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (401, 100, '65.01', 'Assurances du patrimoine. - Contrats dommages aux biens.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (403, 100, '65.02', 'Assurances des personnes (maladie, accident, décès).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (405, 100, '65.03', 'Assurances automobiles (responsabilité civile, dommages aux véhicules, garantie du conducteur).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (407, 100, '65.04', 'Assurances construction (dommages-ouvrage, tous risques chantiers, responsabilité du constructeur).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (408, 100, '65.05', 'Assurances transports terrestres (responsabilité, personnes, corps, facultés).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (410, 100, '65.07', 'Autres assurances de responsabilité (hors 65.03 à 65.06).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (411, 100, '65.09', 'Activité de conseil en assurance (élaboration de cahiers de charges, évaluation du patrimoine, gestion des contrats et des dossiers de sinistres).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (412, 102, '66.01', 'Crédit-bail.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (413, 102, '66.02', 'Intermédiation financière et activité de conseil :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (414, 102, '66.03', 'Autres services d''auxiliaires financiers : gestion de chèques-restaurant ou vacances.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (415, 102, '66.04', 'Assistance et conseil en comptabilité, fiscalité, expertise comptable.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (416, 102, '66.05', 'Services bancaires :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (417, 102, '66.06', 'Service de paiement en ligne', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (418, 110, '67.01', 'Schéma directeur et audit en organisation.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (419, 110, '67.02', 'Assistance à maîtrise d''ouvrage.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (420, 110, '67.03', 'Assistance à maîtrise d''oeuvre.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (421, 110, '67.04', 'Achat et développement de progiciels.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (422, 110, '67.05', 'Achat et développement de logiciels.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (424, 110, '67.06', 'Maintenance logicielle :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (427, 110, '67.07', 'Traitements informatiques :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (428, 110, '67.08', 'Infogérance d''un système d''information :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (429, 110, '67.09', 'Services de banques de données :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (430, 110, '67.11', 'Maintenance des micro-ordinateurs, mini-ordinateurs, stations de travail, périphériques informatiques.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (431, 110, '67.12', 'Maintenance des équipements de réseaux informatiques.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (432, 110, '67.13', 'Maintenance de scanners', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (433, 110, '67.14', 'Maintenance de matériels audiovisuels', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (434, 129, '68.01', 'Hébergement en hôtel, pensions, demi-pension, auberges de jeunesse, refuges, camping ou autres.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (435, 129, '68.02', 'Services de restauration :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (436, 129, '68.03', 'Services des traiteurs (hors restauration collective) :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (440, 131, '69.01', 'Surveillance d''immeubles, garde, protection par vigiles', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (441, 131, '69.02', 'Télésurveillance / videosurveillance', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (442, 138, '70.01', 'Gestion de personnel : recrutement, conseil, organisation.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (443, 138, '70.02', 'Organisation des services : démarche qualité, audit, conseil, contrôle de gestion.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (444, 138, '70.03', 'Prestations de secrétariat et traduction.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (445, 138, '70.05', 'Enquêtes et sondages (hors communication).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (446, 138, '70.06', 'Études à caractère général (hors communication).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (447, 138, '70.07', 'Études et recherches scientifiques fondamentales et appliquées.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (448, 138, '70.08', 'Études à caractère technologique.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (449, 139, '71.01', 'Maîtrise d''oeuvre (hors services de maîtrise d''oeuvre pour les projets urbains) et ordonnancement, pilotage et coordination.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (450, 139, '71.02', 'Conduite d''opération.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (451, 139, '71.03', 'Études, analyses et contrôles nécessaires à la réalisation d''un ouvrage (à l''exclusion des analyses et essais des matériaux, produits et matériels d''installation et d''équipement immobilier - 80) :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (452, 145, '72.01', 'Agences et conseil en communication et publicité.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (453, 145, '72.02', 'Campagnes de communication (information, publicité, relations publiques).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (454, 145, '72.03', 'Achat et gestion d''espaces publicitaires.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (455, 145, '72.04', 'Organisation de colloques et événements (foires, salons).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (456, 145, '72.05', 'Réalisation de stands (salons, foires).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (457, 145, '72.06', 'Publications (conception).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (458, 145, '72.07', 'Études, sondages et enquêtes de communication.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (459, 145, '72.08', 'Services photographiques et audiovisuels de communication : y compris services des laboratoires photographiques et cinématographiques.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (460, 145, '72.09', 'Travaux graphiques de communication :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (461, 145, '72.10', 'Conception et réalisation de sites internet :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (462, 145, '72.11', 'Traitement de l''information :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (463, 145, '72.12', 'Mise en place de plates-formes téléphoniques.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (466, 156, '73.01', 'Nettoyage courant des locaux.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (467, 156, '73.04', 'Nettoyage spécifique des vitres.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (468, 156, '73.07', 'Désinfection, dératisation, désinsectisation. ', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (469, 156, '73.08', 'Nettoyage de véhicules.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (470, 156, '73.09', 'Blanchisserie, teinturerie.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (471, 156, '73.10', 'Location-entretien de linge (y compris hospitalier).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (472, 163, '74.03', 'Enlèvement, tri et stockage des ordures ménagères.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (474, 163, '74.05', 'Enlèvement, tri, stockage et traitement des déchets autres que ménagers ou nucléaires :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (475, 166, '75.01', 'Services de conseils juridiques : dans les différents domaines du droit, y compris en matière de propriété industrielle.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (476, 166, '75.02', 'Services d''établissements d''actes authentiques et des auxiliaires de justice.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (478, 166, '75.03', 'Services de représentation juridique.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (479, 169, '76.06', 'Prestations de services d''intérim de sages-femmes, d''infirmières et de personnels soignants et paramédicaux (1).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (480, 169, '76.15', 'Contrôle et analyses biologiques et autres analyses de laboratoire pour la santé humaine ou animale, à l''exclusion des analyses officielles réalisées dans le cadre des contrôles sanitaires vétérinaires relatifs à la protection de la santé humaine et de la santé animale.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (481, 173, '77.01', 'Services de conception, de production, de distribution, de projection, de traduction et de promotion ou de publicité de films ou d''oeuvres audiovisuelles et multimédias (comprend notamment les activités cinématographiques et de vidéo).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (539, 100, '100.52', 'toto', NULL, 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (464, 145, '72.13', 'Service de webinaires', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (482, 173, '77.02', 'Services de spectacles musicaux, de danse, de théâtre, de représentation artistique et de cirque, de spectacles de sons et lumières fournis par des producteurs ou des artistes amateurs ou professionnels', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (483, 173, '77.03', 'Services auxiliaires des activités de spectacle portant sur la réalisation et l''installation de décors, d''éclairages et de sonorisation, sur la conception et la réalisation de costumes, sur la scénographie, sur la traduction des spectacles et sur la vente de billets.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (484, 173, '77.05', 'Services d''agence de presse écrite, photographique, radio ou télédiffusée ou cinématographique.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (485, 173, '77.06', 'Services d''agence de reportage en direct aux stations de télévision.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (486, 173, '77.08', 'Services de gestion (acquisition, catalogage, conservation et recherche) d''archives publiques.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (487, 173, '77.14', 'Services d''organisation et de promotion des manifestations sportives.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (488, 180, '78.02', 'Services de qualification et d''insertion professionnelles :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (489, 180, '78.03', 'Formation professionnelle initiale destinée aux agents des collectivités publiques (hors services de qualification et d''insertion professionnelles).', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (490, 180, '78.04', 'Préparation aux concours ou examens professionnels destinée aux agents des collectivités publiques.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (491, 180, '78.05', 'Formation professionnelle continue destinée aux agents des collectivités publiques :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (492, 184, '79.01', 'Services des agences immobilières :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (493, 184, '79.02', 'Services d''administration d''immeubles :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (494, 184, '79.03', 'Conseil en immobilier.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (495, 184, '79.04', 'Services de promotion immobilière.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (496, 186, '80.01', 'Contrôle technique automobile.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (497, 186, '80.09', 'Analyses et essais d''équipements de mesure, de test et de santé :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (498, 199, '81.01', 'Maintenance des véhicules de transport de personnes :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (499, 199, '81.02', 'Maintenance des camions et des véhicules utilitaires.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (500, 199, '81.03', 'Maintenance des cycles et cyclomoteurs.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (501, 199, '81.10', 'Maintenance d''équipements mécaniques :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (502, 199, '81.11', 'Maintenance de machines d''usage général :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (503, 199, '81.15', 'Maintenance des appareils ménagers.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (504, 199, '81.16', 'Maintenance des machines de bureau (hors informatique) :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (505, 199, '81.17', 'Maintenance des machines et appareils électriques :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (506, 199, '81.18', 'Maintenance des matériels d''imagerie.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (507, 199, '81.23', 'Maintenance des équipements médicaux et techniques divers.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (508, 199, '81.25', 'Maintenance des équipements de laboratoire.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (509, 199, '81.26', 'Maintenance des machines et matériels de chauffage.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (510, 199, '81.27', 'Maintenance des matériels sanitaires et de plomberie.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (511, 199, '81.28', 'Maintenance d''installations de levage et de transport électro-mécaniques :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (512, 199, '81.29', 'Maintenance d''installations et d''équipements de protection contre l''incendie.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (513, 199, '81.30', 'Maintenance d''installations et d''équipements de contrôle des accès, de portes automatiques.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (514, 199, '81.31', 'Maintenance de matériels et équipements pour la gestion de biens immobiliers :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (515, 199, '81.32', 'Maintenance des bornes multi focntions', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (516, 199, '81.33', 'Maintenance de chariots élévateurs', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (517, 212, '82.01', 'Conception graphique, maquette.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (518, 212, '82.02', 'Pré-presse', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (519, 212, '82.03', 'Travaux d''impression offset.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (520, 212, '82.04', 'Autres travaux d''impression ou de reprographie.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (521, 212, '82.05', 'Travaux de façonnage de produits imprimés.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (522, 212, '82.06', 'Autres travaux de la chaîne graphique :', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (523, 212, '82.07', 'Reproduction d''enregistrements sonores et vidéo.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (524, 214, '84.02', 'Maintenance des espaces verts, parcs, jardins, plantations ornementales : y compris maintenance des jeux d''enfants installés dans les parcs et jardins.', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (525, 215, '85.01', 'Prestations de déménagements', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (526, 218, '86.01', 'Médecine du travail', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (528, 218, '86.02', 'Accompagnement social', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (529, 218, '86.03', 'Soutien psychologique', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (530, 218, '86.04', 'Tests de personnalité', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (531, 218, '86.05', 'Soutien psychologique par téléphone', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (532, 222, '87.01', 'Services d''impression', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (536, 227, '88.01', 'Intérim administratif et technique', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (537, 227, '88.02', 'Intérim informatique', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (314, 8, '14.02', 'Linge de maison, articles d''ameublement et de literie :', NULL, 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (313, 2, '10.03', 'Boissons alcooliques et non alcooliques', 'detail blalal de la nomenclature 10.03 et j''en rajoute 
et encore un peu plus
blabla bla', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (465, 145, '72.14', 'Service d''autorisation de reproduction d''œuvre protégée', '', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (328, 33, '18.10', 'Spécialités pharmaceutiques avec AMM : système respiratoire (codes ATC R).', 'ceci est un détail bien plus conséquentdshfshfdhsdfhlkjsdf sqdlkfjqslkdjflmkjsqdf
sdjflskdjfmlsdkjfmlkjsdf
dskjflksjdfmlksdjfmlk

oh oh oh', 1, 2020);
INSERT INTO public.nomenclature (id, famille_id, numero, intitule, detail_intitule, seuil_id, annee) VALUES (3, 2, '10.03', 'Boissons alcooliques et non alcooliques', 'detail blalal de la nomenclature 10.03 et j''en rajoute 
et encore un peu plus
blabla bla', 1, 2021);


--
-- TOC entry 3164 (class 0 OID 16705)
-- Dependencies: 197
-- Data for Name: nomenclature_fournisseur; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (1, 1, 9, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (2, 2, 48, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (3, 3, 48, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (4, 4, 50, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (5, 5, 62, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (6, 6, 64, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (7, 7, 64, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (9, 6, 67, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (10, 10, 67, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (11, 11, 77, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (12, 16, 77, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (13, 13, 79, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (14, 14, 80, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (15, 15, 80, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (16, 16, 80, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (17, 17, 87, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (19, 23, 91, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (18, 25, 91, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (21, 23, 93, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (20, 25, 93, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (23, 23, 95, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (22, 25, 95, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (24, 24, 97, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (26, 23, 98, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (25, 25, 98, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (27, 27, 106, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (28, 28, 107, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (29, 29, 112, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (30, 30, 114, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (31, 31, 114, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (32, 32, 114, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (33, 33, 114, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (34, 34, 121, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (35, 35, 122, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (36, 36, 123, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (40, 38, 126, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (37, 39, 126, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (41, 41, 130, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (42, 43, 132, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (43, 44, 136, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (44, 45, 139, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (45, 46, 140, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (46, 47, 141, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (49, 50, 156, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (50, 51, 157, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (51, 52, 162, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (52, 53, 162, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (53, 54, 164, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (54, 55, 166, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (55, 56, 166, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (56, 57, 168, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (57, 58, 176, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (58, 59, 190, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (59, 60, 201, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (60, 61, 205, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (61, 62, 206, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (62, 63, 214, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (63, 64, 215, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (64, 65, 216, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (65, 66, 216, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (66, 67, 218, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (67, 68, 219, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (69, 70, 222, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (71, 72, 222, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (72, 73, 226, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (73, 74, 227, NULL, NULL, NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (76, 8045, 86, NULL, NULL, NULL, NULL, true, 1, 2020);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (155, 16, 229, 'local', '54654654', NULL, NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (156, 45, 18, 'national', '789846', NULL, NULL, true, 1, 2020);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (74, 5683, 18, 'local', NULL, NULL, NULL, true, 1, 2020);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (153, 38, 3, 'regional', '789846', 'Cei est l''ovservation 1 de la 153', NULL, true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (75, 5717, 3, 'national', 'ZDEHKQSJDFH', 'Ceci est l''obsevation1 de la75', 'observation 2 tramalalalala etkj ljze lksdf sdf
kjsdflkjsdflkjfds sqlkjfskjsdkljfsd mlopkjsdmlkjmlsdfj 7777', true, 1, 2021);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (48, 49, 155, 'national', NULL, NULL, NULL, true, 1, 2020);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (47, 48, 154, 'national', NULL, NULL, NULL, true, 1, 2020);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (157, 4, 360, NULL, NULL, NULL, NULL, true, 1, 2020);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (158, 2, 358, NULL, NULL, NULL, NULL, true, 1, 2020);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (159, 2, 360, NULL, NULL, NULL, NULL, true, 1, 2020);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (160, 2, 357, NULL, NULL, NULL, NULL, true, 1, 2020);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (161, 7674, 443, NULL, NULL, NULL, NULL, true, 1, 2020);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (162, 7674, 445, NULL, NULL, NULL, NULL, true, 1, 2020);
INSERT INTO public.nomenclature_fournisseur (id, fournisseur_id, nomenclature_id, type_marche, num_marche, obs1, obs2, active, categorie_marche_id, annee) VALUES (165, 7069, 3, 'local', '55667', NULL, NULL, true, NULL, 2021);


--
-- TOC entry 3182 (class 0 OID 0)
-- Dependencies: 198
-- Name: nomenclature_fournisseur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nomenclature_fournisseur_id_seq', 165, true);


--
-- TOC entry 3183 (class 0 OID 0)
-- Dependencies: 199
-- Name: nomenclature_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nomenclature_id_seq', 232, true);


--
-- TOC entry 3167 (class 0 OID 16718)
-- Dependencies: 200
-- Data for Name: seuil; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.seuil (id, nom, seuil, seuil_alerte, annee) VALUES (1, 'Montee en charge', 10000.00, 8000.00, 2021);
INSERT INTO public.seuil (id, nom, seuil, seuil_alerte, annee) VALUES (2, 'Seuil2', 20000.00, 18000.00, 2021);
INSERT INTO public.seuil (id, nom, seuil, seuil_alerte, annee) VALUES (3, 'seuil3', 30000.00, 28000.00, 2021);
INSERT INTO public.seuil (id, nom, seuil, seuil_alerte, annee) VALUES (12, 'Seuil2', 20000.00, 18000.00, 2020);
INSERT INTO public.seuil (id, nom, seuil, seuil_alerte, annee) VALUES (13, 'seuil3', 30000.00, 28000.00, 2020);
INSERT INTO public.seuil (id, nom, seuil, seuil_alerte, annee) VALUES (11, 'Seuil1', 10000.00, 8000.00, 2020);
INSERT INTO public.seuil (id, nom, seuil, seuil_alerte, annee) VALUES (6, 'Seuil1', 23344555.00, 444.00, 2021);


--
-- TOC entry 3184 (class 0 OID 0)
-- Dependencies: 201
-- Name: seuil_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seuil_id_seq', 7, true);


--
-- TOC entry 2996 (class 2606 OID 16736)
-- Name: categorie_marche categorie_marche_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categorie_marche
    ADD CONSTRAINT categorie_marche_pkey PRIMARY KEY (id);


--
-- TOC entry 2998 (class 2606 OID 16738)
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- TOC entry 3000 (class 2606 OID 16740)
-- Name: facture facture_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.facture
    ADD CONSTRAINT facture_pkey PRIMARY KEY (id);


--
-- TOC entry 3004 (class 2606 OID 16742)
-- Name: facturenok facturenok_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.facturenok
    ADD CONSTRAINT facturenok_pkey PRIMARY KEY (id);


--
-- TOC entry 3008 (class 2606 OID 16744)
-- Name: famille_nomenclature famille_nomenclature_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.famille_nomenclature
    ADD CONSTRAINT famille_nomenclature_pkey PRIMARY KEY (id);


--
-- TOC entry 3010 (class 2606 OID 16746)
-- Name: fournisseur fournisseur_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fournisseur
    ADD CONSTRAINT fournisseur_pkey PRIMARY KEY (id);


--
-- TOC entry 3020 (class 2606 OID 16748)
-- Name: nomenclature_fournisseur nomenclature_fournisseur_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nomenclature_fournisseur
    ADD CONSTRAINT nomenclature_fournisseur_pkey PRIMARY KEY (id);


--
-- TOC entry 3015 (class 2606 OID 16750)
-- Name: nomenclature nomenclature_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nomenclature
    ADD CONSTRAINT nomenclature_pkey PRIMARY KEY (id);


--
-- TOC entry 3022 (class 2606 OID 16752)
-- Name: seuil seuil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seuil
    ADD CONSTRAINT seuil_pkey PRIMARY KEY (id);


--
-- TOC entry 3012 (class 1259 OID 16753)
-- Name: idx_799a365297a77b84; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_799a365297a77b84 ON public.nomenclature USING btree (famille_id);


--
-- TOC entry 3013 (class 1259 OID 16754)
-- Name: idx_799a3652b94ef78f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_799a3652b94ef78f ON public.nomenclature USING btree (seuil_id);


--
-- TOC entry 3005 (class 1259 OID 16755)
-- Name: idx_9c61dbb859af2cdf; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_9c61dbb859af2cdf ON public.facturenok USING btree (nomenclature_fournisseur_id);


--
-- TOC entry 3006 (class 1259 OID 16756)
-- Name: idx_9c61dbb8670c757f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_9c61dbb8670c757f ON public.facturenok USING btree (fournisseur_id);


--
-- TOC entry 3016 (class 1259 OID 16757)
-- Name: idx_f23550ae33164616; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_f23550ae33164616 ON public.nomenclature_fournisseur USING btree (categorie_marche_id);


--
-- TOC entry 3017 (class 1259 OID 16758)
-- Name: idx_f23550ae670c757f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_f23550ae670c757f ON public.nomenclature_fournisseur USING btree (fournisseur_id);


--
-- TOC entry 3018 (class 1259 OID 16759)
-- Name: idx_f23550ae90bfd4b8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_f23550ae90bfd4b8 ON public.nomenclature_fournisseur USING btree (nomenclature_id);


--
-- TOC entry 3001 (class 1259 OID 16760)
-- Name: idx_fe86641059af2cdf; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fe86641059af2cdf ON public.facture USING btree (nomenclature_fournisseur_id);


--
-- TOC entry 3002 (class 1259 OID 16761)
-- Name: idx_fe866410670c757f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fe866410670c757f ON public.facture USING btree (fournisseur_id);


--
-- TOC entry 3011 (class 1259 OID 16762)
-- Name: uniq_369eca3251c48ad4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uniq_369eca3251c48ad4 ON public.fournisseur USING btree (num_tiers);


--
-- TOC entry 3027 (class 2606 OID 16763)
-- Name: nomenclature fk_799a365297a77b84; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nomenclature
    ADD CONSTRAINT fk_799a365297a77b84 FOREIGN KEY (famille_id) REFERENCES public.famille_nomenclature(id);


--
-- TOC entry 3028 (class 2606 OID 16768)
-- Name: nomenclature fk_799a3652b94ef78f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nomenclature
    ADD CONSTRAINT fk_799a3652b94ef78f FOREIGN KEY (seuil_id) REFERENCES public.seuil(id);


--
-- TOC entry 3025 (class 2606 OID 16773)
-- Name: facturenok fk_9c61dbb859af2cdf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.facturenok
    ADD CONSTRAINT fk_9c61dbb859af2cdf FOREIGN KEY (nomenclature_fournisseur_id) REFERENCES public.nomenclature_fournisseur(id);


--
-- TOC entry 3026 (class 2606 OID 16778)
-- Name: facturenok fk_9c61dbb8670c757f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.facturenok
    ADD CONSTRAINT fk_9c61dbb8670c757f FOREIGN KEY (fournisseur_id) REFERENCES public.fournisseur(id);


--
-- TOC entry 3029 (class 2606 OID 16783)
-- Name: nomenclature_fournisseur fk_f23550ae33164616; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nomenclature_fournisseur
    ADD CONSTRAINT fk_f23550ae33164616 FOREIGN KEY (categorie_marche_id) REFERENCES public.categorie_marche(id);


--
-- TOC entry 3030 (class 2606 OID 16788)
-- Name: nomenclature_fournisseur fk_f23550ae670c757f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nomenclature_fournisseur
    ADD CONSTRAINT fk_f23550ae670c757f FOREIGN KEY (fournisseur_id) REFERENCES public.fournisseur(id);


--
-- TOC entry 3031 (class 2606 OID 16793)
-- Name: nomenclature_fournisseur fk_f23550ae90bfd4b8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nomenclature_fournisseur
    ADD CONSTRAINT fk_f23550ae90bfd4b8 FOREIGN KEY (nomenclature_id) REFERENCES public.nomenclature(id);


--
-- TOC entry 3023 (class 2606 OID 16798)
-- Name: facture fk_fe86641059af2cdf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.facture
    ADD CONSTRAINT fk_fe86641059af2cdf FOREIGN KEY (nomenclature_fournisseur_id) REFERENCES public.nomenclature_fournisseur(id);


--
-- TOC entry 3024 (class 2606 OID 16803)
-- Name: facture fk_fe866410670c757f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.facture
    ADD CONSTRAINT fk_fe866410670c757f FOREIGN KEY (fournisseur_id) REFERENCES public.fournisseur(id);


-- Completed on 2021-11-12 10:54:01 CET

--
-- PostgreSQL database dump complete
--

