--crée une table temporaire
create table itmp
(
    a character varying(1024) COLLATE pg_catalog."default",
    b character varying(1024) COLLATE pg_catalog."default",
    c character varying(1024) COLLATE pg_catalog."default",
    d character varying(1024) COLLATE pg_catalog."default",
    e character varying(1024) COLLATE pg_catalog."default",
    f character varying(1024) COLLATE pg_catalog."default",
    g character varying(1024) COLLATE pg_catalog."default",
    h character varying(1024) COLLATE pg_catalog."default",
    i character varying(1024) COLLATE pg_catalog."default",
    j character varying(1024) COLLATE pg_catalog."default",
    k character varying(1024) COLLATE pg_catalog."default",
    l character varying(1024) COLLATE pg_catalog."default",
    m character varying(1024) COLLATE pg_catalog."default",
    n character varying(1024) COLLATE pg_catalog."default",
    o character varying(1024) COLLATE pg_catalog."default",
    p character varying(1024) COLLATE pg_catalog."default",
    q character varying(1024) COLLATE pg_catalog."default",
    r character varying(1024) COLLATE pg_catalog."default",
    s character varying(1024) COLLATE pg_catalog."default",
    t character varying(1024) COLLATE pg_catalog."default"
);
copy itmp
from '/tmp/dataencode.csv'
delimiter ';';
--from '/res/dataencode1.csv'

--Crée la table d'import
create table tmpIFacture(
A character varying(1024)
	,B character varying(1024)
	,C character varying(1024)
	,D character varying(1024)
	,E character varying(1024)
	,F character varying(1024)
	,G character varying(1024)
	,H character varying(1024)
	,I character varying(1024)
	,J character varying(1024)
	,K character varying(1024)
	,L character varying(1024)
	,M character varying(1024)
	,N character varying(1024)
	,O character varying(1024)
	,P character varying(1024)
	,Q character varying(1024)
	,R character varying(1024)
	,S character varying(1024)
	,T character varying(1024)
	,idFournisseur int
	,idNomenclaturefournisseur int
);

--Copie dans tmpiFactures
INSERT INTO public.tmpifacture(
	a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t)
	select a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t
	from itmp;
	
--supprime la table temporaire
drop table itmp;
--Supprime les doublons
delete FROM tmpIfacture a 
WHERE a.ctid <> (SELECT min(b.ctid) FROM tmpIfacture b 
WHERE 
a.C = b.C and
a.F = b.F and
a.H = b.H and
a.Q = b.Q and
a.S = b.S and
a.T = b.T) 
; 
--Supprimer les enregistrements dont la colonne B <>85 et <>58
delete FROM tmpIfacture a 
WHERE a.B<>'85' and a.B<>'58';
--supprimer les enregistrements dont la colone P est différente de 'F','H', 'L'
delete from tmpifacture a 
where a.P not in('F','H','L');
--Supprime le facture dont le montant est =0
delete from tmpifacture a 
WHERE a.s = '0';
--remplacement de la virgule par un point dans les montants
UPDATE 
   tmpiFacture
SET 
   S = REPLACE(S,',','.'),
   Q=replace(Q,'.0','');
--récupère les nouveaux fournisseurs
INSERT INTO public.fournisseur(
	id, num_tiers, nom,rejet)
select distinct on(cast(a.Q as bigint))  nextval('fournisseur_id_seq'),cast(a.Q as bigint) as nomTiers, a.r,FALSE 
from tmpifacture a
where (a.Q <>'' and a.R  <>'' and
cast(a.Q as bigint) not in(select num_tiers from fournisseur ));

--Charge les id des Fournisseur
update tmpifacture set idfournisseur=id
from fournisseur
where fournisseur.num_tiers=cast(tmpifacture.Q as bigint);

--Charge les idNomenclature pour les fournisseurs rattachés à une nomenclature
update tmpIfacture set idnomenclaturefournisseur=id
from nomenclature_fournisseur
where nomenclature_fournisseur.fournisseur_id=tmpiFacture.idfournisseur and nomenclature_fournisseur.annee=cast(tmpiFacture.A as integer)and nomenclature_fournisseur.active=TRUE

and nomenclature_fournisseur.fournisseur_id not in(
	SELECT    fournisseur_id
	FROM     nomenclature_fournisseur
 	where  nomenclature_fournisseur.active=TRUE
 
GROUP BY fournisseur_id,annee
HAVING   COUNT(*) > 1);
--copie dans factures les enregistrements complets
insert into facture(id, fournisseur_id
					, nomenclature_fournisseur_id
					, annee
					, montant
					, chemin_doc
					, organisme
				   	, compte
				   	, description
				   	, quantieme
				   	, dateimport
					,datefacture)
(select  nextval('facture_id_seq')
,tmpiFacture.idfournisseur
,tmpifacture.idnomenclaturefournisseur
,cast(tmpifacture.A as integer)
,cast(trim(tmpiFacture.S) as numeric(10,2))
,tmpiFacture.H
,cast(tmpifacture.B as integer)
,cast(tmpifacture.C as integer)
,cast(tmpifacture.E as Text)
,cast(tmpifacture.F as smallint)
, NOW() 
,quantiemetodate(cast(tmpifacture.A as integer)
,cast(tmpifacture.F as integer))
from tmpiFacture inner join nomenclature_fournisseur
on tmpiFacture.idfournisseur=nomenclature_fournisseur.fournisseur_id and
tmpiFacture.idnomenclaturefournisseur=nomenclature_fournisseur.id
where tmpiFacture.idnomenclaturefournisseur is not null);
--sauve les factures pour import manuel
insert into facturenok(id
					   	, fournisseur_id
					   	, nomenclature_fournisseur_id
					   	, annee
					   	, montant
					   	, chemin_doc
					  	, organisme
				   		, compte
				   		, description
				   		, quantieme
				   		, dateimport
					   	,datefacture)
(select  nextval('facturenok_id_seq')
,tmpiFacture.idfournisseur
,tmpifacture.idnomenclaturefournisseur
,cast(tmpifacture.A as integer)
,cast(trim(tmpiFacture.S) as numeric(10,2))
 ,tmpiFacture.H
,cast(tmpifacture.B as integer)
,cast(tmpifacture.C as integer)
,cast(tmpifacture.E as Text)
,cast(tmpifacture.F as smallint)
, NOW()
,quantiemetodate(cast(tmpifacture.A as integer)
,cast(tmpifacture.F as integer))
from tmpiFacture 
where tmpiFacture.idnomenclaturefournisseur is null);

--Supprime la table d'import
drop table tmpIFacture;