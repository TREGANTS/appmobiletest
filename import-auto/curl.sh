source ./conf.ini

echo "*****************************************************************************">>log/"${LOG}"
echo "    execution de la requete et recuperation dans resultatquid.csv           *" >>log/"${LOG}"
echo "*****************************************************************************">>log/"${LOG}"

#execution de la requete QUID et recuperation dans resultatquid.csv

#if [ ! -f "res/resultatquid.csv" ]
#then
	HTTP_CODE=$(curl --write-out "%{http_code}\n" $1  --output res/output.csv --silent)
#else
#	HTTP_CODE=$(curl --write-out "%{http_code}\n" $1 --output res/output.csv --silent)
#fi

if [[ $HTTP_CODE -ne 200 ]]
then
	
	echo "Erreur Code retour CURL "$HTTP_CODE>>log/"${LOG}"
	exit 1
fi

if [ ! -f "res/output.csv" ]
then
	
	echo "Aucun fichier de resultat - output.csv">>log/"${LOG}"
	exit 2

fi

#recuperation des erreurs
PRB_QUID=`grep -c "quid erreur" res/output.csv`
if [ $PRB_QUID -ne 0 ] 
then
		
	echo "opération requetage échouée" >>log/"${LOG}"
	echo `grep "quid erreur" res/output.csv`>>log/"${LOG}"		
	exit 3
else
	echo "opération requetage requete "$url1" OK">>log/"${LOG}"
	echo "*************************************************************">>log/"${LOG}"
fi


