source ./conf.ini
# $1 url1
# $2 url2

echo " execution requete URL QUID = "$1 >>log/"${LOG}"

#execution de la requete url1 et recuperation dans output.csv
./curl.sh $1
retval=$?
if [ $retval -ne 0 ]
then
#ode erreur - envoi mail aux admins
	cat log/result.log | mail -s "Resultat requetage QUID pour Nomac" "${MAILS}"
	exit 1
fi

cat res/output.csv>res/resultatquid.csv

#execution de la requete url2 et recuperation dans output.csv
echo " execution requete URL QUID = "$2 >>log/"${LOG}"
./curl.sh $2
retval=$?
if [ $retval -ne 0 ]
then
	cat log/"${LOG}" | mail -s "Resultat requetage QUID pour Nomac" "${MAILS}"
	exit 1
fi
#cat res/output.csv>res/resultatquid1.csv
cat res/output.csv>>res/resultatquid.csv

#conversion en utf8

echo "Encodage utf8 du fichier resultatquid.csv" >>log/"${LOG}"
iconv -f "iso-8859-1" -t "utf8" res/resultatquid.csv > res/dataencode.csv

if [ ! -f "res/dataencode.csv" ]
then
	echo "Aucun fichier de données encodées">>log/"${LOG}"
	exit 2
fi

#Suppression des lignes vides (certaines sont générees lors de la concatenation)
echo "Suppression des lignes vides">>log/"${LOG}"
sed '/^$/d' res/dataencode.csv>/tmp/dataencode.csv

echo "*****************************************************************************">>log/"${LOG}"
echo "          execution du fichier sql dans postgres                           *" >>log/"${LOG}"
echo "*****************************************************************************">>log/"${LOG}"

#Verification de la présence du fichier sql
if [ ! -f "sql/importFactures.sql" ]
then

	echo "Fichier sql absent">>log/"${LOG}"
	exit 1
fi

export PGPASSWORD=${PGSQL_PASSWORD}
ret=$(psql -U ${PGSQL_USER} -d ${PGSQL_DATABASE} -v ON_ERROR_STOP=1 -f sql/importFactures.sql 2>log/postgres.log)
echo $ret
if [ -s "log/postgres.log" ]
then
	echo "Erreur lors de l'execution du fichier SQL dans postgres">>log/"${LOG}"
	cat log/postgres.log>>log/"${LOG}"
	exit 2	
else
	echo "Execution du fichier SQL OK">>log/"${LOG}"
fi
rm log/postgres.log
