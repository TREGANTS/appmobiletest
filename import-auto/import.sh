source ./conf.ini

mkdir -p -m 755 log
mkdir -p -m 755 res

#calcul des quantiemes en fonction de la date du jour
#JOUR=$(date +"%Y%m%d%H%M")
JOUR=$(date +"%d/%m/%Y-%H%M")

echo "*****************************************************************************">log/"${LOG}"
echo "                         Début de traitement : "$JOUR  >>log/"${LOG}"
echo "*****************************************************************************">>log/"${LOG}"
echo "*****************************************************************************">>log/"${LOG}"
echo "                         calcul des Quantieme                               *">>log/"${LOG}"
echo "*****************************************************************************">>log/"${LOG}"
today=$(date +%Y%m%d)
#test avec le 03 fevrier 2021 il y aura les quantiemes
#today=20210203 
#premier jour mois en cours

jour1moisencours=$(date -d $today +%Y-%m-01)
jour2=$(date -d "$jour1moisencours -1 days" +%Y-%m-%d)

jour1=$(date -d $jour2 +%Y-%m-01)
exercice=$(date -d $jour2 +%Y)

q1=$(date -d $jour1 +%j)
q2=$(date -d $jour2 +%j)
#q1=335
#q2=365
#exercice=2021
echo " Quantième inférieur : "$q1>>log/"${LOG}"
echo " Quantième supérieur : "$q2>>log/"${LOG}"

echo "*****************************************************************************">>log/"${LOG}"
echo "                         calcul des URL QUID                               *" >>log/"${LOG}"
echo "*****************************************************************************">>log/"${LOG}"

#calcul url quid

url1='http://quid.ficorh.cnamts.fr/qdp20/service/exeQuid.php?idUser='${idUser}'&passMD5='${passMD5}'&idReq='${requete1}'&param=exercice='$exercice';quantinf='$q1';quantsup='$q2'&pfusmut=311&sortie=csv'

url2='http://quid.ficorh.cnamts.fr/qdp20/service/exeQuid.php?idUser='${idUser}'&passMD5='${passMD5}'&idReq='${requete2}'&param=exercice='$exercice';quantinf='$q1';quantsup='$q2'&pfusmut=311&sortie=csv'

#traitement
./quid6.sh $url1 $url2
retval=$?
if [ $retval -ne 0 ]
then
#envoi mail aux admins
	cat log/"${LOG}"| mail -s "Resultat requetage QUID pour Nomac" "${MAILS}"
	exit 1
fi


#traitement de quantiemes > 367

if [ $(date +%m) != '02' ]
then
#envoi mail aux admins et on s'arrête là
	echo "Pas de quantieme supérieur à 365 à traiter ce mois-ci">>log/"${LOG}"
	cat log/"${LOG}" | mail -s "Resultat requetage QUID pour Nomac" "${MAILS}"
	exit 1
fi
	
echo "*****************************************************************************">>log/"${LOG}"
echo "                         traitement des quantiemes  > 367                      *" >>log/"${LOG}"
echo "*****************************************************************************">>log/"${LOG}"

#calcul exercice precedent
exerciceprec=$((exercice-1))
url1='http://quid.ficorh.cnamts.fr/qdp20/service/exeQuid.php?idUser='${idUser}'&passMD5='${passMD5}'&idReq='${requete1}'&param=exercice='$exerciceprec';quantinf='${QinfQuantieme}';quantsup='${QsupQuantieme}'&pfusmut=311&sortie=csv'

url2='http://quid.ficorh.cnamts.fr/qdp20/service/exeQuid.php?idUser='${idUser}'&passMD5='${passMD5}'&idReq='${requete2}'&param=exercice='$exerciceprec';quantinf='${QinfQuantieme}';quantsup='${QsupQuantieme}'&pfusmut=311&sortie=csv'

echo " Execution et traitement requete URL QUID Q>367" >>log/"${LOG}"

./quid6.sh $url1 $url2

#ne pas oublier de traiter le cas ou le fichier existe mais avec aucun resultat de marqué

#FIN envoi mail aux admins
cat log/"${LOG}" | mail -s "Resultat requetage QUID pour Nomac" "${MAILS}"
exit 0
