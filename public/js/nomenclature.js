$(document).ready(function() {
    // var annee = (new Date()).getFullYear(); alert(annee);
    // On récupère à partir de l'url active l'annee
    const querystring=window.location.pathname;
    var taburl=querystring.split('/');
    var annee=taburl[2];

    const table = $('#nomac').DataTable( {
        //permet de ne pas conserver le resultat de recherche en memoire
        stateSave: false,
        fixedHeader: true,
        lengthMenu: [100, 50, 25, 10],
        // pageLength: 100,
        // lengthChange: true,

        paging : true,
        columnDefs: [ {
            type: 'string',
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        },
        {
           targets: 6,
            render: $.fn.dataTable.render.ellipsis( 60,true,true )
        },
            {
                targets: 3,
                render: $.fn.dataTable.render.ellipsis( 200,true,true )
            }],
        select: {
            style:    'os',
            selector: 'td:first-child',
            blurable: true
        },
        // order: [[ 1, 'asc' ]],

        //filtrage au clic
        "initComplete": function () {
            var api = this.api();
            api.$('td').not('.select-checkbox').click(function(){
                    api.search( table.cell( this ).data() ).draw();
                })
        //    }
        },
    } );

   // new $.fn.dataTable.FixedHeader( table, { bottom: true } );
    //ajout des boutons copy excel pdf
    new $.fn.dataTable.Buttons( table, {
        name: 'commands-export',
        className : 'btn-info',
        buttons: [
            'copy', 'excel', 'pdf']
    });


    // ajout des boutons de commande
    new $.fn.dataTable.Buttons( table, {
        name: 'commands',
        buttons: [
            {
                text: 'Ajouter',
                attr: {
                    id: 'addButton',
                    disabled: function(){
                        if (annee===(new Date()).getFullYear().toString()){
                            return false;
                        }else{
                            return true;
                        }
                    }
                },
                className: 'btn btn-success fa fa-plus-square',
                action: function ( e, dt, node, config ) {
                    window.location.href = "/nomenclature/new/"+ annee;
                }
            },
            {
                extend: 'selectedSingle',
                text: 'Modifier',
                attr: {
                    id: 'updateButton',
                    disabled: true
                },
                className : 'btn-warning fa fa-edit',
                action: function ( e, dt, node, config ) {

                    // récup ligne selectionnée
                    const data = table.row('.selected').data();
                    // récup id row
                    const id =data[1];
                    window.location.href = '/nomenclature/'+id+'/edit/';
                }
            },
            {
                extend: 'selectedSingle',
                text: 'Supprimer',
                attr:  {
                    // title: 'Copy',
                    id: 'deleteButton',
                    disabled: true
                },
                className: 'btn btn-danger fas fa-times',
                action: function ( e, dt, node, config ) {
                    if (confirm("Êtes vous certain de vouloir supprimer cette ligne de nomenclature ?")){
                        // récup ligne selectionnée
                        const data = table.row('.selected').data();
                        // récup id row
                        const id =data[1];
                        window.location.href = '/nomenclature/delete/'+id+'';
                    }
                }
            },
        ],
    } );

    //desactivation - activation bouton d'ajout
    //si ce n'est pas l'année en cours bouton ajouter désactivé
// //desactivation - activation des boutons
     table.on( 'select', function ( e, dt, type, indexes ) {
         if ( type === 'row' ) {
            if( table.rows({selected: true}).count() === 1){
                //au moins une row est sélectionnée - boutons modifier et supprimer enabled
                $("#updateButton").removeAttr('disabled');
                $("#deleteButton").removeAttr('disabled');
             }
          } });

     table.on( 'deselect', function ( e, dt, type, indexes ) {
         if ( type === 'row' ) {
             if (table.rows({selected: true}).count() === 0) {
                 $("#updateButton").attr('disabled','disabled');
                 $("#deleteButton").attr('disabled','disabled');
             }
         }
         });

    //ajout des boutons print excel etc
    table.buttons().container()
        .appendTo( $('.col-sm-6:eq(1)', table.table().container() ) );
    table.buttons('commands',null).container()
        .appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );

    //entête de table fixe
    var fh1 = new $.fn.dataTable.FixedHeader(table, {
        alwaysCloneTop: true,
    });

//changement d'exercice

});
$('#exercice').change(function(){
    //changement d'exercice recuperation annee*
    var newAnnee=$('#exercice').val();
    window.location.href='/nomenclature/' + newAnnee;
});