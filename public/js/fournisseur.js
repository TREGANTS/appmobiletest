$(document).ready(function() {
    const table = $('#nomac').DataTable( {
        /*
  lengthMenu: [
      [ 100 ],
      ['100']
  ],*/
       // responsive: true,
        stateSave: false,
        fixedHeader: true,
        //colonne select checkbox
        columnDefs: [ {
            type: 'string',
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ,
        {
            //colomne nomenclature active
            targets:   4,
            orderable: true,
            render: function(data,type){
                if ( type === 'display' ) {
                    return (data === 'rejet') ? '<span class="glyphicon glyphicon-ok-circle" style="color:red"></span>' : '<span> </span>';
                }
                return data;
            }}
        ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]],

        //filtrage au clic
        "initComplete": function () {
            var api = this.api();
            $('#nomac tbody td').not('.select-checkbox').on('click',function(){
                api.search( table.cell( this ).data() ).draw();
            })
        },
    } );

    //ajout des boutons copy excel pdf
    new $.fn.dataTable.Buttons( table, {
        name: 'commands-export',
        className : 'btn-info',
        buttons: [
            'copy', 'excel', 'pdf']
    });


    // ajout des boutons de commande
    new $.fn.dataTable.Buttons( table, {
        name: 'commands',
        buttons: [
            {
                text: 'Ajouter',
                className: 'btn btn-success fa fa-plus-square',
                action: function ( e, dt, node, config ) {
                    window.location.href = "/fournisseur/new";
                }
            },
            {
                text: 'Voir',
                attr: {
                    id: 'showButton',
                    disabled: true
                },
                className : 'btn btn-primary fa fa-search',
                action: function ( e, dt, node, config ) {

                    // récup ligne selectionnée
                    const data = table.row('.selected').data();
                    // récup id row
                    const id =data[1];
                    window.location.href = '/fournisseur/'+id+'';
                }
            },
            {
                text: 'Modifier',
                attr: {
                    id: 'updateButton',
                    disabled: true
                },
                className : 'btn-warning fa fa-edit',
                action: function ( e, dt, node, config ) {

                    // récup ligne selectionnée
                    const data = table.row('.selected').data();
                    // récup id row
                    const id =data[1];

                    window.location.href = '/fournisseur/'+id+'/edit/';


                }
            },
            {
                text: 'Supprimer',
                attr:  {
                    // title: 'Copy',
                    id: 'deleteButton',
                    disabled: true
                },
                className: 'btn btn-danger fas fa-times',
                action: function ( e, dt, node, config ) {
                    if (confirm("Êtes vous certain de vouloir supprimer ce fournisseur ?")){
                        // récup ligne selectionnée
                        const data = table.row('.selected').data();
                        // récup id row
                        const id =data[1];
                        window.location.href = '/fournisseur/delete/'+id+'';
                    }
                }
            },
        ],
    } );
//desactivation des boutons

    table.on( 'select', function ( e, dt, type, indexes ) {

        if ( type === 'row' ) {
            if( table.rows({selected: true}).count() === 1){
                //au moins une row est sélectionnée - boutons modifier et supprimer enabled
                $("#updateButton").removeAttr('disabled');
                $("#showButton").removeAttr('disabled');
                $("#deleteButton").removeAttr('disabled');
                //    var data = table.rows( indexes ).data().pluck( 'id' );
            }
        } });
    table.on( 'deselect', function ( e, dt, type, indexes ) {
        if ( type === 'row' ) {

            if (table.rows({selected: true}).count() === 0) {
                $("#updateButton").attr('disabled','disabled');
                $("#showButton").attr('disabled','disabled');
                $("#deleteButton").attr('disabled','disabled');
            }
        }
    });

    //ajout des boutons print excel etc
    table.buttons().container()
        .appendTo( $('.col-sm-6:eq(1)', table.table().container() ) );
    table.buttons('commands',null).container()
        .appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );
});