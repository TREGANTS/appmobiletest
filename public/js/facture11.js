$(document).ready(function() {
    const table = $('#nomac').DataTable( {
        /*
    lengthMenu: [
        [ 100 ],
        ['100']
    ],*/
        stateSave: false,
        fixedHeader: true,
       // responsive: true,
        //select checkbox
        columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        },
            {
                targets: 5,
                render:  $.fn.dataTable.render.number( ' ', ',', 2,'' ,' €')
            },
            {
                targets: 6,
                render: $.fn.dataTable.render.moment( 'DD-MM-YYYY','DD/MM/YYYY')
            },
            {
                targets: 9,
                render: $.fn.dataTable.render.moment( 'DD-MM-YYYY','DD/MM/YYYY')
            },

        ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]],

        //filtrage au clic
        "initComplete": function () {
            var api = this.api();
            $('#nomac tbody td').not('.select-checkbox, .linka').on('click',function(){
                api.search( table.cell( this ).data() ).draw();
            })
        }
    } );

    //ajout des boutons copy excel pdf
    new $.fn.dataTable.Buttons( table, {
        name: 'commands-export',
        className : 'btn-info',
        buttons: [
            'copy', 'excel', 'pdf']
    });

    // ajout des boutons de commande
    new $.fn.dataTable.Buttons( table, {
        name: 'commands',
        buttons: [

            {
                text: 'Voir',
                attr: {
                    id: 'showButton',
                    disabled: true
                },
                className : 'btn btn-primary fa fa-search',
                action: function ( e, dt, node, config ) {

                    // récup ligne selectionnée
                    const data = table.row('.selected').data();
                    // récup id row
                    const id =data[1];
                    window.location.href = '/facture/'+id+'';
                }
            },

            {
                text: 'Modifier',
                attr: {
                    id: 'updateButton',
                    disabled: true
                },
                className : 'btn-warning fa fa-edit',
                action: function ( e, dt, node, config ) {

                    // récup ligne selectionnée
                    const data = table.row('.selected').data();
                    // récup id row
                    const id =data[1];

                    window.location.href = '/facture/'+id+'/edit/';

                }
            },

            {
                text: 'Supprimer',
                attr:  {
                    // title: 'Copy',
                    id: 'deleteButton',
                    disabled: true
                },
                className: 'btn btn-danger fas fa-times',
                action: function ( e, dt, node, config ) {
                    if (confirm("Êtes vous certain de vouloir supprimer cette facture ?")){
                        // récup ligne selectionnée
                        const data = table.row('.selected').data();
                        // récup id row
                        const id =data[1];
                        window.location.href = '/facture/delete/'+id+'';
                    }
                }
            },
        ],
    } );
//desactivation des boutons

    table.on( 'select', function ( e, dt, type, indexes ) {

        if ( type === 'row' ) {
           if( table.rows({selected: true}).count() === 1){
               //au moins une row est sélectionnée - boutons modifier et supprimer enabled
               $("#updateButton").removeAttr('disabled');
               $("#showButton").removeAttr('disabled');
               $("#deleteButton").removeAttr('disabled');
      //    var data = table.rows( indexes ).data().pluck( 'id' );
            }
         } });
    table.on( 'deselect', function ( e, dt, type, indexes ) {
        if ( type === 'row' ) {

            if (table.rows({selected: true}).count() === 0) {

                $("#updateButton").attr('disabled','disabled');
                $("#showButton").attr('disabled','disabled');
                $("#deleteButton").attr('disabled','disabled');
            }
        }
        });

    //ajout des boutons print excel etc
    table.buttons().container()
        .appendTo( $('.col-sm-6:eq(1)', table.table().container() ) );
    table.buttons('commands',null).container()
        .appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );
});