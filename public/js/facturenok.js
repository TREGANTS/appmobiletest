$(document).ready(function() {
    var testfournisseur ="";
    var testannee ="";
    var totrows = "";
    var recupid= "";
    var recupidfrn= "";
    var recupannee= "";
    const table = $('#nomac').DataTable( {

        stateSave: false,
        fixedHeader: true,
       // responsive: true,
        //select checkbox
        columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
            } ,
            {
                targets: 5,
                render: $.fn.dataTable.render.moment( 'DD-MM-YYYY','DD/MM/YYYY')
            },

            {
                targets: 6,
                render:  $.fn.dataTable.render.number( ' ', ',', 2,'' ,' €')

            },
            {
            //colonne de visualisation de la facture
            targets:8,
            orderable:false
            }


            ],
        select: {
            style:    'multi',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]],

        initComplete: function () {
            //filtrage au clic
            var api = this.api();
            $('#nomac tbody td').not('.select-checkbox, .linka' ).on('click',function(){
                api.search( table.cell( this ).data() ).draw();
            }),
            this.api().columns([2,3]).every( function () {
                var column = this;

                var select = $('<select><option value=""></option></select>')
                    //.appendTo( $(column.footer()).empty() )
                    .appendTo( $(column.header()) )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()

                        );
                        //changement de critères => nettoyage des selections
                        table.rows().deselect();
                        // colonne selectionnée
                        var colselect = column.selector.cols.valueOf();

                        // colonne fournisseur
                        if (colselect ===2 ) {
                            testfournisseur= val;

                        }
                        else if(colselect ===3) {
                            testannee= val;

                        }

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();

                        // to avoid conflicts with the column ordening use with .appendTo( $(column.header()) )
                        $( select ).click( function(e) {
                            e.stopPropagation();
                        });


                    } );


                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }

    } );

    //ajout des boutons copy excel pdf
    new $.fn.dataTable.Buttons( table, {
        name: 'commands-export',
        className : 'btn-info',
        buttons: [
            'copy', 'excel', 'pdf']
    });


    //ajout des boutons de commande
    new $.fn.dataTable.Buttons( table, {
        name: 'commands',
        buttons: [
            {
                text: 'Intégrer',
                attr: {
                    id: 'updateButton',
                    disabled: true
                },
                className : 'btn-warning fa fa-edit',
                action: function ( e, dt, node, config ) {
                    window.location.href = '/facturenok/'+recupid+'/'+recupidfrn+'/'+recupannee+'/edit/';

                }
            },
            {
                text: 'Supprimer',
                attr:  {
                    id: 'deleteButton',
                    disabled: true
                },
                className: 'btn btn-danger fas fa-times',
                action: function ( e, dt, node, config ) {
                    if (confirm("Êtes vous certain de vouloir supprimer cette ou ces facture(s) ?")){
                        window.location.href = '/facturenok/delete/'+recupid+'';
                    }
                }
            },
        ]
    } );
//desactivation des boutons

    table.on( 'select', function ( e, dt, type, indexes ) {
        const data2 = table.row('.selected').data();
        if ( type === 'row' ) {
            if( table.rows({selected: true}).count() > 1 && (testfournisseur =="" || testannee =="")){
                //au moins une row est sélectionnée - boutons modifier et supprimer enabled
                alert('Veuillez appliquer un filtre en sélectionnant un fournisseur et une année !');
                table.rows().deselect();
            }
        }


        if ( type === 'row' ) {
            if( table.rows({selected: true}).count() === 1){
                //au moins une ligne est sélectionnée - boutons modifier et supprimer enabled
                $("#updateButton").removeAttr('disabled');
                $("#deleteButton").removeAttr('disabled');

                //    var data = table.rows( indexes ).data().pluck( 'id' );
            }
        }
        //  select de plusieurs  lignes et recup des ID
        if ( type === 'row' ) {
            if( table.rows({selected: true}).count() >= 1){
                rows = table.rows({selected: true} ).data().toArray();
                recuparams(rows);


            }
        }

    });

    table.on( 'deselect', function ( e, dt, type, indexes ) {
        console.log ('on deselect');

        rows = table.rows({selected: true} ).data().toArray();
        recuparams(rows);

        if ( type === 'row' ) {
            if (table.rows({selected: true}).count() === 0) {
                $("#updateButton").attr('disabled','disabled');
                $("#deleteButton").attr('disabled','disabled');
            }
        }

    });
;



    //ajout des boutons print excel etc
    table.buttons().container()
        .appendTo( $('.col-sm-6:eq(1)', table.table().container() ) );
    table.buttons('commands',null).container()
        .appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );

    function recuparams(rows){
        //console.clear();
        var anfact = "";
        var idfr = "";
        tabid = [];
        for (let i in rows) {
            //console.log(JSON.stringify( rows[i] )); //shows
            tabid[i] = rows[i][1];
            anfact= rows[i][3];
            idfr = rows[i][7];

        }
        //console.log (tabid);
        recupid= JSON.stringify(tabid)
        recupidfrn= JSON.stringify(idfr)
        recupannee= JSON.stringify(anfact)

    }
});
