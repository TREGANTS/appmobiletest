$(document).ready(function() {
    const table = $('#nomac').DataTable( {
        stateSave: false,
        fixedHeader: true,
       // responsive: true,
        order: [[ 1, 'asc' ]],
        //select checkbox
        columnDefs: [

            {
                targets: 4,
                render: $.fn.dataTable.render.moment( 'DD-MM-YYYY','DD/MM/YYYY')
            },

            {
                targets: 5,
                render:  $.fn.dataTable.render.number( ' ', ',', 2,'' ,' €')

            },
            {
                //colonne de visualisation de la facture
                targets:7,
                orderable:false
            }],
        //filtrage au clic
        "initComplete": function () {

            var api = this.api();
            $('#nomac tbody td').not('.select-checkbox').on('click',function(){
                api.search( table.cell( this ).data() ).draw();
            })
        }

    } );

    //ajout des boutons copy excel pdf
    new $.fn.dataTable.Buttons( table, {
        name: 'commands-export',
        className : 'btn-info',
        buttons: [
            'copy', 'excel', 'pdf']
    });


    // ajout des boutons de commande
    new $.fn.dataTable.Buttons( table, {
        name: 'commands',
        buttons: [


        ],
    } );

    //ajout des boutons print excel etc
    table.buttons().container()
        .appendTo( $('.col-sm-6:eq(1)', table.table().container() ) );
    table.buttons('commands',null).container()
        .appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );
});