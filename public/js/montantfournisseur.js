$(document).ready(function() {
    const table = $('#nomac').DataTable( {
        stateSave: false,
        fixedHeader: true,
        order: [[ 3, 'desc' ]],
        columnDefs: [
            {
              targets: 3,
                render:  $.fn.dataTable.render.number( ' ', ',', 2,'' ,' €')
            },
            {
                targets: 4,
                render:  $.fn.dataTable.render.number( ' ', ',', 2,'' ,' €')
            },
        ],

        //filtrage au clic
        "initComplete": function () {
            var api = this.api();
            //$('#nomac tbody td').not('.select-checkbox, .linka').on('click',function(){
           //     api.search( table.cell( this ).data() ).draw();
           // })
        }
    } );

    //ajout des boutons copy excel pdf
    new $.fn.dataTable.Buttons( table, {
        name: 'commands-export',
        className : 'btn-info',
        buttons: [
            'copy', 'excel', 'pdf']
    });


    //ajout des boutons print excel etc
    table.buttons().container()
        .appendTo( $('.col-sm-6:eq(1)', table.table().container() ) );
    table.buttons('commands',null).container()
        .appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );
});