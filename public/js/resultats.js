
$(document).ready(function() {

// On récupère à partir de l'url active l'annee
const querystring=window.location.pathname;
var taburl=querystring.split('/');
var annee=taburl[2];
var groupColumn=0;
    console.log('on passe debut');
    $("#nomac").append('<tfoot><th></th></tfoot>');
    const table = $('#nomac').DataTable( {
        paging: true,
        fixedHeader: true,
        lengthMenu: [100, 50, 25, 8],
       // responsive: true,
        //colonne select checkbox
        columnDefs: [
            //famille
           { "visible": false, "targets": groupColumn },
           {
              //intitule
              targets:   3,
              render: $.fn.dataTable.render.ellipsis( 90,true,true )
           },
           {
                //colomne ouvrir fermer
                class: "details-control",
                targets:   1,
                orderable: false,
                "render": function () {
                    return '<i class="fa fa-plus-square" aria-hidden="true"></i>';
                },
                width:"15px"},
           {
               //detail intitule
            targets: 4,
            render: $.fn.dataTable.render.ellipsis( 40,true,true )
            },
            {
                //total colorisé en fonction des valeurs de seuil et seuil alerte
                targets:8,
                render: function (data,type,row){
                    if (type ==='display') {
                        let tooltip="Seuil alerte : " + $.fn.dataTable.render.number( ' ', ',', 2,'' ,' €'). display(row[5]) + " Seuil : " + $.fn.dataTable.render.number( ' ', ',', 2,'' ,' €'). display(row[6]);
                        let color = 'green';
                        let low=row[5]*100;
                        let hh=row[6]*100;
                        let newdata=data*100;
                        if (newdata >=low && newdata < hh) {
                            color = 'orange';
                        } else if (newdata >= hh) {
                            color = 'red';
                        } else {
                            color = 'green'
                        }
                        let vard=$.fn.dataTable.render.number( ' ', ',', 2,'' ,' €'). display(data);

                        return ('<span data-toggle="tooltip" style="font-weight: bold;color:' + color + '" title="' + tooltip + '">' + vard + '</span>');
                    }
                    return data;
                }
            },
            {
                targets:9,
                orderable: false
            },
            {
                targets:10,
                orderable: false
            }
        ],

        "order": [[ groupColumn, 'asc' ]],

        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            alert('on passe 11');
            console.log('on passe 11');
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                    );

                    last = group;
                }
            } );
        },

        "footerCallback": function ( row, data, start, end, display ) {
        //totaliser les montants
            var api = this.api();
           // alert('on passe 12');
            //console.log('on passe 12');
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                console.log('oh yeah');
                return typeof i === 'string' ?
                    i.replace(/[\€,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };


            total=api
                .column(8)
                .data()
                .reduce( function (a,b) {
                    return intVal(a) + intVal(b);
                } );

            // Total over this page
            pageTotal = api
                .column( 8, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
           pagetotalF= $.fn.dataTable.render.number( ' ', ',', 2,'' ,' €'). display(pageTotal)
            totalF=$.fn.dataTable.render.number( ' ', ',', 2,'' ,' €'). display(total)
            // Update footer
            $( api.column( 6 ).footer() ).html(
                '<span>' + totalF + '</span>'
            );
        },
    } ); //fin datatable

//entête de table fixe
   /* new $.fn.dataTable.FixedHeader(table, {
        alwaysCloneTop: true,
    });*/

    //ajout des boutons copy excel pdf
   /* new $.fn.dataTable.Buttons( table, {
        name: 'commands-export',
        className : 'btn-info',
        buttons: [
            'copy', 'excel', 'pdf']
    });*/

    var id;
    //détails des couples NF
    $('#nomac tbody').on( 'click', 'tr td.details-control', function () {

        var tr = $(this).closest('tr');
        var tdi = tr.find("i.fa");
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            tr.removeClass( 'shown' );
            row.child.hide();
            tdi.first().removeClass('fa-minus-square');
            tdi.first().addClass('fa-plus-square');
        }
        else {
            //affichage detail nomenclature/fournisseur
            //recuperation id nomenclature
            var tableid="table-" + row.data()[9]; //creation du tableid
            alert(tableid);
            var newhtml='<table class="child_table" id="' + tableid + '" style="width:100%"></table>';
            row.child(newhtml).show();

            //affichage datatable comprenant les couples NF
            //envoie d'une requete ajax
            const  childTable = $('#' + tableid).DataTable({
                "paging":false,
                "ordering":false,
                "serverSide": true,
                "order": [[ 0, "asc" ]],
                "lengthChange": false,
                "searching": false,
                "responsive": false,
                "info":     false,
                "ajax" : "/resultat/fournisseurs/" + row.data()[9] + "/" + annee,

                "columns": [

                    { "title":"N°Tiers","data":"numTiers"},
                    {"title":"Nom","data": "nom"},
                    {"title":"Type marché","data":"typeMarche"},
                    {"title":"N° marché","data":"numMarche"},
                    {"title":"Catégorie","data":"categorie"},
                    {"title":"Observation1","data":"obs1"},
                    {"title":"Total","data":"total"},
                    {"title":"","data":"auth"},
                ],
                columnDefs: [

                    {
                    //observation1
                    targets: 5,
                    render: $.fn.dataTable.render.ellipsis( 30,true,true )
                    } ,
                    {
                        //montant facture
                        targets: 6,
                        render: $.fn.dataTable.render.number( ' ', ',', 2, '',' €' )
                    },
                    {
                        targets:7,
                        render:function(data) {

                             if (data==="ROLE_ADMIN"){
                                result="<a class='btn_clic btn btn-default btn-warning fa fa-edit btn-sm' data-toggle='tooltip' data-placement='top' title='Modifier'></a><button class='btn_facture btn btn-primary btn-sm  fa fa-search' data-toggle='tooltip' data-placement='top' title='Factures'></button>"
                            }
                            else{

                                result="<a class='btn_clic btn btn-primary btn-sm  fa fa-search' data-toggle='tooltip' data-placement='top' title='Détail'></a><button class='btn_facture btn btn-primary btn-sm  fa fa-search' data-toggle='tooltip' data-placement='top' title='Factures'></button>";
                            }
                            return result;
                        }
                    }]
            });
            var tabfournisseur="#" + tableid + ' tbody';

            //ouverture modale nomenclature fournisseur
            $(tabfournisseur).on( 'click', '.btn_clic', function () {

                var tr = $(this).closest('tr');
                var row = childTable.row( tr );
                var dataRow = $('#' + tableid).DataTable().row(tr).data();
                id =dataRow.id;
                str= '/nomenclature/fournisseur/' + id + '/edit';
           //     console.log(str);

                $.ajax({
                    url: str,
                    success: function (data) {
                        $('.modal-body').html(data);
                        $('#modalNF').modal('show');
                    }
                });
            } );

            //ouverture de la modale d'affichage des factures
            $(tabfournisseur).on( 'click', '.btn_facture', function () {

                var tr = $(this).closest('tr');
                var row = childTable.row( tr );
                var dataRow = $('#' + tableid).DataTable().row(tr).data();
                id =dataRow.id;

                str= '/nomenclature/fournisseur/' + id + '/factures' ;
                $.ajax({
                    url: str,
                    success: function (data) {
                        $('.modal-body').html(data);
                        $('#NFfactures').DataTable();
                         $('#modalF').modal('show');

                    }
                });
            });
            tr.addClass('shown');
            tdi.first().removeClass('fa-plus-square');
            tdi.first().addClass('fa-minus-square');
        }
    } );//fin clic sur details-control

    // On each draw, loop over the `detailRows` array and show any child rows
    table.on("user-select", function (e, dt, type, cell, originalEvent) {
        if ($(cell.node()).hasClass("details-control")) {
            e.preventDefault();
        }
    });

  /*  //ajout des boutons print excel etc
    table.buttons().container()
        .appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );*/

    // trier les familles
    $('#nomac tbody').on( 'click', 'tr.group', function () {

        var currentOrder = table.order()[0];
        if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
            table.order( [ groupColumn, 'desc' ] ).draw();
        }
        else {
            table.order( [ groupColumn, 'asc' ] ).draw();
        }
    } );//fin trier sur familles

   //Modification du seuil
    $('#nomac tbody').on( 'click', '.btn_updaterow', function () {
        //enregistrement de la nouvelle valeur de seuil
        var mytr = $(this).closest('tr');
        select=mytr.find('select');
        idSeuil=select.val();
        var row = table.row( mytr );
        var datarow=row.data();
        id=datarow[9];
        str= '/nomenclature/' + id + '/editseuil/' + idSeuil ;
  //      alert(str);
        window.location.href = str;

    });//fin clic enregistrer nomenclature

});//document ready

$('#exercice').change(function(){
    //changement d'exercice recuperation annee
    var newAnnee=$('#exercice').val();
    window.location.href='/resultat/' + newAnnee;

});


/*function getHTMLSplit1() {
    str= "/myexcel/2021/";
    $.ajax({
        url: str,
        success: function (data) {
            alert("ok");
          //  console.log(data);
           /!* var win = window.open("", "_blank");
            win.location.href = data;*!/

          /!*  var $a = $("<a>");
            $a.attr("href",data.file);
            $("body").append($a);
            $a.attr("download","file.xls");
            $a[0].click();
            $a.remove();*!/
        }
    })*/
//};

