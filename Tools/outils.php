<?php
namespace App\Tools;

use Symfony\Component\Validator\ConstraintViolationList;

class outils
{
    public static function violations_to_array(ConstraintViolationList $violationsList, $propertyPath = null)
    {
        $output = array();

        foreach ($violationsList as $violation) {
            $output[$violation->getPropertyPath()][] = $violation->getMessage();
        }

        if (null !== $propertyPath) {
            if (array_key_exists($propertyPath, $output)) {
                $output = array($propertyPath => $output[$propertyPath]);
            } else {
                return array();
            }
        }

        return $output;
    }
}